// Fill out your copyright notice in the Description page of Project Settings.

#include "AI/AI_Character_Base.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"

#include "FuncLibrary/AnimUtils.h"
#include "Character/Animation/TDSCanSetDamageAnimNotify.h"
#include "Character/Animation/TDSAttackAnimIsEndNotify.h"
#include "ActorComponent/CustomMovementComponent/TDSMovementComponentBase.h"

// Sets default values
AAI_Character_Base::AAI_Character_Base(const FObjectInitializer& ObjInit)
	: Super(ObjInit.SetDefaultSubobjectClass<UTDSMovementComponentBase>(AAI_Character_Base::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//AI_CharacterMovement = Cast<UTDSMovementComponentBase>(GetMovementComponent());

	// Set Collision Profile:
	GetCapsuleComponent()->SetCollisionProfileName("AI_Pawn");

	AttackDelay_Delegate.BindUFunction(this, "SetCanAttack", true);

	SetCanAttack(true);
}

void AAI_Character_Base::SetCharacterParameters(FCharacterParameters NewCharacterParameters)
{
	CharacterParameters = NewCharacterParameters;
}

// Called when the game starts or when spawned
void AAI_Character_Base::BeginPlay()
{
	Super::BeginPlay();

	if (bIsSpawnDynamic)
	{
		SpawnFX();
	}
}

FCharacterParameters AAI_Character_Base::GetCharacterParameters() const
{
	return CharacterParameters;
}

void AAI_Character_Base::SpawnFX_Implementation()
{
}

// Called every frame
void AAI_Character_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AAI_Character_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

bool AAI_Character_Base::TryAttackTarget_Implementation()
{
	if (IsCanAttack())
	{
		if (bIsWaitAttackAnim)
		{
			StartPlayAttackAnimation();
			return true;
		}
		else
		{
			return AttackTarget(GetTarget(), CharacterParameters.DamageAttack);
		}
	}
	else
	{
		return false;
	}
}

void AAI_Character_Base::CharacterDeath_Implementation()
{
	Super::CharacterDeath_Implementation();
}

bool AAI_Character_Base::IsCanAttackReach(const AActor* Target) const
{
	// Current distance between this character and Target:
	const auto Distance = UKismetMathLibrary::Vector_Distance(this->GetActorLocation(), Target->GetActorLocation());

	return Distance <= CharacterParameters.AttackDistance;
}

void AAI_Character_Base::StartReloadAttack()
{
	if (GetWorld())
	{
		if (!GetWorld()->GetTimerManager().IsTimerActive(AttackDelay_Timer))
		{
			GetWorld()->GetTimerManager().SetTimer(AttackDelay_Timer, AttackDelay_Delegate, CharacterParameters.AttackFrequency, false);
		}
	}
}

bool AAI_Character_Base::AttackTarget_Implementation(AActor* Target, float Damage)
{
	if (Target)
	{
		Target->TakeDamage(Damage, FDamageEvent(), nullptr, this);

		SetCanAttack(false);

		return true;
	}
	else
	{
		return false;
	}

	return false;
}

bool AAI_Character_Base::IsCanAttack() const
{
	return bIsCanAttack;
}

void AAI_Character_Base::SetCanAttack(bool Can)
{
	bIsCanAttack = Can;
	OnCanAttackStatus.Broadcast(bIsCanAttack);
}

void AAI_Character_Base::StartPlayAttackAnimation_Implementation()
{
	// Choose anim:
	int8 Rand = FMath::RandHelper(AttackAnimations.Num());

	if (AttackAnimations.IsValidIndex(Rand) && AttackAnimations[Rand] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		// Play Montage:
		GetMesh()->GetAnimInstance()->Montage_Play(AttackAnimations[Rand]);

		auto CanAttackNotify = AnimUtils::FindNotifyByClass<UTDSCanSetDamageAnimNotify>(AttackAnimations[Rand]);

		// Debug:
		checkf(CanAttackNotify, TEXT("AttackAnimation: %s not have Notify \"UTDSCanSetDamageAnimNotify\""), *AttackAnimations[Rand]->GetName());

		auto EndNotify = AnimUtils::FindNotifyByClass<UTDSAttackAnimIsEndNotify>(AttackAnimations[Rand]);

		// Debug:
		checkf(EndNotify, TEXT("AttackAnimation: %s not have Notify \"UTDSAttackAnimIsEndNotify\""), *AttackAnimations[Rand]->GetName());

		// Bind to Notify anim is end and StartRagDoll:
		if (CanAttackNotify && EndNotify)
		{
			CanAttackNotify->OnNotifiedSignature.AddUObject(this, &AAI_Character_Base::AttackAfterAnimation);
			EndNotify->OnNotifiedSignature.AddUObject(this, &AAI_Character_Base::AttackAnimationIsEnd);
			SetCanAttack(false);
			bIsStartAttack = true;
			OnStartAttack.Broadcast(bIsStartAttack);
		}
	}
}

void AAI_Character_Base::AttackAfterAnimation_Implementation(USkeletalMeshComponent* SkeletalMesh)
{
	if (SkeletalMesh && GetMesh())
	{
		if (SkeletalMesh == GetMesh())
		{
			if (IsCanAttackReach(GetTarget()))
			{
				AttackTarget(GetTarget(), CharacterParameters.DamageAttack);
			}
		}
	}
}

void AAI_Character_Base::AttackAnimationIsEnd_Implementation(USkeletalMeshComponent* SkeletalMesh)
{
	SetCanAttack(true);
	bIsStartAttack = false;
	OnStartAttack.Broadcast(bIsStartAttack);
}
