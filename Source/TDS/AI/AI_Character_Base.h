// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/TDSCharacterBase.h"
#include "FuncLibrary/Types.h"
#include "AI_Character_Base.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCanAttackStatus, bool, bIsCanAttack);

class UTPSHealthComponentBase;
class UTDSMovementComponentBase;

UCLASS()
class TDS_API AAI_Character_Base : public ATDSCharacterBase
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAI_Character_Base(const FObjectInitializer& ObjInit);

#pragma region Parameters

public:
	UFUNCTION(BlueprintCallable, Category = "AI_Character")
	void SetCharacterParameters(FCharacterParameters NewCharacterParameters);

protected:
	// Base Character Parameters (Speed, Damage, Distance to attack):
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI_Character")
	FCharacterParameters CharacterParameters;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "AI_Character")
	FCharacterParameters GetCharacterParameters() const;

#pragma endregion

#pragma region Component

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character | Movement")
	UTDSMovementComponentBase* AI_CharacterMovement = nullptr;

#pragma endregion

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AI_Character")
	void SpawnFX();
	virtual void SpawnFX_Implementation();

#pragma region BaseFunction

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

#pragma endregion

#pragma region ICharacterInterface
public:
	bool TryAttackTarget_Implementation();

#pragma endregion
protected:
	virtual void CharacterDeath_Implementation() override;

#pragma region Attack
public:

	UPROPERTY(BlueprintAssignable, Category = "AI_Character")
	FOnCanAttackStatus OnCanAttackStatus;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "AI_Character")
	bool IsCanAttackReach(const AActor* Target) const;

	UFUNCTION(BlueprintCallable, Category = "AI_Character")
	void StartReloadAttack();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AI_Character")
	bool AttackTarget(AActor* Target, float Damage);
	virtual bool AttackTarget_Implementation(AActor* Target, float Damage);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "AI_Character")
	bool IsCanAttack() const;

	UFUNCTION(BlueprintCallable, Category = "AI_Character")
	void SetCanAttack(bool Can);

private:
	bool bIsCanAttack = true;
	bool bIsStartAttack = false;
	FTimerHandle AttackDelay_Timer;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AI_Character")
	void StartPlayAttackAnimation();
	void StartPlayAttackAnimation_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AI_Character")
	void AttackAfterAnimation(USkeletalMeshComponent* SkeletalMesh);
	void AttackAfterAnimation_Implementation(USkeletalMeshComponent* SkeletalMesh);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "AI_Character")
	void AttackAnimationIsEnd(USkeletalMeshComponent* SkeletalMesh);
	void AttackAnimationIsEnd_Implementation(USkeletalMeshComponent* SkeletalMesh);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI_Character")
	bool bIsWaitAttackAnim = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI_Character")
	TArray<UAnimMontage*> AttackAnimations = {};

	FTimerDelegate AttackDelay_Delegate;

#pragma endregion

public:
	UFUNCTION(BlueprintCallable, Category = "AI_Character")
	void SetCanMove(bool Can) { bIsCanMove = Can; }
};
