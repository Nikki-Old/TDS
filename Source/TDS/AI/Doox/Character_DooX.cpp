// Fill out your copyright notice in the Description page of Project Settings.

#include "AI/Doox/Character_DooX.h"
#include "PaperFlipbookComponent.h"
#include "PaperFlipbook.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "ActorComponent/CustomMovementComponent/DooXMovementComponent.h"
#include "Game/GameMode/TPSGameMode.h"
#include "FuncLibrary/PrintString.h"
#include "ActorComponent/Health/TPSUpdatedHealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogCharacter_DooX, All, All);

ACharacter_DooX::ACharacter_DooX(const FObjectInitializer& ObjInit)
	: Super(ObjInit.SetDefaultSubobjectClass<UDooXMovementComponent>(ACharacter_DooX::CharacterMovementComponentName))
{
	// Create Flipbook component:
	FlipbookComponent = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("FlipbookComponent"));
	FlipbookComponent->SetupAttachment(GetRootComponent());

	// Off collision on flipbook component:
	FlipbookComponent->SetCollisionProfileName("NoCollision");

	// Set Collision Profile:
	GetCapsuleComponent()->SetCollisionProfileName("AI_Pawn");

	// Mesh is not needed
	GetMesh()->DestroyComponent();

#pragma region Movement

	DooXMovementComponent = Cast<UDooXMovementComponent>(GetMovementComponent());

#pragma endregion

	AttackDelay_Delegate.BindLambda([this]() { SetStateFlipbookComponent(EFlipbookStateDooX::Idle_State);
		SetCanAttack(true); });

	SpecialAttackEnd_Delegate.BindLambda([this]() {
		SetStateFlipbookComponent(EFlipbookStateDooX::Idle_State);
										 SetCanAttack(true);
		bIsSpecialAttack = false; });

	SpecialAttackStart_Delegate.BindLambda([this]() { 
		SetCanMove(true);
		SetCanAttack(true);
		GetWorld()->GetTimerManager().SetTimer(SpecialAttackDelayEnd_Timer, SpecialAttackEnd_Delegate, TimeDelaySpecialAttackEnd, false); });

	SpecialAttackDelay_Delegate.BindLambda([this]() { bIsSpecialAttack = true; });
}

void ACharacter_DooX::BeginPlay()
{
	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &ACharacter_DooX::CapsuleOnComponentHit);
	DefaultRotation = FlipbookComponent->GetRelativeRotation();
	InitEnemy();

	Super::BeginPlay();
}

void ACharacter_DooX::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACharacter_DooX::InitEnemy()
{
	if (GetWorld())
	{
		if (CharacterParameters.IsEmpty())
		{
			// TO DO: DT need move to EnemyController:
			GameMode = Cast<ATPSGameMode>(GetWorld()->GetAuthGameMode());
			const auto ClassName = this->GetClass()->GetName();
			//GEngine->AddOnScreenDebugMessage(0, 10.0f, FColor::Blue, ClassName);
			if (GameMode)
			{
				if (GameMode->GetEnemyParamsByName(FName(ClassName), CharacterParameters))
				{
					UpdateEnemyParameters();
				}

				//if (!bIsSpawnDynamic)
				//{
				//	auto EnemyController = Cast<UEnemyControllerComponent>(GameMode->GetComponentByClass(UEnemyControllerComponent::StaticClass()));
				//	if (EnemyController)
				//	{
				//		EnemyController->OnTargetForEnemy.AddDynamic(this, &ACharacter_DooX::SetTarget);
				//		this->SetTarget(EnemyController->GetTargetForEnemy());
				//	}
				//}
			}
			else
			{
				UE_LOG(LogCharacter_DooX, Warning, TEXT("ACharacter_DooX::InitEnemy - GameMode is not valid."));
			}
		}
		else
		{
			UpdateEnemyParameters();
		}
	}
}

void ACharacter_DooX::SetStateFlipbookComponent(EFlipbookStateDooX NewFlipbookState)
{
	float NewSpeed = 0.0f;

	switch (NewFlipbookState)
	{
		case EFlipbookStateDooX::Idle_State:
			if (IdleFlipbook)
			{
				FlipbookComponent->SetFlipbook(IdleFlipbook);
				NewSpeed = CharacterParameters.MovementSpeed;

				break;
			}

		case EFlipbookStateDooX::Attack_State:
			if (AttackFlipbook)
			{
				FlipbookComponent->SetFlipbook(AttackFlipbook);
				NewSpeed = 0.0f;
				break;
			}

		case EFlipbookStateDooX::SpecialAttack_State:
			if (SpecialAttackFlipbook)
			{
				FlipbookComponent->SetFlipbook(SpecialAttackFlipbook);
				NewSpeed = CharacterParameters.MovementSpeed * CoefMovementSpeedSpecialAttack;
				break;
			}
	}

	CurrentFlipbookState = NewFlipbookState;

	if (DooXMovementComponent)
	{
		DooXMovementComponent->MaxWalkSpeed = NewSpeed;
	}
}

void ACharacter_DooX::SpawnFX_Implementation()
{
	if (!GetWorld())
	{
		return;
	}

	// Death FX:
	for (const auto CurrentParticle : SpawnParticleSystems)
	{
		if (CurrentParticle)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CurrentParticle, this->GetActorTransform());
		}
	}
}

void ACharacter_DooX::SetRotationToTarget(const AActor* Target)
{
	if (Target)
	{
		// Find look at Rotation between THIS Actor and Target:
		auto LookAtRotation = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), Target->GetActorLocation());

		// Add offset:
		auto NewRotationYaw = (UKismetMathLibrary::ComposeRotators(LookAtRotation, RotationToTargetOffset)).Yaw;

		// Set Rotation:
		if (FlipbookComponent)
		{
			FlipbookComponent->SetWorldRotation(FRotator(DefaultRotation.Pitch, NewRotationYaw, DefaultRotation.Roll));
		}
	}
}

bool ACharacter_DooX::AttackTarget_Implementation(AActor* Target, float Damage)
{
	if (!bIsSpecialAttack)
	{
		if (Super::AttackTarget_Implementation(Target, Damage))
		{
			SetStateFlipbookComponent(EFlipbookStateDooX::Attack_State);

			return true;
		}
	}

	return false;
}

void ACharacter_DooX::SpecialAttack_Implementation(AActor* Target)
{
	if (!GetWorld() || !Target || !bIsSpecialAttack || !IsCanAttack())
	{
		return;
	}

	bIsSpecialAttack = true;

	// Stop Attack:
	SetCanAttack(false);

	// Set State FlipbooK
	SetStateFlipbookComponent(EFlipbookStateDooX::SpecialAttack_State);

	// Stop move:
	SetCanMove(false);

	// Start delay-timer:
	GetWorld()->GetTimerManager().SetTimer(SpecialAttackDelayStart_Timer, SpecialAttackStart_Delegate, TimeDelaySpecialAttackStart, false);
	GetWorld()->GetTimerManager().SetTimer(SpecialAttackDelay_Timer, SpecialAttackDelay_Delegate, SpecialAttackTimeDelay, false);
}

void ACharacter_DooX::CharacterDeath_Implementation()
{
	if (!GetWorld())
	{
		return;
	}

	// Death FX:
	for (const auto CurrentParticle : DeathParticleSystems)
	{
		if (CurrentParticle)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CurrentParticle, this->GetActorTransform());
		}
	}
}

void ACharacter_DooX::UpdateEnemyParameters_Implementation()
{
	if (!CharacterParameters.IsEmpty())
	{
		if (DooXMovementComponent)
		{
			// Set MovementSpeed:
			DooXMovementComponent->MaxWalkSpeed = CharacterParameters.MovementSpeed;
			SpecialAttackTimeDelay = CharacterParameters.SpecialAttackFrequency;
		}
	}
}

void ACharacter_DooX::DooXCharacterOnFalling()
{
	if (FlipbookComponent)
	{
		FlipbookComponent->Stop();
	}
}

void ACharacter_DooX::DooXCharacterOnWalking()
{
	if (FlipbookComponent)
	{
		if (!FlipbookComponent->IsPlaying())
		{
			FlipbookComponent->Play();
		}
	}
}

void ACharacter_DooX::CapsuleOnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor)
	{
		if (bIsSpecialAttack)
		{
			if (OtherActor == GetTarget())
			{
				Super::AttackTarget_Implementation(GetTarget(), CharacterParameters.DamageAttack * CharacterParameters.SpecialAttackCoefDamage);
				bIsSpecialAttack = false;
			}
		}
	}
}
