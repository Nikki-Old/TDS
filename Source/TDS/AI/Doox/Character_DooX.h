// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/AI_Character_Base.h"
#include "FuncLibrary/Types.h"
#include "Character_DooX.generated.h"

/**
 * 
 */

class UPaperFlipbookComponent;
class UPaperFlipbook;
class UDooXMovementComponent;
class ATPSGameMode;
class UParticleSystem;

UCLASS()
class TDS_API ACharacter_DooX : public AAI_Character_Base
{
	GENERATED_BODY()

public:
	ACharacter_DooX(const FObjectInitializer& ObjInit);

#pragma region Component

	/** Flipbook Component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX")
	UPaperFlipbookComponent* FlipbookComponent = nullptr;

	/** Movement Component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX")
	UDooXMovementComponent* DooXMovementComponent = nullptr;

#pragma endregion

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	/***/
	UFUNCTION(BlueprintCallable, Category = "CharacterDooX | Movement")
	void SetRotationToTarget(const AActor* Target);

protected:
	UFUNCTION()
	void InitEnemy();

#pragma region Flipbook

	// Flipbook:

	/** Idle Paper Flipbook */ // TO DO: Array
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | State")
	UPaperFlipbook* IdleFlipbook = nullptr;

	/** Attack Paper Flipbook */ // TO DO: Array
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | State")
	UPaperFlipbook* AttackFlipbook = nullptr;

	/** Special Attack Paper Flipbook */ // TO DO: Array
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | State")
	UPaperFlipbook* SpecialAttackFlipbook = nullptr;

	/** Each state correspons a specific flipbook. */
	UFUNCTION(BlueprintCallable, Category = "CharacterDooX | State")
	void SetStateFlipbookComponent(EFlipbookStateDooX NewFlipbookState);

#pragma endregion

	virtual void SpawnFX_Implementation() override;

	// Movement:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterDooX | Movement")
	FRotator RotationToTargetOffset = FRotator(0);

	// Attack Target:
	virtual bool AttackTarget_Implementation(AActor* Target, float Damage) override;

	/** Special attack */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "CharacterDooX")
	void SpecialAttack(AActor* Target);
	virtual void SpecialAttack_Implementation(AActor* Target);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | SpecialAttack")
	float TimeDelaySpecialAttackStart = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | SpecialAttack")
	float TimeDelaySpecialAttackEnd = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | SpecialAttack")
	float CoefMovementSpeedSpecialAttack = 2.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | SpecialAttack")
	float SpecialAttackTimeDelay = 10.0f;

	virtual void CharacterDeath_Implementation() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "CharacterDooX | Target")
	void UpdateEnemyParameters();
	void UpdateEnemyParameters_Implementation();

	// FX:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | FX")
	TArray<UParticleSystem*> DeathParticleSystems;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterDooX | FX")
	TArray<UParticleSystem*> SpawnParticleSystems;

	UFUNCTION()
	void CapsuleOnComponentHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

private:
	FRotator DefaultRotation = FRotator(0);

	EFlipbookStateDooX CurrentFlipbookState = EFlipbookStateDooX::NONE_State;

#pragma region Movement

public:
	UFUNCTION()
	void DooXCharacterOnFalling();
	UFUNCTION()
	void DooXCharacterOnWalking();

#pragma endregion

private:
	ATPSGameMode* GameMode = nullptr;

	// Special Attack:

	bool bIsSpecialAttack = true;

	FTimerHandle SpecialAttackDelayStart_Timer;
	FTimerHandle SpecialAttackDelayEnd_Timer;

	FTimerDelegate SpecialAttackStart_Delegate;
	FTimerDelegate SpecialAttackEnd_Delegate;

	FTimerHandle SpecialAttackDelay_Timer;
	FTimerDelegate SpecialAttackDelay_Delegate;
};
