// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Zombi/Character_Zombi.h"
#include "CustomMovementComponent/ZombiMovementComponent.h"

ACharacter_Zombi::ACharacter_Zombi(const FObjectInitializer& ObjInit)
	: Super(ObjInit.SetDefaultSubobjectClass<UZombiMovementComponent>(ACharacter_Zombi::CharacterMovementComponentName))
{
	ZombiMovementComponent = Cast<UZombiMovementComponent>(GetMovementComponent()); //GetCustomCharacterMovement());
}

void ACharacter_Zombi::SetZombieType(EZombieType NewZombieState)
{
	if (GetZombieType() != NewZombieState)
	{
		ZombieType = NewZombieState;
	}
}
