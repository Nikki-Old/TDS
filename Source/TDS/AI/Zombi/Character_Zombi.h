// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/AI_Character_Base.h"
#include "FuncLibrary/Types.h"
#include "Character_Zombi.generated.h"

/**
 * 
 */

class UZombiMovementComponent;

UCLASS()
class TDS_API ACharacter_Zombi : public AAI_Character_Base
{
	GENERATED_BODY()

public:
	ACharacter_Zombi(const FObjectInitializer& ObjInit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	UZombiMovementComponent* ZombiMovementComponent = nullptr;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Zombie")
	FORCEINLINE EZombieType GetZombieType() const { return ZombieType; }

protected:
	UFUNCTION(BlueprintCallable, Category = "Zombie")
	void SetZombieType(EZombieType NewZombieState);

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "Zombie")
	EZombieType ZombieType = EZombieType::None_Type;
};
