// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/Cursor/TDSCursorComponent.h"
#include "Character/TDSCharacterBase.h"
#include "UI/Cursor/CursorWidget.h"

UTDSCursorComponent::UTDSCursorComponent()
{
	WidgetClass = UCursorWidget::StaticClass();
}

void UTDSCursorComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerCharacter = Cast<ATDSCharacterBase>(GetOwner());

	if (OwnerCharacter)
	{
		auto CursorWidget = Cast<UCursorWidget>(this->GetWidget());
		if (CursorWidget)
		{
			CursorWidget->SetOwnerCharacter(OwnerCharacter);
		}
	}
}
