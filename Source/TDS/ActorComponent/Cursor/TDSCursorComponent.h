// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "TDSCursorComponent.generated.h"

/**
 * 
 */

class ATDSCharacterBase;

UCLASS()
class TDS_API UTDSCursorComponent : public UWidgetComponent
{
	GENERATED_BODY()

public:
	// Constructor:
	UTDSCursorComponent();

protected:

	void BeginPlay();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "CursorComponent")
	ATDSCharacterBase* OwnerCharacter = nullptr;
};
