// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/CustomMovementComponent/DooXMovementComponent.h"
#include "AI/Doox/Character_DooX.h"

UDooXMovementComponent::UDooXMovementComponent()
{

}

void UDooXMovementComponent::TickComponent(float DeltaSeconds, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
	{
	Super::TickComponent(DeltaSeconds, TickType, ThisTickFunction);

	MovementTick(DeltaSeconds);
}

void UDooXMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UDooXMovementComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (CharacterOwner)
	{
		CharacterDooxOwner = Cast<ACharacter_DooX>(CharacterOwner);

		if (CharacterDooxOwner)
		{
			this->OnWalking.AddUniqueDynamic(CharacterDooxOwner, &ACharacter_DooX::DooXCharacterOnWalking);
			this->OnFalling.AddUniqueDynamic(CharacterDooxOwner, &ACharacter_DooX::DooXCharacterOnFalling);
		}
	}
}

void UDooXMovementComponent::SetMovementMode(EMovementMode NewMovementMode, uint8 NewCustomMode)
{
	Super::SetMovementMode(NewMovementMode, NewCustomMode);

	switch (NewMovementMode)
	{
		case EMovementMode::MOVE_Falling:
			OnFalling.Broadcast();
			break;

		case EMovementMode::MOVE_Walking:
			OnWalking.Broadcast();
			break;
	}
}

void UDooXMovementComponent::MovementTick(float DeltaSeconds)
{
	if (CharacterDooxOwner && !CharacterDooxOwner->IsDead())
	{
		if (CharacterDooxOwner->GetTarget())
		{
			CharacterDooxOwner->SetRotationToTarget(CharacterDooxOwner->GetTarget());
		}
	}
}
