// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CustomMovementComponent/TDSMovementComponentBase.h"
#include "DooXMovementComponent.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWalking);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFallingOwner);

class ACharacter_DooX;

UCLASS(Blueprintable)
class TDS_API UDooXMovementComponent : public UTDSMovementComponentBase
{
	GENERATED_BODY()

public:
	UDooXMovementComponent();

	UPROPERTY()
	FOnWalking OnWalking;

	UPROPERTY()
	FOnFallingOwner OnFalling;

#pragma region FunctionBase

	virtual void BeginPlay() override;

	/** Base Tick: */
	virtual void TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void InitializeComponent() override;

#pragma endregion

private:
	virtual void SetMovementMode(EMovementMode NewMovementMode, uint8 NewCustomMode = 0) override;

protected:
	/** Movement tick */
	virtual void MovementTick(float DeltaSeconds) override;

	ACharacter_DooX* CharacterDooxOwner = nullptr;
};
