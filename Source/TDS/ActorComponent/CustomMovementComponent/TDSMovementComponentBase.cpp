// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/CustomMovementComponent/TDSMovementComponentBase.h"
#include "Character/TDSCharacterBase.h"

UTDSMovementComponentBase::UTDSMovementComponentBase()
{
}

void UTDSMovementComponentBase::TickComponent(float DeltaSeconds, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaSeconds, TickType, ThisTickFunction);

	MovementTick(DeltaSeconds);
}

float UTDSMovementComponentBase::GetMaxSpeed() const
{
	return UpdateMovementState();
}

void UTDSMovementComponentBase::InitializeComponent()
{
	Character = Cast<ATDSCharacterBase>(GetOwner());

	if (Character)
	{
		// Bind ChangeMovementState	in Custom Movement Component on delegate OnChangeMovementState:
		Character->OnChangeMovementState.AddDynamic(this, &UTDSMovementComponentBase::ChangeMovementState);

		Character->OnCharacterDead.AddDynamic(this, &UTDSMovementComponentBase::CharacterIsDead);
	}
	Super::InitializeComponent();
}

float UTDSMovementComponentBase::UpdateMovementState() const
{
	return Super::GetMaxSpeed();
}

void UTDSMovementComponentBase::MovementTick(float DeltaSeconds)
{
}

void UTDSMovementComponentBase::CharacterIsDead_Implementation(ATDSCharacterBase* Char)
{
	if (Character != Char)
		return;

	bCharacterIsDead = true;
}

void UTDSMovementComponentBase::ChangeMovementState_Implementation()
{
}
