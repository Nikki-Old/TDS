// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TDSMovementComponentBase.generated.h"

/**
 * 
 */

class ATDSCharacterBase;

UCLASS(Blueprintable)
class TDS_API UTDSMovementComponentBase : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UTDSMovementComponentBase();

#pragma region FunctionBase
	/** Base Tick: */
	virtual void TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Overloading a dunction GetMaxSpeed: */
	virtual float GetMaxSpeed() const override;

	virtual void InitializeComponent() override;
#pragma endregion

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MovementComponent")
	void ChangeMovementState();
	virtual void ChangeMovementState_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "")
	void CharacterIsDead(ATDSCharacterBase* Char);
	virtual void CharacterIsDead_Implementation(ATDSCharacterBase* Char);

private:
	// Set speed and state:
	UFUNCTION()
	virtual float UpdateMovementState() const;

protected:
	/** Tick for movement: */
	virtual void MovementTick(float DeltaSeconds);

protected:
	/****/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "MovementComponent")
	TArray<float> Speeds = {};

	UPROPERTY()
	bool bCharacterIsDead = false;

	ATDSCharacterBase* Character = nullptr;
};
