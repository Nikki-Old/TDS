// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/CustomMovementComponent/TPSCharacterMovementComponent.h"

#include "Character/TDSCharacterBase.h"
#include "Interface/WeaponInterface.h"

#include "Kismet/KismetMathLibrary.h"

UTPSCharacterMovementComponent::UTPSCharacterMovementComponent()
{
	Speeds = {
		0.0f,	// None_State
		200.0f, // Aim_State
		400.0f, // AimWalk_State
		300.0f, // Walk_State
		600.0f, // Run_State
		800.0f	// SprintRun_State
	};

	CanDash = true;
	DashStop = 0.1f;
	DashDistance = 6000.f;
	DashCooldown = 1.f;
}

void UTPSCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTPSCharacterMovementComponent::InitializeComponent()
{
	Super::InitializeComponent();
}

void UTPSCharacterMovementComponent::TickComponent(
	float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaSeconds, TickType, ThisTickFunction);
}

void UTPSCharacterMovementComponent::MovementTick(
	float DeltaSeconds)
{
	Super::MovementTick(DeltaSeconds);

	if (!Character || bCharacterIsDead)
		return;

	Character->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Character->AxisY);
	Character->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Character->AxisX);

	if (bIsPressesSprint)
	{
		CheckingCanSprint();
	}
}

ECharacterMovementState UTPSCharacterMovementComponent::GetCurrentMovementState() const
{
	return MovementState;
}

/** Overloading a dunction GetMaxSpeed: */
float UTPSCharacterMovementComponent::GetMaxSpeed() const
{
	if (!Character && bCharacterIsDead)
		return -1.f;

	return Super::GetMaxSpeed();
}

// Set speed and state:
float UTPSCharacterMovementComponent::UpdateMovementState() const
{
	float ResSpeed = 0.0f; //MovementSpeedInput.RunSpeedNormal; // Start speed. ???

	check(Speeds.Num() == uint8(ECharacterMovementState::MAX_State));

	switch (MovementState)
	{
		case ECharacterMovementState::Aim_State:
			ResSpeed = Speeds[uint8(ECharacterMovementState::Aim_State)];
			break;
		case ECharacterMovementState::AimWalk_State:
			ResSpeed = Speeds[uint8(ECharacterMovementState::AimWalk_State)];
			break;
		case ECharacterMovementState::Walk_State:
			ResSpeed = Speeds[uint8(ECharacterMovementState::Walk_State)];
			break;
		case ECharacterMovementState::Run_State:
			ResSpeed = Speeds[uint8(ECharacterMovementState::Run_State)];
			break;
		case ECharacterMovementState::SprintRun_State:
			ResSpeed = Speeds[uint8(ECharacterMovementState::SprintRun_State)];
			break;

		default:
			break;
	}

	return ResSpeed; // Set speed walk.
}

void UTPSCharacterMovementComponent::CheckingCanSprint()
{
	FVector MovementDir = Character->GetVelocity();
	MovementDir.Normalize(0.00001f);
	FVector ForwardDir = Character->GetActorForwardVector();
	ForwardDir.Normalize(0.00001f);

	const float RangeNow = UKismetMathLibrary::DegAcos(UKismetMathLibrary::Dot_VectorVector(MovementDir, ForwardDir));

	const bool CanActive = RangeNow <= SprintActivateZone; // && RangeNow >= -SprintActivateZone;

	if (CanActive)
	{
		SprintRunEnabled = true;
		ChangeMovementState();
	}
	else
	{
		SprintRunEnabled = false;
		ChangeMovementState();
	}
}

void UTPSCharacterMovementComponent::ChangeMovementState_Implementation()
{
	if (!Character)
		return;

	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = ECharacterMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = ECharacterMovementState::SprintRun_State;
		}

		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = ECharacterMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = ECharacterMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = ECharacterMovementState::Aim_State;
				}
			}
		}
	}

	AWeaponDefault* OwnerWeapon = nullptr;
	if (IWeaponInterface::Execute_GetWeapon(Character, OwnerWeapon))
	{
		// Weapon state update:
		if (OwnerWeapon)
		{
			OwnerWeapon->UpdateStateWeapon(MovementState);
		}
	}
}

void UTPSCharacterMovementComponent::CharacterIsDead_Implementation(ATDSCharacterBase* Char)
{
	Super::CharacterIsDead_Implementation(Char);

	StopDashing();
}

#pragma region Dash

void UTPSCharacterMovementComponent::Dash()
{
	if (CanDash && Character && !bCharacterIsDead)
	{
		Character->GetCharacterMovement()->BrakingFrictionFactor = 0.f;
		Character->LaunchCharacter(FVector(Character->GetVelocity().GetSafeNormal()) * DashDistance, false, false);
		CanDash = false;
		GetOwner()->GetWorldTimerManager().SetTimer(UnusedHandle, this, &UTPSCharacterMovementComponent::StopDashing, DashStop, false);
	}
}

void UTPSCharacterMovementComponent::StopDashing()
{
	if (!Character)
		return;

	Character->GetCharacterMovement()->StopMovementImmediately();
	Character->GetWorldTimerManager().SetTimer(UnusedHandle, this, &UTPSCharacterMovementComponent::ResetDash, DashCooldown, false);
	Character->GetCharacterMovement()->BrakingFrictionFactor = 2.f;
}

void UTPSCharacterMovementComponent::ResetDash()
{
	CanDash = true;
}

#pragma endregion
