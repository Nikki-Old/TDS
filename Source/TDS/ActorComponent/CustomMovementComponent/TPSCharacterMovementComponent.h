// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CustomMovementComponent/TDSMovementComponentBase.h"
#include "FuncLibrary/Types.h"
#include "TPSCharacterMovementComponent.generated.h"

/**
 *		Character movement component:
 */

UCLASS(Blueprintable)
class TDS_API UTPSCharacterMovementComponent : public UTDSMovementComponentBase
{
	GENERATED_BODY()

public:
	UTPSCharacterMovementComponent();

#pragma region FunctionBase
	/** Begin play: */
	virtual void BeginPlay() override;

	virtual void InitializeComponent() override;

	/** Base Tick: */
	virtual void TickComponent(float DeltaSeconds, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Overloading a dunction GetMaxSpeed: */
	virtual float GetMaxSpeed() const override;
#pragma endregion

private:
	/** Current movement state: */
	ECharacterMovementState MovementState = ECharacterMovementState::Run_State;

	// Set speed and state:
	virtual float UpdateMovementState() const override;

public:
	/** Inputs: */
	// Input's:
	// Movement
	ECharacterMovementState GetCurrentMovementState() const;



	// State movement:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;

	// Flag:
	// Monitor whether to sprint
	bool bIsPressesSprint = false;

	// For active sprint need allowed range in angels.
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float SprintActivateZone = 30.0f;

#pragma region Dash

	/** Dash: */
	UFUNCTION()
	void Dash();
	UFUNCTION()
	void StopDashing();

	// For dash:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Movement | Dash")
	float DashDistance = 0.f;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Movement | Dash")
	float DashCooldown = 0.f;
	// Flags:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Movement | Dash")
	bool CanDash;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Movement | Dash")
	float DashStop;
	UPROPERTY()
	FTimerHandle UnusedHandle;

#pragma endregion

	virtual void ChangeMovementState_Implementation() override;

	virtual void CharacterIsDead_Implementation(ATDSCharacterBase* Char) override;

private:
	/** Tick for movement: */
	UFUNCTION()
	virtual void MovementTick(float DeltaSeconds) override;

	UFUNCTION()
	void ResetDash();

public:
	UFUNCTION(BlueprintCallable)
	void CheckingCanSprint();
};
