// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/CustomMovementComponent/ZombiMovementComponent.h"
#include "GameFramework/NavMovementComponent.h"

UZombiMovementComponent::UZombiMovementComponent()
{
	Speeds = {
		0.0f,	// None_State
		200.0f, // Aim_State
		400.0f, // AimWalk_State
		300.0f, // Walk_State
	};
}

EZombiMovementState UZombiMovementComponent::GetZombiMovementState() const
{
	return ZombiMovementState;
}

void UZombiMovementComponent::RequestPathMove(const FVector& MoveVelocity)
{
	Super::RequestPathMove(MoveVelocity);
}

void UZombiMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	Super::RequestDirectMove(MoveVelocity, bForceMaxSpeed);
}

float UZombiMovementComponent::UpdateMovementState() const
{
	float ResSpeed = 0.0f; //MovementSpeedInput.RunSpeedNormal; // Start speed. ???

	check(Speeds.Num() == uint8(EZombiMovementState::MAX_State));

	switch (ZombiMovementState)
	{
		case EZombiMovementState::None_State:
			ResSpeed = Speeds[uint8(EZombiMovementState::None_State)];
			break;
		case EZombiMovementState::Slow_State:
			ResSpeed = Speeds[uint8(EZombiMovementState::Slow_State)];
			break;
		case EZombiMovementState::Normal_State:
			ResSpeed = Speeds[uint8(EZombiMovementState::Normal_State)];
			break;
		case EZombiMovementState::Fast_State:
			ResSpeed = Speeds[uint8(EZombiMovementState::Fast_State)];
			break;
		default:
			break;
	}

	return ResSpeed; // Set speed walk.
}