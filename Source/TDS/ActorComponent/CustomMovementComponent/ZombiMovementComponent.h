// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CustomMovementComponent/TDSMovementComponentBase.h"
#include "ZombiMovementComponent.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EZombiMovementState : uint8
{
	None_State UMETA(DisplayName = "None"),
	Slow_State UMETA(DisplayName = "Slow"),
	Normal_State UMETA(DisplayName = "Normal"),
	Fast_State UMETA(DisplayName = "Fast"),
	MAX_State UMETA(DisplayName = "MAX")
};

UCLASS(Blueprintable)
class TDS_API UZombiMovementComponent : public UTDSMovementComponentBase
{
	GENERATED_BODY()

public:
	UZombiMovementComponent();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ZombiMovement")
	EZombiMovementState GetZombiMovementState() const;

	virtual void RequestPathMove(const FVector& MoveVelocity) override;
	virtual void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;

private:
	virtual float UpdateMovementState() const override;

protected:
	UPROPERTY(BlueprintReadWrite, Category = "ZombiMovement")
	EZombiMovementState ZombiMovementState = EZombiMovementState::None_State;
};
