#include "ControlledEnemyComponent.h"
// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/Enemy/EnemyControllerComponent.h"
#include "GameFramework/GameModeBase.h"
#include "Interface/CharacterInterface.h"

// Sets default values for this component's properties
UControlledEnemyComponent::UControlledEnemyComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Called when the game starts
void UControlledEnemyComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

void UControlledEnemyComponent::SetTargetForOwner(AActor* NewTarget)
{
	auto CharacterInterface = Cast<ICharacterInterface>(GetOwner());
	if(CharacterInterface)
	{
		ICharacterInterface::Execute_SetTarget(GetOwner(), NewTarget);
		OnCurrentSwapTarget.Broadcast(GetOwner(), NewTarget);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Owner not have ICharacterInterface!"));
	}
}

// Called every frame
void UControlledEnemyComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UControlledEnemyComponent::InitComponent(UEnemyControllerComponent* EnemyController)
{
	if (!GetWorld() || bIsInit)
	{
		return;
	}

	if (EnemyController)
	{
		EnemyController->OnTargetForEnemy.AddDynamic(this, &UControlledEnemyComponent::SetTargetForOwner);
		SetTargetForOwner(EnemyController->GetTargetForEnemy());
		bIsInit = true;
	}
}
