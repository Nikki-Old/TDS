// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ControlledEnemyComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCurrentSwapTarget, AActor*, Owner, AActor*, NewTarget);

class UEnemyControllerComponent;

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_API UControlledEnemyComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UControlledEnemyComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void SetTargetForOwner(AActor* NewTarget);

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "EnemyController")
	void InitComponent(UEnemyControllerComponent* EnemyController);

	UPROPERTY(BlueprintAssignable, Category = "EnemyController")
	FOnCurrentSwapTarget OnCurrentSwapTarget;

private:
	bool bIsInit = false;
};
