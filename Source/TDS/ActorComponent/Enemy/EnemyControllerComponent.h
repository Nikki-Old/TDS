// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EnemyControllerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTargetForEnemy, AActor*, NewTarget);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartSpawnEnemy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndSpawnEnemy);

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_API UEnemyControllerComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	UEnemyControllerComponent();

	// Delegate:
	UPROPERTY(BlueprintAssignable, Category = "EnemyController")
	FOnTargetForEnemy OnTargetForEnemy;

	UPROPERTY(BlueprintAssignable, Category = "EnemyController")
	FOnStartSpawnEnemy OnStartSpawnEnemy;

	UPROPERTY(BlueprintAssignable, Category = "EnemyController")
	FOnEndSpawnEnemy OnEndSpawnEnemy;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "EnemyController")
	void StartSignalForSpawn();
	UFUNCTION(BlueprintCallable, Category = "EnemyController")
	void EndSignalForSpawn();

	UFUNCTION(BlueprintCallable, Category = "EnemyController")
	void SetCurrentTarget(AActor* NewTarget);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "EnemyController")
	AActor* GetTargetForEnemy() const;

private:
	AActor* CurrentTargetForEnemy = nullptr;
};
