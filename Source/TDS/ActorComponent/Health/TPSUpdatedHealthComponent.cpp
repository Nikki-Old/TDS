// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/Health/TPSUpdatedHealthComponent.h"

UTPSUpdatedHealthComponent::UTPSUpdatedHealthComponent()
{
	// Set base value:
	bShieldIsRecovery = true;
	TimeToStartRecovery = 3.0f;
	HowOftenRecoveryShieldPoints = 1.0f;
	ShieldRecoveryValue = 1.0f;
}

void UTPSUpdatedHealthComponent::OwnerTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	ChangeShield(-Damage, DamageCauser);
}

void UTPSUpdatedHealthComponent::ChangeShield(const float Change, AActor* Causer)
{
	float SkippedChange = Change;

	// If Have ShieldPoints and Change is Damage...
	if (ShieldPoints > 0.0f && SkippedChange < 0)
	{
		if (ShieldPoints >= FMath::Abs(SkippedChange))
		{
			ShieldPoints += SkippedChange;
			SkippedChange = 0;

			SpawnDamageActor(ShieldDamageClass, Change);
		}
		else
		{
			SkippedChange -= ShieldPoints;
			SpawnDamageActor(ShieldDamageClass, -ShieldPoints);

			ShieldPoints = 0;
		}

		OnChangeShield.Broadcast(GetOwner(), ShieldPoints, Change);

		if (bShieldIsRecovery)
			DelayToRecoveryShield();
	}

	ChangeHealth(SkippedChange, Causer);
}

void UTPSUpdatedHealthComponent::DelayToRecoveryShield()
{
	if (!GetWorld())
		return;

	// Clear Current Recovery:
	GetWorld()->GetTimerManager().ClearTimer(TimerToRecoveryShieldPoints);

	// Start Delay:
	GetWorld()->GetTimerManager().SetTimer(TimerToStartRecoveryShield, this, &UTPSUpdatedHealthComponent::StartRecoveryShield, TimeToStartRecovery, false);
}

void UTPSUpdatedHealthComponent::StartRecoveryShield()
{
	if (!GetWorld())
		return;

	// Start Recovery:
	GetWorld()->GetTimerManager().SetTimer(TimerToRecoveryShieldPoints, this, &UTPSUpdatedHealthComponent::RecoveryShield, HowOftenRecoveryShieldPoints, true);
}

void UTPSUpdatedHealthComponent::RecoveryShield()
{
	if (ShieldPoints <= TPSConstant::MAX_SHIELD_POINTS)
	{
		ShieldPoints = FMath::Clamp(ShieldPoints + ShieldRecoveryValue, 0.0f, float(TPSConstant::MAX_SHIELD_POINTS));
		OnChangeShield.Broadcast(GetOwner(), ShieldPoints, ShieldRecoveryValue);
	}
	else
	{
		// Clear Current Recovery:
		GetWorld()->GetTimerManager().ClearTimer(TimerToRecoveryShieldPoints);

		// TO DO: create delegate and broadcst for animation - "Shield is FULL"
	}
}