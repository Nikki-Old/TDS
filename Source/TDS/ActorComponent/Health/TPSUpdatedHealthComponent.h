// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ActorComponent/Health/TPSHealthComponentBase.h"
#include "FuncLibrary/Constant/TPSConstant.h"
#include "TPSUpdatedHealthComponent.generated.h"

/**
 *	 Expanding the UTPSHealthComponent. Added Shield Logic:
 */

/** Delegate broadcast Shield change */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnChangeShield, AActor*, Owner, float, CurrentShieldPoints, float, Change);

UCLASS()
class TDS_API UTPSUpdatedHealthComponent : public UTPSHealthComponentBase
{
	GENERATED_BODY()
public:
	// Delegates:
	UPROPERTY(BlueprintAssignable, Category = "Shield")
	FOnChangeShield OnChangeShield;

	UTPSUpdatedHealthComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shield")
	bool bShieldIsRecovery = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shield", meta = (ClampMin = "0", UMin = "0", EditCondition = "bShieldIsRecovery"))
	float TimeToStartRecovery = 0.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shield", meta = (ClampMin = "0", UMin = "0", EditCondition = "bShieldIsRecovery"))
	float HowOftenRecoveryShieldPoints = 0.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shield", meta = (ClampMin = "1", UMin = "1", EditCondition = "bShieldIsRecovery"))
	float ShieldRecoveryValue = 0.0f;

protected:
	// Current Shield Points:
	UPROPERTY(EditDefaultsOnly, BlueprintGetter = GetShieldPoints, Category = "Shield")
	float ShieldPoints = TPSConstant::MAX_SHIELD_POINTS;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shield | Damage", meta = (EditCondition = "bIsNeedSpawnDamage"))
	TSubclassOf<ATPSDamageActorBase> ShieldDamageClass;

	virtual void OwnerTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:
	virtual void ChangeShield(const float Change, AActor* Causer);

	UFUNCTION(BlueprintCallable, Category = "Shield")
	float GetShieldPoints() const { return ShieldPoints; }

private:
	FTimerHandle TimerToStartRecoveryShield;
	FTimerHandle TimerToRecoveryShieldPoints;

	UFUNCTION()
	void DelayToRecoveryShield();

	UFUNCTION()
	void StartRecoveryShield();

	UFUNCTION()
	void RecoveryShield();
};
