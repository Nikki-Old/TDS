// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/TPSInventoryComponent.h"
#include "Game/WorldActor/Item/ItemBase.h"
#include "Interface/InteractInterface.h"
#include "Game/TPSGameInstance.h"
#include "FuncLibrary/Utility.h"
#include "UI/Inventory/DynamicInventoryWidgetBase.h"
#include "FuncLibrary/TPSClassLibrary.h"
#include "Game/TPSPlayerController.h"
#include "Game/PlayerState/TPSPlayerState.h"

#include "Blueprint/UserWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogInventory, All, All)

// Sets default values for this component's properties
UTPSInventoryComponent::UTPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
	// Base value:
}

// Called when the game starts
void UTPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	//TODO: add logic: take from save data info:
	// ...

	//InitWeaponSlots();
}

void UTPSInventoryComponent::InitWeaponSlots()
{
	if (!GetWorld())
		return;

	// Get Game Instance:
	GI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (!GI)
	{
		UE_LOG(LogInventory, Error, TEXT("UTPSInventoryComponent::InitWeaponSlots - GI Cast to UTPSGameInstance - fail!"));
		return;
	}

	// Get OwnerPawn:
	const auto OwnerPawn = GetOwner<APawn>();

	// We detemine who controls OwnerPawn:
	if (OwnerPawn)
	{
		// If not AI...
		const auto OwnerPlayerController = OwnerPawn->GetController<ATPSPlayerController>();
		if (OwnerPlayerController)
		{
			// Get Player State:
			OwnerPlayerState = OwnerPlayerController->GetPlayerState<ATPSPlayerState>();

			// Init Saved information:
			if (OwnerPlayerState)
			{
				// Get WeaponSlots information in Player State:
				WeaponSlots = OwnerPlayerState->GetWeaponSlots();

				// Get Ammo slots infromation in Player State:
				AmmoSlots = OwnerPlayerState->GetAmmoSlots();

				// If update weapon slot, switch weapon slot in Player State:
				OnUpdateWeaponSlot.AddDynamic(OwnerPlayerState, &ATPSPlayerState::SwitchWeaponSlot);
			}
		}
	}

	// TO DO: Sort weapons
	// TO DO: if not have weapon, to do ... 1. Set current index weapon is -1 2. Set base weapon, knife?
	// Find init WeaponSlots and first init Weapon:
	bool bIsSetFirstWeapon = false;
	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{

		if (!WeaponSlots[i].NameItem.IsNone())
		{
			FWeaponInfo Info;
			// Searh info from Table:
			if (GI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
			{
				//WeaponSlots[i].AdditionalInfo.CurrentRound = Info.MaxRounds;

				// Set First find weapon:
				if (!WeaponSlots[i].IsEmpty() && !bIsSetFirstWeapon)
				{
					bIsSetFirstWeapon = true;
					auto NewSwitchWeaponInfo = FSwitchWeaponInfo(WeaponSlots[i].NameItem, WeaponSlots[i].AdditionalInfo, i);
					OnSwitchWeapon.Broadcast(NewSwitchWeaponInfo);
				}
			}
		}
		else // ...if information is not found, that removes from array:
		{
			if (bRemoveClearWeaponSlots)
			{
				WeaponSlots.RemoveAt(i);
				i--;
			}
		}
	}

	// Set MaxSlot after cleaning WeaponArray:
	MaxWeaponSlots = WeaponSlots.Num();
}

int32 UTPSInventoryComponent::GetNumberOfWeapons() const
{
	int32 NumberOfWeapons = 0;

	for (auto& WeaponSlot : WeaponSlots)
	{
		if (!WeaponSlot.NameItem.IsNone())
		{
			++NumberOfWeapons;
		}
	}

	return NumberOfWeapons;
}

void UTPSInventoryComponent::AddAmmoSlot(EWeaponType AmmoType, int32 InitialCount)
{
	// TO DO: Need take MaxCount infromation in table:
	auto NewAmmoSlot = FAmmoSlot(AmmoType, InitialCount, 50);

	AmmoSlots.Add(NewAmmoSlot);

	if (OwnerPlayerState)
	{
		OwnerPlayerState->AddAmmoSlot(NewAmmoSlot);
	}
}

// Called every frame
void UTPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

int32 UTPSInventoryComponent::GetMaxWeaponSlots() const
{
	return MaxWeaponSlots;
}

bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	if (ChangeToIndex == OldIndex)
		return false;

	bool bIsSuccess = false;

	// ChagesToIndex - next index in Weapon array:
	int8 CorrectIndex = ChangeToIndex;

	// Information SwitchWeapon:
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;

	// Check: is can switch weapon on ChangeToIndex?
	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (CheckAmmoForWeapon(WeaponSlots[CorrectIndex])) // Have Ammo this weapon?
			{
				bIsSuccess = true;

				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}

	if (!bIsSuccess)
	{

		// In cases where CorretIndex more Array.Num:
		if (CorrectIndex > WeaponSlots.Num() - 1) // ChangeToIndex...
		{
			// ...set first index:
			CorrectIndex = -1;
		}
		else
		{
			if (CorrectIndex < 0) // ChangeToIndex...
			{
				// ...set last index:
				CorrectIndex = WeaponSlots.Num() - 1;
			}
		}

		int Step = bIsForward ? 1 : -1;
		int Trial = WeaponSlots.Num();

		// Start find next weapon:
		while (Trial != 0)
		{
			CorrectIndex = FMath::Clamp((CorrectIndex + Step) % WeaponSlots.Num(), 0, WeaponSlots.Num());

			if (WeaponSlots.IsValidIndex(CorrectIndex))
			{
				if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
				{
					if (CheckAmmoForWeapon(WeaponSlots[CorrectIndex])) // Have Ammo this weapon?
					{
						bIsSuccess = true;

						NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
						NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
						break;
					}
				}
			}

			Trial--;
		}
	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);

		// Speaks to subscribers new current weapon and Additional information:
		auto NewSwitchWeaponInfo = FSwitchWeaponInfo(NewIdWeapon, NewAdditionalInfo, CorrectIndex);
		OnSwitchWeapon.Broadcast(NewSwitchWeaponInfo);
	}
	else
	{
		UE_LOG(LogInventory, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex: FAIL"));
	}

	return bIsSuccess;
}

FAdditionalWeaponInfo UTPSInventoryComponent::GetAdditionalInfoWeaponByIndexSlot(int32 IndexWeapon) const
{
	FAdditionalWeaponInfo Result;

	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon) // WeaponSlots[i].IndexSlot == IndexWeapon
			{
				Result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogInventory, Warning, TEXT("UTPSInventoryComponent::GetAdditionalInfoWeapon - No Found Weapon with index - %d."),
				IndexWeapon);
		}
	}
	else
	{
		UE_LOG(
			LogInventory, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not correct index Weapon - %d."), IndexWeapon);
	}

	return Result;
}

int32 UTPSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName) const
{
	int32 Result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			Result = i; // WeaponSlots[i].IndexSlot
		}
		i++;
	}
	return Result;
}

FName UTPSInventoryComponent::GetWeaponNameByIndexSlot(int32 IndexSlot) const
{
	FName Result;

	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		Result = WeaponSlots[IndexSlot].NameItem;
	}
	else
	{
		UE_LOG(LogInventory, Error,
			TEXT("UTPSInventoryComponent::GetWeaponNameByIndexSlot: Index slot - %i in Array WeaponSlots is not valid!"), IndexSlot);
	}

	return Result;
}

void UTPSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon) // WeaponSlots[i].IndexSlot
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				// Speaks to subscribers weapon index and its new additional information:
				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}

		if (!bIsFind)
		{
			UE_LOG(LogInventory, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"),
				IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogInventory, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not correct index Weapon - %d"), IndexWeapon);
	}
}

void UTPSInventoryComponent::AmmoSlotChangeValue(EWeaponType Type, int32 CountChangeAmmo)
{
	bool bIsFind = false;
	int i = 0;
	while (i < AmmoSlots.Num())
	{
		if (AmmoSlots[i].WeaponType == Type)
		{
			AmmoSlots[i].Count += CountChangeAmmo;
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;

			if (OwnerPlayerState)
			{
				// Set new infromation in PlayerState:
				OwnerPlayerState->SetAmmoSlot(i, AmmoSlots[i]);
			}

			// Speaks to subscribers current rounds for this Type of Weapon:
			OnAmmoChange.Broadcast(Type, AmmoSlots[i].Count);
			bIsFind = true;
		}
		i++;
	}

	// If not find need AmmoType...
	if (!bIsFind)
	{
		AddAmmoSlot(Type, CountChangeAmmo);
	}
}

bool UTPSInventoryComponent::CheckAmmoForWeapon(EWeaponType WeaponType, int8& AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num())
	{
		if (AmmoSlots[i].WeaponType == WeaponType)
		{
			bIsFind = true;

			if (AmmoSlots[i].Count > 0)
			{
				AvailableAmmoForWeapon = AmmoSlots[i].Count;
				return bIsFind;
			}
		}
		i++;
	}

	// Speaks to subscribers: Weapon type has no ammo.
	OnWeaponAmmoIsEmpty.Broadcast(WeaponType);

	return bIsFind;
}

bool UTPSInventoryComponent::CheckAmmoForWeapon(FWeaponSlot Weapon)
{
	if (Weapon.AdditionalInfo.CurrentRound > 0)
	{
		return true;
	}
	else
	{
		if (!Weapon.NameItem.IsNone())
		{
			FWeaponInfo WeaponInfo;
			UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());

			if (myGI)
			{
				myGI->GetWeaponInfoByName(Weapon.NameItem, WeaponInfo);
			}
			else
			{
				UE_LOG(LogInventory, Error, TEXT("UTPSInventoryComponent::CheckAmmoForWeapon: Cast to GI - Fail!"));
				return false;
			}

			int8 i = 0;
			while (i < AmmoSlots.Num())
			{
				if (AmmoSlots[i].WeaponType == WeaponInfo.TypeWeapon && AmmoSlots[i].Count > 0)
				{
					return true;
				}
				i++;
			}
		}
		else
		{
			UE_LOG(LogInventory, Error, TEXT("UTPSInventoryComponent::CheckAmmoForWeapon: Weapon name - is none."));
		}
	}

	return false;
}

void UTPSInventoryComponent::DropWeapon(const int CurrentIndexWeapon, FTransform DropTransform, FVector ImpulseDirection, float ImpulseStrenght, bool bIsSwitch)
{
	if (!GetWorld() || !IsValid(WeaponDropClass))
		return;

	// Spawn Parameters:
	FVector SpawnLocation = DropTransform.GetLocation();
	FRotator SpawnRotation = DropTransform.GetRotation().Rotator();
	FActorSpawnParameters DropWeaponSpawnParameters;
	DropWeaponSpawnParameters.Owner = nullptr;
	DropWeaponSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	// Spawn WeaponDrom
	auto WeaponDrop = Cast<AItemBase>(GetWorld()->SpawnActor(WeaponDropClass, &SpawnLocation, &SpawnRotation, DropWeaponSpawnParameters));
	if (WeaponDrop)
	{
		FWeaponSlot CurrentWeaponSlot = WeaponSlots[CurrentIndexWeapon];
		WeaponDrop->SetWeaponDropInfo(CurrentWeaponSlot);
		WeaponDrop->InitialItem();

		// Add Impulse:
		auto WeaponPrimComp = Utils::GetTPSComponent<UPrimitiveComponent>(WeaponDrop);
		if (WeaponPrimComp)
		{
			WeaponPrimComp->AddImpulse((ImpulseDirection * ImpulseStrenght), FName(), true);
		}

		WeaponSlots[CurrentIndexWeapon].Clear();

		// Updated information:
		OnUpdateWeaponSlot.Broadcast(CurrentIndexWeapon, FWeaponSlot());

		if (bIsSwitch)
		{
			// Swap to Next Weapon:
			SwitchWeaponToIndex(CurrentIndexWeapon + 1, CurrentIndexWeapon, FAdditionalWeaponInfo(), true);
		}
	}
}

bool UTPSInventoryComponent::CheckCanTakeWeapon(const FName ItemName, int32& FreeSlot)
{
	bool Result = false;
	FreeSlot = -1;

	for (int32 IndexSlot = 0; IndexSlot < WeaponSlots.Num(); IndexSlot++)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			// if Current Slot is Empty...
			if (WeaponSlots[IndexSlot].NameItem.IsNone())
			{
				Result = true;

				// Set once FreeSlot:
				if (FreeSlot == -1)
				{
					FreeSlot = IndexSlot;
				}
			}
			else if (WeaponSlots[IndexSlot].NameItem == ItemName) // Check, have duplicate
			{
				return false;
			}
		}
	}

	return Result;
}

bool UTPSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType) const
{
	int8 i = 0;
	while (i < AmmoSlots.Num())
	{
		// Find need AmmoType and check count is not Max?
		if (AmmoSlots[i].WeaponType == AmmoType)
		{
			if (AmmoSlots[i].Count < AmmoSlots[i].MaxCount)
			{
				return true;
			}
			else
			{
				// if Ammo Count > MaxCount:
				return false;
			}
		}
		i++;
	}

	return true;
}

bool UTPSInventoryComponent::SwitchWeaponToInventory(
	FWeaponSlot NewWeapon, int32 IndexSlotChange, int32 CurrentIndexWeaponChar, FWeaponSlot& DropItemInfo)
{
	if (!GI)
		return false;

	// Get Weapon info:
	FWeaponInfo WeaponInfo;
	if (!GI->GetWeaponInfoByName(NewWeapon.NameItem, WeaponInfo))
		return false;

	if (WeaponSlots.IsValidIndex(IndexSlotChange) && GetDropWeaponInfoFromInventory(IndexSlotChange, DropItemInfo))
	{
		WeaponSlots[IndexSlotChange] = NewWeapon;

		SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);

		if (OwnerPlayerState)
		{
			OwnerPlayerState->SwitchWeaponSlot(IndexSlotChange, NewWeapon);
		}

		OnUpdateWeaponSlot.Broadcast(IndexSlotChange, NewWeapon);
		return true;
	}

	return false;
}

bool UTPSInventoryComponent::GetDropWeaponInfoFromInventory(const int32 IndexSlot, FWeaponSlot& DropItemInfo)
{
	if (!GetWorld())
		return false;

	if (!GI)
		return false;

	DropItemInfo.NameItem = GetWeaponNameByIndexSlot(IndexSlot);

	if (!DropItemInfo.NameItem.IsNone())
	{
		DropItemInfo.AdditionalInfo = GetAdditionalInfoWeaponByIndexSlot(IndexSlot);
		return true;
	}

	return false;
}

bool UTPSInventoryComponent::GetWeaponSlotToInventory(FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(NewWeapon.NameItem, IndexSlot))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			WeaponSlots[IndexSlot] = NewWeapon;

			OnUpdateWeaponSlot.Broadcast(IndexSlot, NewWeapon);
			return true;
		}
	}

	return false;
}

void UTPSInventoryComponent::SetVisibleDynamicInventoryWidget(bool IsVisible)
{
	if (IsVisible)
	{
		if (!DynamicInventoryWidget)
		{
			auto OwnerPlayerController = UTPSClassLibrary::GetTPSPlayerController(GetWorld());

			DynamicInventoryWidget = CreateWidget<UDynamicInventoryWidgetBase>(OwnerPlayerController, DynamicInventoryWidgetClass);
		}

		DynamicInventoryWidget->UnHide();
	}
	else
	{
		if (DynamicInventoryWidget)
			DynamicInventoryWidget->Hide();
	}
}
