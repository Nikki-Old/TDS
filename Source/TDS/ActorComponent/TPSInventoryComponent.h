// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "FuncLibrary/Types.h"
#include "FuncLibrary/Constant/TPSConstant.h"
#include "TPSInventoryComponent.generated.h"

/*
* 
*	This a actor component with inventory functionality.
*/

/** Delegates*/
// Create delegate to call on Character and transfer Weapon ID, WeaponAdditional:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(
	FOnSwitchWeapon, FSwitchWeaponInfo, SwitchWeaponInfo);

// Create delegate to call in here for Widgets, take AmmoType and Ammo count:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, AmmoType, int32, Count);

// Create delegate to call in here for Widgets, take Index Weapon slot and Additional Info:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexWeaponSlot, FAdditionalWeaponInfo, AdditionalInfo);

// Create delegate to call in here for Widgets, take Weapon type:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoIsEmpty, EWeaponType, WeaponType);

// Create delegate to call in Character for Widgets, tale Weapon type:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvailable, EWeaponType, WeaponType);

// Create delegate to call in HERE for Widgets, take Index weapon slot and New Weapon Slot information:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int32, IndexWeaponSlot, FWeaponSlot, NewWeaponSlotInformation);

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_API UTPSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTPSInventoryComponent();

	/** Delegates */

	// Notifies that Weapon has switch:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	FOnSwitchWeapon OnSwitchWeapon;

	// Notifies that Ammo has change:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoChange OnAmmoChange;

	// Notifies that Weapon Additional information has change:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;

	// Notifies that Weapon ammo is empty:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponAmmoIsEmpty OnWeaponAmmoIsEmpty;

	// Notifies that Weapon ammo is available:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponAmmoAvailable OnWeaponAmmoAvailable;

	// Notifies that Weapon ammo is empty:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	FOnUpdateWeaponSlot OnUpdateWeaponSlot;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** Weapons Infromation: */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory | Weapons")
	TArray<FWeaponSlot> WeaponSlots;

	/** Need to remove the empty weapon slots. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory | Weapons")
	bool bRemoveClearWeaponSlots = false;

	/** Ammo Information: */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory | Ammo")
	TArray<FAmmoSlot> AmmoSlots;

	/** Get Max Weapon Slots in inventory: */
	UFUNCTION(BlueprintPure, Category = "Inventory | Weapons")
	int32 GetMaxWeaponSlots() const;

	/** Switch Weapon */
	UFUNCTION(BlueprintCallable, Category = "Inventory | Weapons")
	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward);

	
	UFUNCTION()
	FAdditionalWeaponInfo GetAdditionalInfoWeaponByIndexSlot(int32 IndexWeapon) const;

	UFUNCTION()
	int32 GetWeaponIndexSlotByName(FName IdWeaponName) const;
	UFUNCTION()
	FName GetWeaponNameByIndexSlot(int32 IndexSlot) const;

	UFUNCTION()
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);

	// Change Value in Ammo Slot by Type:
	UFUNCTION(BlueprintCallable)
	void AmmoSlotChangeValue(EWeaponType Type, int32 CountChangeAmmo);

	// Take available ammo by type:
	// UFUNCTION()


	UFUNCTION()
	bool CheckAmmoForWeapon(EWeaponType WeaponType, int8& AvailableAmmoForWeapon);
	bool CheckAmmoForWeapon(FWeaponSlot Weapon);

	/** Drop weapon */
	UFUNCTION(BlueprintCallable, Category = "Inventory | Weapons")
	void DropWeapon(const int CurrentIndexWeapon, FTransform DropTransform, FVector ImpulseDirection, float ImpulseStrenght = 500.0f, bool bIsSwitch = true);

	/** Interface Pick up items: */
	UFUNCTION(BlueprintCallable, Category = "Inventory | Weapons")
	bool CheckCanTakeWeapon(const FName ItemName, int32& FreeSlot);

	UFUNCTION(BlueprintPure, Category = "Inventory | Ammo")
	bool CheckCanTakeAmmo(EWeaponType AmmoType) const;

	UFUNCTION(BlueprintCallable, Category = "Inventory | Weapons")
	bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlotChange, int32 CurrentIndexWeaponChar, FWeaponSlot& DropItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Inventory | Weapons")
	bool GetDropWeaponInfoFromInventory(const int32 IndexSlot, FWeaponSlot& DropItemInfo);

	UFUNCTION(BlueprintPure, Category = "Inventory | Weapons")
	bool GetWeaponSlotToInventory(FWeaponSlot NewWeapon);

	// Widgets:
	/** Widget class for spawn Dynamic Inventory: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory | Widgets")
	TSubclassOf<class UDynamicInventoryWidgetBase> DynamicInventoryWidgetClass;

	/** Dynamic Inventory Widget */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Inventory | Widgets")
	UDynamicInventoryWidgetBase* DynamicInventoryWidget = nullptr;

	UFUNCTION(BlueprintCallable, Category = "Inventory | Widgets")
	void SetVisibleDynamicInventoryWidget(bool IsVisible);

	/** Called in Owner: */
	UFUNCTION()
	void InitWeaponSlots();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	int32 GetNumberOfWeapons() const;

protected:
	/** Drop Weapon Class */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory | Weapons")
	TSubclassOf<class AItemBase> WeaponDropClass;

	void AddAmmoSlot(EWeaponType AmmoType, int32 InitialCount);

private:
	class UTPSGameInstance* GI = nullptr;
	int32 MaxWeaponSlots = TPSConstant::MAX_WEAPON_SLOTS;

	class ATPSPlayerState* OwnerPlayerState = nullptr;
};
