// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/Animation/Base/TPSAnimNotifyBase.h"

void UTPSAnimNotifyBase::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnNotifiedSignature.Broadcast(MeshComp);
	Super::Notify(MeshComp, Animation);
}
