// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "TPSAnimNotifyBase.generated.h"

/** Create delegate for notification: */
DECLARE_MULTICAST_DELEGATE_OneParam(FOnNotifiedSignature, USkeletalMeshComponent*);

/**
 *	Base anim notify.
 */

UCLASS()
class TDS_API UTPSAnimNotifyBase : public UAnimNotify
{
	GENERATED_BODY()

public:
	/** Delegate notification:*/
	FOnNotifiedSignature OnNotifiedSignature;

	/** Create virtual function: */
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
