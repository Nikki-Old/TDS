// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/Animation/Base/TPSAnimNotifyBase.h"
#include "TDSCanSetDamageAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTDSCanSetDamageAnimNotify : public UTPSAnimNotifyBase
{
	GENERATED_BODY()
	
};
