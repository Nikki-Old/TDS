// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base\TPSAnimNotifyBase.h"
#include "TPSRemoveWeaponAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTPSRemoveWeaponAnimNotify : public UTPSAnimNotifyBase
{
	GENERATED_BODY()
	
};
