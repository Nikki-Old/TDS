// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base/TPSAnimNotifyBase.h"
#include "TPSTakeWeaponAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTPSTakeWeaponAnimNotify : public UTPSAnimNotifyBase
{
	GENERATED_BODY()
	
};
