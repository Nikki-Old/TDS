// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PlayerCharacter.h"

#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"

// ActorComponent:
#include "ActorComponent/CustomMovementComponent/TPSCharacterMovementComponent.h"
#include "ActorComponent/TPSInventoryComponent.h"
#include "ActorComponent/TPSWeaponComponent.h"
#include "ActorComponent/Health/TPSUpdatedHealthComponent.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/Material.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

#include "Game/MySaveGameBase.h"
#include "FuncLibrary/AnimUtils.h"
#include "FuncLibrary/TPSClassLibrary.h"
#include "Game/TPSPlayerController.h"
#include "FuncLibrary/PrintString.h"

// Animation:
// Notify:
#include "Character\Animation\TPSRemoveWeaponAnimNotify.h"
#include "Character\Animation\TPSTakeWeaponAnimNotify.h"

// Create log for this class:
DEFINE_LOG_CATEGORY_STATIC(LogPlayerCharacter, All, All)

APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjInit)
	: Super(ObjInit.SetDefaultSubobjectClass<UTPSCharacterMovementComponent>(APlayerCharacter::CharacterMovementComponentName))
{
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

#pragma region Movement

	PlayerMovementComponent = Cast<UTPSCharacterMovementComponent>(GetMovementComponent());
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	GetCharacterMovement()->bOrientRotationToMovement = true;  // Rotate character to moving direction ?????
	GetCharacterMovement()->bOrientRotationToMovement = false; // ?????

#pragma endregion

#pragma region Camera

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

#pragma endregion

	// Create a InventoryComponent:
	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	// Binding to a delegate:
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &APlayerCharacter::SwitchWeapon);
		this->OnCharacterIsPossessed.AddDynamic(InventoryComponent, &UTPSInventoryComponent::InitWeaponSlots);
	}

	// Create a HealthComponent:
	HealthComponent = CreateDefaultSubobject<UTPSUpdatedHealthComponent>(TEXT("HealthComponent_Test"));
	// Binding to a delegate:
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &APlayerCharacter::CharacterDeath);
	}

	// Create a WeaponComponent:
	//WeaponComponet = CreateDefaultSubobject<UTPSWeaponComponent>(TEXT("WeaponComponent"));

	// Base:
	bIsStartReload = false;

	// Camera:
	bIsCanRotation = true;
	HeightCameraChangeDistance = 100.0f;
	HeightCameraMax = 1000.0f;
	HeightCameraMin = 500.0f;
	SlideStepDistance = 1.0f;
	SlideSmoothTimerStep = 0.01f;
	MinDistForOffset = 100.0f;
	MaxDistForOffset = 1000.0f;
	HowOftenUpdateCamOffsetTimer = 0.01f;
	DirectionForceCoeff = 0.5f;

	// Rotation:
	InterpSpeed = 1.f;
	HowOftenUpdateRotation = 0.01f;

	// Inputs:
	TimeOfPressingButton = 1.0f;
}

void APlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CursorTick(DeltaSeconds);
	WeaponTick(DeltaSeconds);

	// Check: Aim is enabled?
	if (PlayerMovementComponent->AimEnabled)
	{
		StartCameraAddOffset();
	}
}

void APlayerCharacter::WeaponTick(float DeltaSeconds)
{
	if (CurrentWeapon && PlayerMovementComponent)
	{
		// The Beginning vector of the firing vector...
		FVector Displacement = FVector(0);

		//...in dependence status movement, set.
		switch (PlayerMovementComponent->GetCurrentMovementState())
		{
			case ECharacterMovementState::Aim_State:
				Displacement = WeaponDisplacement.AimNormal;
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case ECharacterMovementState::AimWalk_State:
				Displacement = WeaponDisplacement.AimWalk;
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case ECharacterMovementState::Run_State:
				Displacement = WeaponDisplacement.RunNormal;
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case ECharacterMovementState::Walk_State:
				Displacement = WeaponDisplacement.WalkNormal;
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case ECharacterMovementState::SprintRun_State:
				break;
			default:
				break;
		}

		if (bIsAddDisplacement)
		{
			CurrentWeapon->ShootEndLocation = HitResultUnderCursor.Location + Displacement;
		}
		else
		{
			CurrentWeapon->ShootEndLocation = HitResultUnderCursor.Location;
		}
	}
}

bool APlayerCharacter::IsCanRotation() const
{
	return bIsCanRotation;
}

void APlayerCharacter::SetCanRotation(bool Can)
{
	bIsCanRotation = Can;
}

void APlayerCharacter::CursorTick(float DeltaSeconds)
{
	if (!PlayerController || !GetWorld() || !bIsCanRotation)
		return;

	FHitResult HitResultForCursor;

	// Start spawn Mouse LineTrace:
	PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResultUnderCursor);

	//// Check: Actor is valid?
	//if (HitResultUnderCursor.GetActor())
	//{
	//	// Get Collision Profile name:
	//	FName HitActorCollisionProfileName = HitResultUnderCursor.Component->GetCollisionProfileName();
	//
	//	// Check, if Name is "Elevation"...
	//	if (HitActorCollisionProfileName == "Elevation")
	//	{
	//		/** Result Hit Between Character and Hit under Cursor: */
	//		FHitResult HitResultCharCurs;
	//		TArray<AActor*> IgnoreActors{ this };
	//
	//		// Start Spawn line trace Between Character and Hit result under cursor:
	//		UKismetSystemLibrary::LineTraceSingle(GetWorld(), this->GetActorLocation(), HitResultUnderCursor.Location, ETraceTypeQuery::TraceTypeQuery1, false, IgnoreActors, EDrawDebugTrace::ForDuration, HitResultCharCurs, true);
	//
	//		// Set Hit result for cursor:
	//		HitResultForCursor = HitResultCharCurs;
	//
	//	}
	//	else
	//	{
	//		HitResultForCursor = HitResultUnderCursor;
	//	}
	//}

	// Set cursor location:
	if (CurrentCursor)
	{
		HitResultForCursor = HitResultUnderCursor;
		SetLocationCursor(HitResultForCursor);
	}

	// Set Character Rotation:
	if (HealthComponent && !HealthComponent->IsDead())
	{
		SetCharacterRotation();
	}
	else
	{
		AOPitch = 0.0f;
		AOYaw = 0.0f;
	}
}

void APlayerCharacter::SetCharacterRotation()
{
	if (CurrentCursor)
	{
		FVector ActorLocation = this->GetActorLocation();
		FVector CursorLocation = CurrentCursor->GetComponentLocation();

		/** Direction Chacter to Cursor: */
		DirCharToCursor = UKismetMathLibrary::GetDirectionUnitVector(ActorLocation, FVector(CursorLocation.X, CursorLocation.Y, ActorLocation.Z));

		/** Dot vectors: Character Forward Vector and Direction to Cursor: */
		float DotCharCursor = UKismetMathLibrary::DotProduct2D(FVector2D(this->GetActorForwardVector()), FVector2D(DirCharToCursor));

		/** Cross vectors: Character Forward Vector and Direction to Cursor: */
		FVector DerAcosCharCursor = UKismetMathLibrary::Cross_VectorVector(this->GetActorForwardVector(), DirCharToCursor);

		/** Look at rotation Character to Cursor: */
		FRotator LookRotationCharCursor = UKismetMathLibrary::FindLookAtRotation(ActorLocation, CursorLocation);

		/** Find Delta Rotation between Actor Location and Look at rotation Character to Cursor: */
		FRotator DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(GetActorRotation(), LookRotationCharCursor);

		// Set AimOffset:
		AOYaw = FMath::Clamp(DeltaRotation.Yaw, -90.f, 90.f);
		AOPitch = FMath::Clamp(LookRotationCharCursor.Pitch, -90.f, 90.f);

		// DEBAG:
		if (bIsShowDebugView && GetWorld())
		{
			// Draw forwardVector:
			UKismetSystemLibrary::DrawDebugArrow(GetWorld(), ActorLocation, ActorLocation + (GetActorForwardVector() * 500.0f), 15.0f, FLinearColor::Red, 0.0f, 20.0f);

			// Draw Vector between Actor and Cursor:
			UKismetSystemLibrary::DrawDebugArrow(GetWorld(), ActorLocation, ActorLocation + (DirCharToCursor * 250.0f), 15.0f, FLinearColor::Green, 0.0f, 10.0f);

			printf_k(0, "Dot: %f", DotCharCursor);
			printf_k(1, "Cross Z: %f", DerAcosCharCursor.Z);
			printf_k(2, "Velocity: %f", UKismetMathLibrary::Vector4_Size(GetVelocity()));
		}

		// Check: Character is moving?
		if (UKismetMathLibrary::Vector4_Size(GetVelocity()) <= 1.f)
		{
			// If not Moving:
			if (DotCharCursor < 0.f)
			{

				if (DerAcosCharCursor.Z > 0)
				{
					bIsTurnRight = true;
					bIsTurnLeft = false;
				}
				else
				{
					bIsTurnRight = false;
					bIsTurnLeft = true;
				}

				if (bIsShowDebugView)
				{
					printf_k(3, "IsTurnRight: %i	IsTurnLeft: %i", bIsTurnRight, bIsTurnLeft);
				}

				StartRotationCharacter(LookRotationCharCursor.Yaw);
			}
			else
			{
				StopRotationCharacter();
				bIsTurnRight = false;
				bIsTurnLeft = false;
			}
		}
		else
		{
			// If Character is moving:
			// Reset to Zero AimOffset Yaw:
			AOYaw = 0.0f;
			this->SetActorRotation(FQuat(FRotator(0.f, LookRotationCharCursor.Yaw, 0.f)));
			if (bIsShowDebugView)
			{
				print("Reset to Zero");
			}

			StopRotationCharacter();
			bIsTurnRight = false;
			bIsTurnLeft = false;
		}
	}
}
void APlayerCharacter::StartRotationCharacter(float NewRotation)
{
	if (!GetWorld())
		return;

	// Set Target rotation:
	TargetRotationYaw = NewRotation;

	if (!CharacterRotationTimer.IsValid())
		GetWorld()->GetTimerManager().SetTimer(CharacterRotationTimer, this, &APlayerCharacter::SetRotationYaw, HowOftenUpdateRotation, true);
}

void APlayerCharacter::StopRotationCharacter()
{
	if (!GetWorld() || !CharacterRotationTimer.IsValid())
		return;

	if (bIsShowDebugView)
	{
		print("Stop Rotation");
	}

	GetWorld()->GetTimerManager().ClearTimer(CharacterRotationTimer);
	TargetRotationYaw = 0.0f;
}

void APlayerCharacter::SetRotationYaw()
{
	auto CurrentRotation = this->GetActorRotation();
	auto NewRotation = UKismetMathLibrary::RInterpTo(CurrentRotation, FRotator(0.0f, TargetRotationYaw, 0.0f), HowOftenUpdateRotation, InterpSpeed);

	if (bIsShowDebugView)
	{
		printf_k(10, "CurrentRotation: %s	NewRotation: %s", *CurrentRotation.ToString(), *NewRotation.ToString());
	}

	this->SetActorRotation(NewRotation);
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Set Cursor material:
	if (CursorMaterial)
	{
		CurrentCursor =
			UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0), FRotator(0.f, -90.f, 0.f));
	}

	// Get Base classes:
	PlayerController = UTPSClassLibrary::GetTPSPlayerController(GetWorld());
	check(PlayerController);
	GameInstance = UTPSClassLibrary::GetTPSGameInstance(GetWorld());
	check(GameInstance);
}

void APlayerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorld()->GetTimerManager().ClearTimer(CameraSlideSmoothTimer);
	Super::EndPlay(EndPlayReason);
}

void APlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	// Broadcast that this character is Possessed By NewController:
	OnCharacterIsPossessed.Broadcast();
}

bool APlayerCharacter::GetWeapon_Implementation(AWeaponDefault*& Weapon)
{
	Weapon = GetCurrentWeapon();
	return Weapon ? true : false;
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	// Save and Load:
	//NewInputComponent->BindAction("SaveEvent", IE_Pressed, this, &APlayerCharacter::SaveGame);
	//NewInputComponent->BindAction("LoadSavingEvent", IE_Pressed, this, &APlayerCharacter::LoadGame);

	// Movement:
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &APlayerCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &APlayerCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("MovementModeChangeWalk"), IE_Pressed, this, &APlayerCharacter::ChangeWalkEventPressed);
	NewInputComponent->BindAction(TEXT("MovementModeChangeWalk"), IE_Released, this, &APlayerCharacter::ChangeWalEventReleased);

	NewInputComponent->BindAction(TEXT("DashEvent"), IE_Pressed, PlayerMovementComponent, &UTPSCharacterMovementComponent::Dash);

	NewInputComponent->BindAction(TEXT("MovementModeChangeSprint"), IE_Pressed, this, &APlayerCharacter::ChangeSprintEventPressed);
	NewInputComponent->BindAction(TEXT("MovementModeChangeSprint"), IE_Released, this, &APlayerCharacter::ChangeSprintEventReleased);

	// Camera:
	NewInputComponent->BindAxis(TEXT("MoveWheel"), this, &APlayerCharacter::StartCameraSlideSmooth);

	// Weapon:
	NewInputComponent->BindAction(TEXT("AimEvent"), IE_Pressed, this, &APlayerCharacter::AimEventPressed);
	NewInputComponent->BindAction(TEXT("AimEvent"), IE_Released, this, &APlayerCharacter::AimEventReleased);

	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Pressed, this, &APlayerCharacter::AttackEventPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Released, this, &APlayerCharacter::AttackEventReleased);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), IE_Pressed, this, &APlayerCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &APlayerCharacter::PressedSwitchNextWeaponButton);
	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Released, this, &APlayerCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), IE_Pressed, this, &APlayerCharacter::TrySwitchPreviosWeapon);

	NewInputComponent->BindAction(TEXT("DropWeaponEvent"), EInputEvent::IE_Pressed, this, &APlayerCharacter::DropCurrentWeapon);
}

void APlayerCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void APlayerCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

/*
void APlayerCharacter::SaveGame()
{
	// Create an instance of our SaveGame class:
	UMySaveGameBase* SaveGameInstance = Cast<UMySaveGameBase>(UGameplayStatics::CreateSaveGameObject(UMySaveGameBase::StaticClass()));
	// Set the SaveGame instance location equal to the Players current location:
	SaveGameInstance->PlayerPosition = this->GetActorLocation();
	// Save the SaveGameInstance:
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("MySlot"), 0);
	// Log a message to show saved the game:
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Game Saved."));
}

void APlayerCharacter::LoadGame()
{
	// Create an instance of our SaveGame class:
	UMySaveGameBase* SaveGameInstance = Cast<UMySaveGameBase>(UGameplayStatics::CreateSaveGameObject(UMySaveGameBase::StaticClass()));
	// Load the saved game into our SaveGameInstance variable:
	SaveGameInstance = Cast<UMySaveGameBase>(UGameplayStatics::LoadGameFromSlot("MySlot", 0));
	// Set the player posirion from saved game file:
	this->SetActorLocation(SaveGameInstance->PlayerPosition);
	// Log a message to show we have loaded the game:
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Game Loaded."));
	}
}
*/

void APlayerCharacter::SetLocationCursor_Implementation(FHitResult HitResult)
{
	if (CurrentCursor && HitResult.bBlockingHit)
	{
		FVector CursorLocation = HitResult.Location;
		FQuat CursorRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
		CurrentCursor->SetWorldLocationAndRotation(CursorLocation, CursorRotation);
	}
}

void APlayerCharacter::ChangeWalkEventPressed()
{
	if (!PlayerMovementComponent && !GetController())
		return;

	PlayerMovementComponent->WalkEnabled = true;
	OnChangeMovementState.Broadcast();
}

void APlayerCharacter::ChangeWalEventReleased()
{
	if (!PlayerMovementComponent && !GetController())
		return;

	PlayerMovementComponent->WalkEnabled = false;
	OnChangeMovementState.Broadcast();
}

void APlayerCharacter::ChangeSprintEventPressed()
{
	if (!PlayerMovementComponent && !GetController())
		return;

	PlayerMovementComponent->bIsPressesSprint = true;
	OnChangeMovementState.Broadcast();
}

void APlayerCharacter::ChangeSprintEventReleased()
{
	if (!PlayerMovementComponent && !GetController())
		return;

	PlayerMovementComponent->bIsPressesSprint = false;
	PlayerMovementComponent->SprintRunEnabled = false;
	OnChangeMovementState.Broadcast();
}

void APlayerCharacter::AimEventPressed()
{
	if (!PlayerMovementComponent && !GetController())
		return;

	PlayerMovementComponent->AimEnabled = true;
	OnChangeMovementState.Broadcast();
}

void APlayerCharacter::AimEventReleased()
{
	if (!PlayerMovementComponent && !GetController())
		return;

	PlayerMovementComponent->AimEnabled = false;
	StopCameraAddOffset();
	OnChangeMovementState.Broadcast();
}

void APlayerCharacter::AttackEventPressed()
{
	if (GetController())
		AttackCharEvent(true);
}

void APlayerCharacter::AttackEventReleased()
{
	if (GetController())
		AttackCharEvent(false);
}

void APlayerCharacter::CountingTimePressingSwitchButton()
{
	CurrentTimePressingButton += 0.01f;
	if (bShowDebugInputs)
		printf_k(0, "CurrentTimePressingButton: %f", CurrentTimePressingButton);

	if (CurrentTimePressingButton >= TimeOfPressingButton)
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(PressingSwitchWeaponTimer);

			bIsSelectedWeaponInWidgetInventory = true;
			//
			TimeOfPressingButtonIsOver();
		}
	}
}

void APlayerCharacter::StartCameraSlideSmooth(float Axis)
{
	// Check: Maybe is work, World is valid and Axis ~= 0.0f?
	if (!SlideDone || !GetWorld() || FMath::IsNearlyZero(Axis, 0.01f) || PlayerMovementComponent->AimEnabled)
		return;

	auto CurrentArmLength = CameraBoom->TargetArmLength;

	// Up:
	if (Axis > 0.0f && CurrentArmLength + HeightCameraChangeDistance <= HeightCameraMax)
	{
		SlideUp = true;
		SlideDone = false;
		GetWorldTimerManager().SetTimer(CameraSlideSmoothTimer, this, &APlayerCharacter::CameraSlideSmooth, SlideSmoothTimerStep, true);
	}

	// Down:
	if (Axis < 0.0f && CurrentArmLength - HeightCameraChangeDistance >= HeightCameraMin)
	{
		SlideUp = false;
		SlideDone = false;
		GetWorldTimerManager().SetTimer(CameraSlideSmoothTimer, this, &APlayerCharacter::CameraSlideSmooth, SlideSmoothTimerStep, true);
	}
}

void APlayerCharacter::CameraSlideSmooth()
{
	// CurrentSlideDistane - Target and Add step distance:
	CurrentSlideDistance += SlideStepDistance;
	// Get Current CameraBoom length:
	auto CurrentArmLength = CameraBoom->TargetArmLength;
	// Determining which way to move with SlideUp:
	CameraBoom->TargetArmLength = SlideUp ? SlideStepDistance + CurrentArmLength : CurrentArmLength - SlideStepDistance;
	// Target is completed?
	if (CurrentSlideDistance >= HeightCameraChangeDistance)
	{
		SlideDone = true;
		CurrentSlideDistance = 0.0f;
		GetWorldTimerManager().ClearTimer(CameraSlideSmoothTimer);
	}
}
void APlayerCharacter::StartCameraAddOffset()
{
	if (!GetWorld())
		return;

	if (!CameraAddOffsetTimer.IsValid())
	{
		// Save OldTargetArmLength and TargetOffset:
		OldTargetArmLength = CameraBoom->TargetArmLength;
		OldTargetOffset = CameraBoom->TargetOffset;

		GetWorld()->GetTimerManager().SetTimer(CameraAddOffsetTimer, this, &APlayerCharacter::CameraAddOffset, HowOftenUpdateCamOffsetTimer, true);
	}
}

void APlayerCharacter::CameraAddOffset()
{
	auto CursorLocation = GetCursorToWorld()->GetComponentLocation();

	// Take Distance between Character and Cursor:
	float DistCharCursor = UKismetMathLibrary::Distance2D(FVector2D(GetActorLocation()), FVector2D(CursorLocation));

	// DEBUG:
	if (bIsShowDebugCameraOffset)
	{
		printf_k(0, "TargetArmLength (Char->Camera): %f", CameraBoom->TargetArmLength);
		printf_k(1, "Distance (Char->Cursor): %f", DistCharCursor);
		printf_k(2, "Old Distance: %f", OldDistanceLength);
	}

	if (DistCharCursor >= MinDistForOffset && DistCharCursor <= MaxDistForOffset)
	{
		bool bIsDistanceFromCharacter = OldDistanceLength > DistCharCursor ? true : false;

		float CoefLenght = DistCharCursor / MaxDistForOffset;
		float NewTargetArmLength = FMath::Clamp(CoefLenght * HeightCameraMax, HeightCameraMin, HeightCameraMax);

		// DEBUG:
		if (bIsShowDebugCameraOffset)
		{
			printf_k(3, "bIsDistanceFromCharacter: %i", bIsDistanceFromCharacter);
			printf_k(4, "CoefLenght: %f", CoefLenght);
			printf_k(4, "NewTargetArmLength: %f", NewTargetArmLength);
		}

		// Set Target Arm Length:
		if (CameraBoom->TargetArmLength > NewTargetArmLength && bIsDistanceFromCharacter)
		{
			CameraBoom->TargetArmLength = NewTargetArmLength;
		}

		if (CameraBoom->TargetArmLength < NewTargetArmLength && !bIsDistanceFromCharacter)
		{
			CameraBoom->TargetArmLength = NewTargetArmLength;
		}

		// Set Offset:
		FVector OffsetDir = FVector(0);

		if (!bIsDistanceFromCharacter)
		{
			auto VectorBetweenCharCursor = GetActorLocation() + (UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(), CursorLocation) * DistCharCursor);
			auto MiddleBetweenCharCursor = (GetActorLocation() + VectorBetweenCharCursor) / 2;

			// Find Direction from Character to Middle:
			auto DirCharMiddle = UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(), MiddleBetweenCharCursor);
			OffsetDir = DirCharMiddle;
		}

		if (bIsDistanceFromCharacter)
		{
			auto VectorBetweenCursorChar = GetActorLocation() + (UKismetMathLibrary::GetDirectionUnitVector(CursorLocation, GetActorLocation()) * DistCharCursor);
			auto MiddleBetweenCursorChar = (GetActorLocation() + VectorBetweenCursorChar) / 2;

			// Find Direction from Middle to Character:
			auto DirMiddleChar = UKismetMathLibrary::GetDirectionUnitVector(MiddleBetweenCursorChar, GetActorLocation());
			OffsetDir = DirMiddleChar;
		}

		CameraBoom->TargetOffset = OffsetDir * (DistCharCursor * DirectionForceCoeff);
	}

	OldDistanceLength = DistCharCursor;
}

void APlayerCharacter::StopCameraAddOffset()
{
	// Return to Old params before Aim enabled:
	CameraBoom->TargetArmLength = OldTargetArmLength;
	CameraBoom->TargetOffset = OldTargetOffset;

	if (CameraAddOffsetTimer.IsValid() && GetWorld())
		GetWorld()->GetTimerManager().ClearTimer(CameraAddOffsetTimer);
}

UDecalComponent* APlayerCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void APlayerCharacter::AttackCharEvent(bool bIsFire)
{
	// Call on delegate:
	OnStartAttack.Broadcast(bIsFire);

	if (CurrentWeapon)
	{
		CurrentWeapon->SetWeaponStateFire(bIsFire);
	}
	else
	{
		UE_LOG(LogPlayerCharacter, Warning, (TEXT("ATPSChararcter::AttachCharEvent - CurrentWeapon - NULL")));
	}
}

void APlayerCharacter::InitWeapon(FSwitchWeaponInfo NewSwitchWeaponInfo)
{

	if (!GameInstance)
	{
		GameInstance = UTPSClassLibrary::GetTPSGameInstance(GetWorld());
	}

	if (GameInstance)
	{

		FWeaponInfo myWeaponInfo;

		if (GameInstance->GetWeaponInfoByName(NewSwitchWeaponInfo.IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParam;
				SpawnParam.SpawnCollisionHandlingOverride =
					ESpawnActorCollisionHandlingMethod::AlwaysSpawn; // Method for resolving collisions at the spawn point.
				SpawnParam.Owner = this;							 // Owner this actor.
				SpawnParam.Instigator = this;						 // Instigator(��������) this actor.

				// Spawn weapon:
				AWeaponDefault* myWeapon =
					Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParam));
				if (myWeapon)
				{
					// Rules for attaching component's:
					FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rules, FName(myWeaponInfo.WeaponSocket));

					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->AdditionalWeaponInfo.CurrentRound = myWeaponInfo.MaxRounds;

					myWeapon->AdditionalWeaponInfo = NewSwitchWeaponInfo.AdditionalWeaponInfo;

					// Binding to delegates:
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &APlayerCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &APlayerCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &APlayerCharacter::WeaponFireStart);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
					{
						//CurrentIndexWeapon = NewSwitchWeaponInfo.NewCurrentIndexWeapon;
						CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(NewSwitchWeaponInfo.IdWeaponName);
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSetting.TypeWeapon);
					}

					SwitchWeaponInfo = NewSwitchWeaponInfo;
					OnWeaponIsSwitch.Broadcast(NewSwitchWeaponInfo);
					//NewSwitchWeaponInfo.Clear();
				}
			}
		}
		else
		{
			UE_LOG(LogPlayerCharacter, Warning, (TEXT("ATPSChararcter::InitWeapon - WeaponClass not found in Table - NULL")));
		}
	}
}

void APlayerCharacter::PressedSwitchNextWeaponButton()
{
	if (!GetWorld() || !HealthComponent || HealthComponent->IsDead())
	{
		return;
	}
	// Clear Time:
	CurrentTimePressingButton = 0.0f;
	print("PressedSwitchNextWeaponButton");

	GetWorld()->GetTimerManager().SetTimer(PressingSwitchWeaponTimer, this, &APlayerCharacter::CountingTimePressingSwitchButton, 0.01f, true);
}

void APlayerCharacter::TrySwitchNextWeapon()
{
	print("TrySwitchNextWeapon");

	if (GetWorld())
	{
		if (InventoryComponent)
		{
			InventoryComponent->SetVisibleDynamicInventoryWidget(false);
		}

		GetWorld()->GetTimerManager().ClearTimer(PressingSwitchWeaponTimer);
	}

	if (bIsSelectedWeaponInWidgetInventory)
	{
		bIsSelectedWeaponInWidgetInventory = false;
		return;
	}

	if (InventoryComponent->GetNumberOfWeapons() > 1)
	{
		// We have more then one weapon go switch:
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			// Need Timer to switch with Anim
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void APlayerCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		// We have more then one weapon go switch:
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			// Need Timer to switch with Anim
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void APlayerCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		InventoryComponent->DropWeapon(CurrentIndexWeapon, GetActorTransform(), DirCharToCursor);
	}
}

void APlayerCharacter::TimeOfPressingButtonIsOver_Implementation()
{
}

void APlayerCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRounds && CurrentWeapon->CheckCanWeaponReload())
		{
			if (!bIsStartReload)
				CurrentWeapon->InitReload();
		}
	}
}

void APlayerCharacter::SwitchWeapon(FSwitchWeaponInfo NewSwitchWeaponInfo)
{
	// Check: need play remove weapon animation?
	if (!StartRemoveWeapon())
	{
		// No:

		if (CurrentWeapon)
			CurrentWeapon->Destroy();

		// Init New Weapon:
		InitWeapon(NewSwitchWeaponInfo);
	}
	else
	{
		// Yes, save init values:
		SwitchWeaponInfo = NewSwitchWeaponInfo;
	}
}

void APlayerCharacter::StartAnimRemoveWeapon_BP_Implementation(UAnimMontage* Anim)
{
}

FSwitchWeaponInfo APlayerCharacter::GetLastSwitchWeaponInfo() const
{

	return SwitchWeaponInfo;
}

bool APlayerCharacter::StartRemoveWeapon()
{
	if (!CurrentWeapon || !CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimWeaponRemoveWeapon)
		return false;

	// Take Current Anim montage:
	auto CurrentAnim = CurrentWeapon->WeaponSetting.AnimWeaponInfo.AnimWeaponRemoveWeapon;
	// Take Notify this anim monatge:
	auto RemoveNotify = AnimUtils::FindNotifyByClass<UTPSRemoveWeaponAnimNotify>(CurrentAnim);

	// For blueprints:
	StartAnimRemoveWeapon_BP(CurrentAnim);

	if (RemoveNotify)
	{
		// Set Block fire:
		CurrentWeapon->BlockFire = true;

		RemoveNotify->OnNotifiedSignature.AddUObject(this, &APlayerCharacter::OnRemoveWeapon);
		return true;
	}

	return false;
}

void APlayerCharacter::OnRemoveWeapon(USkeletalMeshComponent* MeshComp)
{
	if (MeshComp != this->GetMesh())
		return;

	if (CurrentWeapon)
		CurrentWeapon->Destroy();

	InitWeapon(SwitchWeaponInfo);
}

void APlayerCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (!bIsStartReload)
		if (InventoryComponent && CurrentWeapon)
		{
			InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
			WeaponFireStart_BP(Anim);
		}
}

void APlayerCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// In BP.
}

void APlayerCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	if (InventoryComponent)
	{
		int8 AvailableAmmoForWeapon = 0;
		InventoryComponent->CheckAmmoForWeapon(CurrentWeapon->WeaponSetting.TypeWeapon, AvailableAmmoForWeapon);

		if (AvailableAmmoForWeapon <= 0)
			return;
	}

	bIsStartReload = true;

	WeaponReloadStart_BP(Anim);
}

void APlayerCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// In BP.
}

void APlayerCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	bIsStartReload = false;
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.TypeWeapon, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void APlayerCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// In BP.
}

void APlayerCharacter::CharacterDeath_Implementation()
{

	Super::CharacterDeath_Implementation();

	// DropWeapon:
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->DropWeapon(CurrentIndexWeapon, CurrentWeapon->GetActorTransform(), FVector(0), 0.0f, false);
		CurrentWeapon->Destroy();
	}

	if (PlayerController)
	{
		DisableInput(PlayerController);
		PlayerController->PlayerIsDead();
	}

	// Off Attack:
	AttackCharEvent(false);

	// Off Collision:
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
}

AWeaponDefault* APlayerCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}
