// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FuncLibrary/Types.h"
#include "Game/TPSGameInstance.h"
#include "Game/Weapon/WeaponDefault.h"
#include "Character/TDSCharacterBase.h"
#include "Interface/WeaponInterface.h"

#include "PlayerCharacter.generated.h"

/** We will find out from delegate when weapon is switched. Used for Current Weapon Widget. */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponIsSwitch, FSwitchWeaponInfo, NewSwitchWeaponInfo);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterIsPossessed);

class UTPSCharacterMovementComponent;

UCLASS(Blueprintable)
class APlayerCharacter : public ATDSCharacterBase, public IWeaponInterface, public IInteractInterface
{
	GENERATED_BODY()

public:
	APlayerCharacter(const FObjectInitializer& ObjInit);

	FOnCharacterIsPossessed OnCharacterIsPossessed;
#pragma region BaseFunction
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);

	virtual void PossessedBy(AController* NewController);
#pragma endregion

#pragma region Component
public:
	/** Movement: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	UTPSCharacterMovementComponent* PlayerMovementComponent = nullptr;

	/** Inventory: */
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Components")
	class UTPSInventoryComponent* InventoryComponent = nullptr;

	/** Health Component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	class UTPSUpdatedHealthComponent* HealthComponent = nullptr;

	/** Weapon Component: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	class UTPSWeaponComponent* WeaponComponet = nullptr;

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

#pragma endregion

/* private:
	UFUNCTION()
	void SaveGame();

	UFUNCTION()
	void LoadGame();
	*/

// Interface:
#pragma region WeaponInterface

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	bool GetWeapon(AWeaponDefault*& Weapon);
	bool GetWeapon_Implementation(AWeaponDefault*& Weapon);

#pragma endregion

#pragma region Cursor

protected:
	/** Cursor: */
	void CursorTick(float DeltaSeconds);

	// For cursor setting
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(10.0f, 20.0f, 20.0f);
	UPROPERTY()
	UDecalComponent* CurrentCursor = nullptr; // Cursor to world.

	/** Cursor: */
	UFUNCTION(BlueprintNativeEvent, Category = "Cursor")
	void SetLocationCursor(FHitResult HitResult);
	void SetLocationCursor_Implementation(FHitResult HitResult);

	FHitResult HitResultUnderCursor = FHitResult();

public:
	/** Cursor: */
	// Take cursor:
	/** Returns CursorToWorld subobject **/
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UDecalComponent* GetCursorToWorld();

#pragma endregion

#pragma region Rotation
	// Rotation:

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Rotation")
	bool IsCanRotation() const;

	UFUNCTION(BlueprintCallable, Category = "Rotation")
	void SetCanRotation(bool Can);

	bool bIsCanRotation = false;

	/** Character Rotation */
	void SetCharacterRotation();

	/** Turn right */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Rotation")
	bool bIsTurnRight = false;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Rotation")
	bool bIsTurnLeft = false;

	/** How ofter update timer CharacterRotationTimer. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Rotation", meta = (ClampMin = "0.01", UMin = "0.01", ClamMax = "3.0", UMax = "3.0"))
	float HowOftenUpdateRotation = 0.0f;

	/** Iterpolation speed, if the speed given is 0, then jump to the target: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Rotation", meta = (ClampMin = "0", UMin = "0"))
	float InterpSpeed = 0.0f;

	/** Turn left */
	/** Aim Offset Yaw: */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Rotation | AimOffset")
	float AOYaw = 0.0f;
	/** Aim Offset Pitch: */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Rotation | AimOffset")
	float AOPitch = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Rotation")
	bool bIsShowDebugView = true;

private:
	/** Target for rotation Character: */
	float TargetRotationYaw = 0.0f;

	FTimerHandle CharacterRotationTimer;

	void StartRotationCharacter(float NewRotation);
	void StopRotationCharacter();
	UFUNCTION()
	void SetRotationYaw();
#pragma endregion

#pragma region Weapon
private:
	/** Weapon: */
	void WeaponTick(float DeltaSeconds);

	// Weapon:
	UPROPERTY()
	AWeaponDefault* CurrentWeapon = nullptr;

	FWeaponDisplacement WeaponDisplacement;

public:
	/** Weapon: */
	/** Broadcast if weapon is Switch. */
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
	FOnWeaponIsSwitch OnWeaponIsSwitch;

	/**Added displacement to Shoot End Location: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	bool bIsAddDisplacement = false;

	// Checking have Weapon
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FSwitchWeaponInfo NewSwitchWeaponInfo);

	// Take weapon:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponDefault* GetCurrentWeapon();

	/** Reload: */
	// Start logic reloading in Weapon class.
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();

	/** Start switch weapon: */
	UFUNCTION()
	void SwitchWeapon(FSwitchWeaponInfo NewSwitchWeaponInfo);

	// For calling in blueprints:
	UFUNCTION(BlueprintNativeEvent)
	void StartAnimRemoveWeapon_BP(UAnimMontage* Anim);
	void StartAnimRemoveWeapon_BP_Implementation(UAnimMontage* Anim);

	// Get Last information:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon")
	FSwitchWeaponInfo GetLastSwitchWeaponInfo() const;

private:
	/** If Start Remove Weapon, save init values: */
	FSwitchWeaponInfo SwitchWeaponInfo;
	// First:
	bool StartRemoveWeapon();
	// Finish:
	void OnRemoveWeapon(USkeletalMeshComponent* MeshComp);

public:
	// Need for switch weapons in InventoryComponent:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CurrentWeapon")
	int32 CurrentIndexWeapon;

	// Take name from table Weapon's ana initialed this
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CurrentWeapon")
	FName InitWeaponName;

	/** Reload: */
	// For reload animation:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CurrentWeapon")
	bool bIsStartReload = true;

	// Debug:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
#pragma endregion

	/** Inputs: */
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inputs")
	bool bShowDebugInputs = false;

protected:
	// Input's:
	UFUNCTION()
	void InputAxisY(float Value); //...movement.
	UFUNCTION()
	void InputAxisX(float Value); //...movement.

	UFUNCTION()
	void ChangeWalkEventPressed(); //...Walk state.
	UFUNCTION()
	void ChangeWalEventReleased(); //...Walk state.
	UFUNCTION()
	void ChangeSprintEventPressed(); //...Sprint state.
	UFUNCTION()
	void ChangeSprintEventReleased(); //...Sprint state.
	UFUNCTION()
	void AimEventPressed(); //..Aim state.
	UFUNCTION()
	void AimEventReleased(); //..Aim state.
	UFUNCTION()
	void AttackEventPressed(); //...Attack.
	UFUNCTION()
	void AttackEventReleased(); //...Attack.

	/** How much pressing button "SwitchNextWeapon: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inputs")
	float TimeOfPressingButton = 0.0f;

	/** If true, not switch weapon. */
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Inputs")
	bool bIsSelectedWeaponInWidgetInventory = false;

	UFUNCTION()
	void PressedSwitchNextWeaponButton();

	UFUNCTION()
	void TrySwitchNextWeapon(); //...Switch weapon.
	UFUNCTION()
	void TrySwitchPreviosWeapon(); //...Switch weapon.

	UFUNCTION()
	void DropCurrentWeapon();

	/** Call this event if Time if pressing button "SwitchNextWeapon" is over*/
	UFUNCTION(BlueprintNativeEvent, Category = "Inputs")
	void TimeOfPressingButtonIsOver();
	void TimeOfPressingButtonIsOver_Implementation();

private:
	/** Timer to count amount pressing button "SwitchNextWeapon" */
	FTimerHandle PressingSwitchWeaponTimer;

	/** Current time pressed button "SwitchNextWeapon" */
	float CurrentTimePressingButton = 0.0f;

	UFUNCTION()
	void CountingTimePressingSwitchButton();

	/** Camera logic: */
private:
	void StartCameraSlideSmooth(float Axis);

	UFUNCTION()
	void CameraSlideSmooth();

	/** Sttoper, if Slide is work: */
	bool SlideDone = true;
	/** Direction flag: */
	bool SlideUp = true;

	float CurrentSlideDistance = 0.0f;
	/** Timer for CameraSlideSmooth: */
	FTimerHandle CameraSlideSmoothTimer;

public:
	/** Change distnace from one action: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | Slide ", meta = (ClampMin = "0", UMin = "0"))
	float HeightCameraChangeDistance = 0.0f;
	/** One step Lenght: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | Slide ", meta = (ClampMin = "0", UMin = "0"))
	float SlideStepDistance = 0.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | Slide ", meta = (ClampMin = "0", UMin = "0"))
	float SlideSmoothTimerStep = 0.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | Slide ", meta = (ClampMin = "0", UMin = "0"))
	float HeightCameraMax = 0.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | Slide ", meta = (ClampMin = "0", UMin = "0"))
	float HeightCameraMin = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | AddOffset")
	bool bIsShowDebugCameraOffset;
	/** Min distance for start offset: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | AddOffset")
	float MinDistForOffset = 0.0f;
	/** Max distance for start offset: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | AddOffset")
	float MaxDistForOffset = 0.0f;
	/** How often update CameraAddOffsetTimer */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | AddOffset", meta = (ClampMin = "0.001", UMin = "0.001"))
	float HowOftenUpdateCamOffsetTimer = 0.0f;
	/** Ñoefficient of the direction force by which the length is multiplied */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera | AddOffset", meta = (ClampMin = "0.001", UMin = "0.001"))
	float DirectionForceCoeff = 0.0f;

private:
	UFUNCTION()
	void StartCameraAddOffset();
	UFUNCTION()
	void StopCameraAddOffset();
	UFUNCTION()
	void CameraAddOffset();

	/** Timer */
	FTimerHandle CameraAddOffsetTimer;

	/** CurrentTargetArmLength */
	float OldDistanceLength = 0.f;

	/** TargetArmLenght before Aim enabled */
	float OldTargetArmLength = 0.f;
	/** TargetOffset before Aim enabled */
	FVector OldTargetOffset = FVector(0.f);

private:
public:
	/** Attack: */
	// Character Attack logic:
	UFUNCTION()
	void AttackCharEvent(bool bIsFire);

	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);
	void WeaponFireStart_BP_Implementation(UAnimMontage* Anim);

	// Reload:
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	void WeaponReloadEnd_BP_Implementation(bool bIsSuccess);

	virtual void CharacterDeath_Implementation() override;

private:
	class ATPSPlayerController* PlayerController = nullptr;
	class UTPSGameInstance* GameInstance = nullptr;

	FVector DirCharToCursor = FVector();
};
