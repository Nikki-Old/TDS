// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/TDSCharacterBase.h"
#include "Components/CapsuleComponent.h"

// ActorComponent:
#include "ActorComponent/CustomMovementComponent/TDSMovementComponentBase.h"

// Animation:
// Notify:
#include "Character\Animation\TPSDeathAnimIsEndNotify.h"

#include "FuncLibrary/AnimUtils.h"

// Sets default values
ATDSCharacterBase::ATDSCharacterBase(const FObjectInitializer& ObjInit)
	: Super(ObjInit.SetDefaultSubobjectClass<UTDSMovementComponentBase>(ATDSCharacterBase::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

#pragma region Movement
	// Configuration character movement:
	//CustomMovementComponent = Cast<UTDSMovementComponentBase>(GetMovementComponent());

	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction

#pragma endregion
}

UPawnMovementComponent* ATDSCharacterBase::GetMovementComponent() const
{
	return FindComponentByClass<UTDSMovementComponentBase>();
}

//UTDSMovementComponentBase* ATDSCharacterBase::GetCustomCharacterMovement()
//{
//	if (CustomMovementComponent)
//	{
//		return CustomMovementComponent;
//	}
//	else
//	{
//		CustomMovementComponent = Cast<UTDSMovementComponentBase>(GetMovementComponent());
//		return CustomMovementComponent;
//	}
//}

void ATDSCharacterBase::SetTarget_Implementation(AActor* Target)
{
	CurrentTarget = Target;
}

AActor* ATDSCharacterBase::GetTarget_Implementation() const
{
	return CurrentTarget;
}

bool ATDSCharacterBase::IsCanSetStateEffect_Implementation()
{
	return bIsCanSetState;
}

TArray<UStateEffect*> ATDSCharacterBase::GetStateEffects_Implementation() const
{
	return StateEffects;
}

bool ATDSCharacterBase::AddStateEffect_Implementation(UStateEffect* NewStateEffect)
{
	return NewStateEffect ? true : false;
}

bool ATDSCharacterBase::RemoveStateEffect_Implementation(UStateEffect* StateEffect)
{
	if (StateEffects.Find(StateEffect))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ATDSCharacterBase::StartRagDoll(USkeletalMeshComponent* MeshComp)
{
	if (!GetMesh() || MeshComp != this->GetMesh())
		return;

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	GetMesh()->SetSimulatePhysics(true);
}

void ATDSCharacterBase::CharacterDeath_Implementation()
{
	bIsDead = true;

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Broadcast - Character is Dead:
	OnCharacterDead.Broadcast(this);

	// Get Anim Death:
	int8 Rand = FMath::RandHelper(DeathAnimations.Num());

	if (DeathAnimations.IsValidIndex(Rand) && DeathAnimations[Rand] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		// Play Montage:
		GetMesh()->GetAnimInstance()->Montage_Play(DeathAnimations[Rand]);

		auto EndNotify = AnimUtils::FindNotifyByClass<UTPSDeathAnimIsEndNotify>(DeathAnimations[Rand]);

		// Debug:
		checkf(EndNotify, TEXT("DeathAnimation: %s not have Notify \"UTPSDeathAnimIsEndNotify\""), *DeathAnimations[Rand]->GetName());

		// Bind to Notify anim is end and StartRagDoll:
		if (EndNotify)
			EndNotify->OnNotifiedSignature.AddUObject(this, &ATDSCharacterBase::StartRagDoll);
	}

	// Set Collision Profile:
	if (GetMesh())
	{
		// Set Collision Profile:
		GetMesh()->SetCollisionProfileName("DeadCharacter");
	}

	SetLifeSpan(LifeSpanTimeAfterDeath);
}

bool ATDSCharacterBase::IsCanMove_Implementation() const
{
	return bIsCanMove;
}

bool ATDSCharacterBase::IsDead_Implementation() const
{
	return bIsDead;
}

void ATDSCharacterBase::CharacterIsHit_Implementation() const
{

	OnCharacterIsHit.Broadcast();
}

void ATDSCharacterBase::CharacterIsKill_Implementation(AActor* WhoIsKilled)
{
	if (!bIsCall_OnCharacterIsKill)
	{
		bIsCall_OnCharacterIsKill = true;
		OnCharacterIsKill.Broadcast(WhoIsKilled);
	}
}

void ATDSCharacterBase::SetDynamicSpawn_Implementation(bool bIsDynamicSpawn)
{
	bIsSpawnDynamic = bIsDynamicSpawn;
}

bool ATDSCharacterBase::TryAttackTarget_Implementation()
{
	return false;
}

// Called when the game starts or when spawned
void ATDSCharacterBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATDSCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ATDSCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}
