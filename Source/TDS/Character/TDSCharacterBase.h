// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FuncLibrary/Types.h"
#include "GameFramework/Character.h"

// Interface:
#include "Interface/CharacterInterface.h"
#include "Interface/StateEffectInterface.h"

#include "TDSCharacterBase.generated.h"

/** We will find out from the delegate when state movement is changed */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeMovementState);

/** Notifies that the TPSCharacter is dead */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterDead, ATDSCharacterBase*, Character);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStartAttack, bool, bIsStartAttack);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterIsKill, AActor*, WhoIsKilled);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterIsHit);

class UTDSMovementComponentBase;

UCLASS(Blueprintable)
class TDS_API ATDSCharacterBase : public ACharacter, public ICharacterInterface, public IStateEffectInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDSCharacterBase(const FObjectInitializer& ObjInit);

	UPROPERTY(BlueprintAssignable, Category = "Character")
	FOnCharacterIsKill OnCharacterIsKill;

	UPROPERTY(BlueprintAssignable, Category = "Character")
	FOnStartAttack OnStartAttack;

	UPROPERTY(BlueprintAssignable, Category = "Character")
	FOnCharacterIsHit OnCharacterIsHit;

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Character")
	bool bIsSpawnDynamic = false;

#pragma region Movement

public:
	virtual UPawnMovementComponent* GetMovementComponent() const override;

	///** Get Custom Character Movement. */
	//UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Character | Movement")
	//UTDSMovementComponentBase* GetCustomCharacterMovement();

	/* Delegate: */
	UPROPERTY()
	FOnChangeMovementState OnChangeMovementState;

public:
	// Input Axis:
	float AxisX = 0.0f;
	float AxisY = 0.0f;

protected:
	/** Custom movement component for this project */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character | Movement")
	UTDSMovementComponentBase* CustomMovementComponent = nullptr;

#pragma endregion

#pragma region Health

public:
	UPROPERTY(BlueprintAssignable, Category = "Character | Death")
	FOnCharacterDead OnCharacterDead;

protected:
	// Death:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character | Death")
	TArray<UAnimMontage*> DeathAnimations;

	UFUNCTION(BlueprintCallable, Category = "Character | Death")
	void StartRagDoll(USkeletalMeshComponent* MeshComp);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	void CharacterDeath();
	virtual void CharacterDeath_Implementation();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character | Death")
	float LifeSpanTimeAfterDeath = 50.0f;

	UPROPERTY()
	bool bIsDead = false;

private:
	bool bIsCall_OnCharacterIsKill = false;
#pragma endregion

#pragma region CharacterInterface

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	bool IsCanMove() const;
	bool IsCanMove_Implementation() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	bool IsDead() const;
	bool IsDead_Implementation() const;

	UFUNCTION(BlueprintNativeEvent, Category = "Character")
	void CharacterIsHit() const;
	void CharacterIsHit_Implementation() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	void CharacterIsKill(AActor* WhoIsKilled);
	void CharacterIsKill_Implementation(AActor* WhoIsKilled);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	void SetDynamicSpawn(bool bIsDynamicSpawn);
	void SetDynamicSpawn_Implementation(bool bIsDynamicSpawn);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	bool TryAttackTarget();
	bool TryAttackTarget_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	void SetTarget(AActor* Target);
	void SetTarget_Implementation(AActor* Target);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	AActor* GetTarget() const;
	AActor* GetTarget_Implementation() const;

#pragma endregion

#pragma region StateEffectInterface

public:
	/** Check: Return can set state effect for this Actor: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	bool IsCanSetStateEffect();
	bool IsCanSetStateEffect_Implementation();

	/** Get Current State Effects: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	TArray<UStateEffect*> GetStateEffects() const;
	TArray<UStateEffect*> GetStateEffects_Implementation() const;

	/** Add State Effect: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	bool AddStateEffect(UStateEffect* NewStateEffect);
	bool AddStateEffect_Implementation(UStateEffect* NewStateEffect);

	/** Remove State Effect: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	bool RemoveStateEffect(UStateEffect* StateEffect);
	bool RemoveStateEffect_Implementation(UStateEffect* StateEffect);

#pragma endregion

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AI_Character")
	bool bIsCanMove = true;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
