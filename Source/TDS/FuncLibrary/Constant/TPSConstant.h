// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

;
class TPSConstant
{
public:
	static const int MAX_HEALTHS = 100.0f;
	static const int MAX_SHIELD_POINTS = 100.0f;
	/** Start count 1.*/
	static const int MAX_WEAPON_SLOTS = 4;
};
