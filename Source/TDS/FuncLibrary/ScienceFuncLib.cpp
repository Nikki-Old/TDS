// Fill out your copyright notice in the Description page of Project Settings.

#include "FuncLibrary/ScienceFuncLib.h"

DEFINE_LOG_CATEGORY_STATIC(LogFibonacci, All, All);

int32 UScienceFuncLib::Fibonacci(int32 Value)
{
	// check(Value >= 0); this +-equal vvv

	if (Value < 0)
	{
		UE_LOG(LogFibonacci, Error, TEXT("Invalid input for Fibonacci: %i"), Value);
	}

	return Value <= 1 ? Value : Fibonacci(Value - 1) + Fibonacci(Value - 2);
}