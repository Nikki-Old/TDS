// Fill out your copyright notice in the Description page of Project Settings.


#include "FuncLibrary/TPSClassLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "FuncLibrary/Constant/TPSConstant.h"
#include "Game/TPSGameInstance.h"
#include "Game/TPSPlayerController.h"
#include "PlayerCharacter.h"

//const int MAX_HEALTHS = 100.f;

UTPSGameInstance* UTPSClassLibrary::GetTPSGameInstance(const UWorld* WorldContext)
{
	if (!WorldContext)
		return nullptr;

	auto World = GEngine->GetWorldFromContextObject(WorldContext, EGetWorldErrorMode::LogAndReturnNull);
	auto CurrentGameInstance = Cast<UTPSGameInstance>(UGameplayStatics::GetGameInstance(World));

	// Debug:
	checkf(CurrentGameInstance, TEXT("GameInstance cast to UTPSGameInstance - FAIL!"));

	return CurrentGameInstance;
}

ATPSPlayerController* UTPSClassLibrary::GetTPSPlayerController(const UWorld* WorldContext)
{
	if (!WorldContext)
		return nullptr;

	auto World = GEngine->GetWorldFromContextObject(WorldContext, EGetWorldErrorMode::LogAndReturnNull);
	auto CurrentPlayerController = Cast<ATPSPlayerController>(UGameplayStatics::GetPlayerController(World, 0));

	// Debug:
	checkf(CurrentPlayerController, TEXT("PlayerController cast to ATPSPlayerController - FAIL!"));

	return CurrentPlayerController;
}

APlayerCharacter* UTPSClassLibrary::GetTPSCharacter(const UWorld* WorldContext)
{
	if (!WorldContext)
		return nullptr;

	auto World = GEngine->GetWorldFromContextObject(WorldContext, EGetWorldErrorMode::LogAndReturnNull);
	auto CurrentCharacter = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(World, 0));

	// Debug:
	checkf(CurrentCharacter, TEXT("PlayerController cast to ATPSPlayerController - FAIL!"));

	return CurrentCharacter;
}

int32 UTPSClassLibrary::GetMaxHealthConst() const
{
	return TPSConstant::MAX_HEALTHS;
}
