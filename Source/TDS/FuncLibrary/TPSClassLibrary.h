// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "TPSClassLibrary.generated.h"

/**
*	Function for take base classes in c++ and BP.
 */

UCLASS()
class TDS_API UTPSClassLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	

	/** Get TPSGameInstance. */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TPS Base Classes")
	static class UTPSGameInstance* GetTPSGameInstance(const UWorld* WorldContext);

	/** Get TPSGameInstance. */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TPS Base Classes")
	static class ATPSPlayerController* GetTPSPlayerController(const UWorld* WorldContext);

	/** Get TPSCharacter. */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "TPS Base Classes")
	static class APlayerCharacter* GetTPSCharacter(const UWorld* WorldContext);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Constant")
	int32 GetMaxHealthConst() const;
};
