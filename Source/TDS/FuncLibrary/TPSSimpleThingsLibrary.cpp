// Fill out your copyright notice in the Description page of Project Settings.
#include "FuncLibrary/TPSSimpleThingsLibrary.h"
#include "Game/StateEffect/StateEffect.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Containers/EnumAsByte.h"

#include "PrintString.h"

#include "Utility.h"

void UTPSSimpleThingsLibrary::AxisRotation(AActor* Actor, EAxisRotation AxisRotation, float RotationForce)
{
	if (!Actor)
		return;
	FRotator Rotation = FRotator(0);

	switch (AxisRotation)
	{
		case EAxisRotation::Pitch_Axis:
			Rotation = FRotator(1.0f * RotationForce, 0.0f, 0.0f);
			break;

		case EAxisRotation::Yaw_Axis:
			Rotation = FRotator(0.0f, 1.0f * RotationForce, 0.0f);
			break;

		case EAxisRotation::Roll_Axis:
			Rotation = FRotator(0.0f, 0.0f, 1.0f * RotationForce);
			break;
	}

	Actor->AddActorLocalRotation(Rotation);
}

void UTPSSimpleThingsLibrary::Levitation(AActor* Actor, float LevitationForce, ELevitationDirection LevitationDirection, bool bIsUp, bool& NextDirection)
{
	if (!Actor)
		return;

	FVector Direction = FVector(0);
	switch (LevitationDirection)
	{
		case ELevitationDirection::X_Direction:
			Direction = FVector(1.0f * LevitationForce, 0.0f, 0.0f);
			break;

		case ELevitationDirection::Y_Direction:
			Direction = FVector(0.0f, 1.0f * LevitationForce, 0.0f);
			break;

		case ELevitationDirection::Z_Direction:
			Direction = FVector(0.0f, 0.0f, 1.0f * LevitationForce);
			break;
	}

	Direction = bIsUp ? Direction : Direction * -1.0f;

	NextDirection = !bIsUp;

	Actor->AddActorLocalOffset(Direction, true);
}

bool UTPSSimpleThingsLibrary::AddStateEffectBySurface(AActor* Actor, AActor* Instigator, TSubclassOf<UStateEffect> StateEffectClass, EPhysicalSurface SurfaceType, UStateEffect*& StateEffect)
{
	if (!Actor)
		return false;

	TEnumAsByte<EPhysicalSurface> ActorSurface;

	// Get Surface type:
	if (EPhysicalSurface::SurfaceType_Default == SurfaceType)
	{
		// Find Mesh component:
		auto ActorMesh = Utils::GetTPSComponent<UMeshComponent>(Actor);

		// If not find, return false:
		if (ActorMesh)
		{
			// Try take another surface type:
			UTPSSimpleThingsLibrary::GetSurfaceType(ActorMesh, ActorSurface);
		}
		else
		{
			ActorSurface = SurfaceType;
		}
	}
	else
	{
		ActorSurface = SurfaceType;
	}

	// Spawn State Effect:
	if (StateEffectClass)
	{
		UStateEffect* DefaultStateEffect = Cast<UStateEffect>(StateEffectClass->GetDefaultObject());
		if (DefaultStateEffect)
		{
			if (DefaultStateEffect->IsPossibleSpawn(ActorSurface))
			{
				UStateEffect* NewStateEffect = NewObject<UStateEffect>(Actor, StateEffectClass); //NewObject<UStateEffect>(Actor);
				if (NewStateEffect)
				{
					StateEffect = NewStateEffect;
					NewStateEffect->InitObject(Actor, Instigator);
					return true;
				}
			}
		}
	}

	return false;
}

bool UTPSSimpleThingsLibrary::FindSameStateClass(const UStateEffect* NewStateEffect, TArray<UStateEffect*> CurrentStateEffects)
{
	if (!NewStateEffect)
		return false;

	for (const auto State : CurrentStateEffects)
	{
		// If current State class == NewStateEffect Class...
		if (State->StaticClass() == NewStateEffect->StaticClass())
		{
			return true;
		}
	}

	return false;
}

bool UTPSSimpleThingsLibrary::GetSurfaceType(UMeshComponent* ActorMesh, TEnumAsByte<EPhysicalSurface>& Surface)
{
	if (!ActorMesh)
		return false;

	// Get Materials:
	const TArray<UMaterialInterface*> Materials = ActorMesh->GetMaterials();

	// Check: is have material with needed SurfaceType:
	for (const auto Material : Materials)
	{
		auto PhysMaterial = Material->GetPhysicalMaterial();

		Surface = PhysMaterial->SurfaceType;

		if (Surface != EPhysicalSurface::SurfaceType_Default)
		{
			return true;
		}
	}

	return false;
}

//template <typename TDelegate>
//UMenuWidgetBase* UTPSSimpleThingsLibrary::CreateMenuWidget(TSubclassOf<UMenuWidgetBase> MenuWidgetClass, APlayerController* OwningPlayerPlayer, UUserWidget* PreviosWidget, TDelegate CloseEvent)
//{
//	if (!IsValid(MenuWidgetClass) || !OwningPlayerPlayer)
//	{
//		return nullptr;
//	}
//
//	auto NewMenuWidget = CreateWidget<UMenuWidgetBase>(OwningPlayerPlayer, MenuWidgetClass.Get());
//
//	if (NewMenuWidget)
//	{
//		NewMenuWidget->SetPreviousWidget(PreviosWidget);
//		NewMenuWidget->OnCloseMenuWidget = CloseEvent;
//		NewMenuWidget->AddToViewport(0);
//	}
//
//	return NewMenuWidget;
//}
