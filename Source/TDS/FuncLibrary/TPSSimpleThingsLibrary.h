// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "TPSSimpleThingsLibrary.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EAxisRotation : uint8
{
	Pitch_Axis UMETA(DisplayName = "Pitch Axis Rotation"),
	Yaw_Axis UMETA(DisplayName = "Yaw Axis Rotation"),
	Roll_Axis UMETA(DisplayName = "Roll Axis Rotation")
};

UENUM(BlueprintType)
enum class ELevitationDirection : uint8
{
	X_Direction UMETA(DisplayName = "X Direction"),
	Y_Direction UMETA(DisplayName = "Y Direction"),
	Z_Direction UMETA(DisplayName = "Z Direction")
};

UCLASS()
class TDS_API UTPSSimpleThingsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Add Axis Rotation */
	UFUNCTION(BlueprintCallable, Category = "Movement")
	static void AxisRotation(AActor* Actor, EAxisRotation AxisRotation, float RotationForce);

	/** Add Lavitation */
	UFUNCTION(BlueprintCallable, Category = "Movement")
	static void Levitation(AActor* Actor, float LevitationForce, ELevitationDirection LevitationDirection, bool bIsUp, bool& NextDirection);

	/** Added State Effect by Surface, return StateEffect: */
	UFUNCTION(BlueprintCallable, Category = "StateEffect")
	static bool AddStateEffectBySurface(AActor* Actor, AActor* Instigator, TSubclassOf<UStateEffect> StateEffectClass, EPhysicalSurface HSurfaceType, UStateEffect*& StateEffect);

	/** Find for same new state class: */
	UFUNCTION(BlueprintCallable, Category = "StateEffect")
	static bool FindSameStateClass(const class UStateEffect* NewStateEffect, TArray<UStateEffect*> CurrentStateEffects);

	/** Get MeshComponent->Material Surface type: */
	UFUNCTION(BlueprintCallable, Category = "Surface Type")
	static bool GetSurfaceType(UMeshComponent* ActorMesh, TEnumAsByte<EPhysicalSurface>& Surface);
	
	/*template <typename TDelegate>
	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	static class UMenuWidgetBase* CreateMenuWidget(TSubclassOf<UMenuWidgetBase> MenuWidgetClass, APlayerController* OwningPlayerPlayer, class UUserWidget* PreviosWidget, TDelegate CloseEvent);*/

};
