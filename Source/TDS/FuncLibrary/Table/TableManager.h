// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "TableManager.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTableManager : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UTableManager();
	
	//UFUNCTION(BlueprintPure, Category = "TableManager")
	//static UDataTable* GetTestTable(UDataTable*& Table);

	//UPROPERTY(EditDefaultsOnly, Category = "TableManager | Tables")
	//UDataTable* TestTable;
};
