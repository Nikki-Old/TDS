// Fill out your copyright notice in the Description page of Project Settings.


#include "FuncLibrary/TagsFunctionLibrary.h"
#include "Misc/Paths.h"
#include "HAL/PlatformFilemanager.h"

const FString UTagsFunctionLibrary::TagFileName = FString::Printf(TEXT("TagFile.txt"));
const FString UTagsFunctionLibrary::TagFileDir = FString::Printf(TEXT(""));

bool UTagsFunctionLibrary::AddNewTag(FName NewTag)
{
	if (NewTag.IsNone())
	{
		return false;
	}

	bool Result = false;

	auto Tags = GetAllTags();
	if(Tags.AddUnique(NewTag.ToString()) > -1)
	{
		return FileSaveArrayString(FPaths::ProjectDir() + TagFileDir, TagFileName, Tags, true);
	}

	return false;
}

TArray<FString> UTagsFunctionLibrary::GetAllTags()
{
	TArray<FString> Tags;
	FileLoadString(FPaths::ProjectDir() + TagFileDir, TagFileName, Tags);

	return Tags;
}

bool UTagsFunctionLibrary::FileSaveArrayString(FString SaveDir, FString FileName, TArray<FString> SaveText, bool AllowOverWriting)
{
	// Set complete file path
	SaveDir += "\\";
	SaveDir += FileName;

	if (!AllowOverWriting)
	{
		if (FPlatformFileManager::Get().GetPlatformFile().FileExists(*SaveDir))
		{
			return false;
		}
	}

	FString FinalString = "";
	for (FString& Each : SaveText)
	{
		FinalString += Each;
		FinalString += LINE_TERMINATOR;
	}

	return FFileHelper::SaveStringToFile(*FinalString, *SaveDir);
}

bool UTagsFunctionLibrary::FileLoadString(FString Dir, FString FileName, TArray<FString>& Text)
{
	Dir += "\\";
	Dir += FileName;

	return FFileHelper::LoadFileToStringArray(Text, *Dir);
}

//bool UTagsFunctionLibrary::FileSaveString(FString SaveText)
//{
//	return FFileHelper::SaveStringToFile(SaveText, *(FPaths::GameDir() + TagFileName));
//}
//
//bool UTagsFunctionLibrary::FileLoadString(FString& SaveText)
//{
//	return FFileHelper::LoadFileToString(SaveText, *(FPaths::GameDir() + TagFileName));
//}