// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TagsFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTagsFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Add New Tag in Tag File */
	UFUNCTION(BlueprintCallable, Category = "Tags", meta = (Keywords = "Tag"))
	static bool AddNewTag(FName NewTag);

	/** Get All Tags from Tag File*/
	UFUNCTION(BlueprintCallable, Category = "Tags", meta = (Keywords = "Tag"))
	static TArray<FString> GetAllTags();

	/** Save */
	UFUNCTION(BlueprintCallable, Category = "SaveFile")
	static bool FileSaveArrayString(FString SaveDir, FString FileName, TArray<FString> SaveText, bool AllowOverWriting = false);

	/** Load */
	UFUNCTION(BlueprintPure, Category = "SaveFile")
	static bool FileLoadString(FString Dir, FString FileName, TArray<FString>& Text);

private:
	static const FString TagFileName;
	static const FString TagFileDir;

};
