// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
//#include "Sound/SoundBase.h"
//#include "Particles/ParticleSystem.h"
//#include "Components/DecalComponent.h"
//#include "Animation/AnimMontage.h"
//#include "Engine/StaticMesh.h"
//#include "Materials/MaterialInterface.h"
//#include "Sound/SoundBase.h"
#include "Containers/EnumAsByte.h"
//#include "Engine/StaticMesh.h"

#include "Types.generated.h"

class UPaperFlipbook;

UENUM(BlueprintType)
enum class ESublevelType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	Preloading_Type UMETA(DisplayName = "Preload"),
	LoadingByEvent_Type UMETA(DisplayName = "LoadingByEvent")
};

UENUM(BlueprintType)
enum class ETDSGameState : uint8
{
	None_State UMETA(DisplayName = "None"),
	InMain_State UMETA(DisplayName = "InMainMenu"),
	InGame_State UMETA(DisplayName = "InGame"),
	InPause_State UMETA(DisplayName = "InPause")
};

UENUM(BlueprintType)
enum class EZombieType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	Calm_Type UMETA(DisplayName = "Count"),
	Agressive_Type UMETA(DisplayName = "Agressive"),
	MAX_Type UMETA(DisplayName = "MAX")
};

/** Effect Type, how it works. TIME - until time runs out, COUNT - until it happens several times. */
UENUM(BlueprintType)
enum class ETemporaryStateEffectType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	Time_Type UMETA(DisplayName = "Time"),
	Count_Type UMETA(DisplayName = "Count")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	NONE_Type UMETA(DisplayName = "None"),
	Rifle_Type UMETA(DisplayName = "Rifle"),
	Shotgun_Type UMETA(DisplayName = "Shotgun"),
	SniperRifle_Type UMETA(DisplayName = "SniperRifle"),
	GrenadeLauncher_Type UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncher_Type UMETA(DisplayName = "RocketLauncher"),
	LaserGun_Type UMETA(DisplayName = "LaserGun")
};

UENUM(BlueprintType)
enum class EFlipbookStateDooX : uint8
{
	NONE_State UMETA(DisplayName = "None"),
	Idle_State UMETA(DisplayName = "Idle"),
	Attack_State UMETA(DisplayName = "Attack"),
	SpecialAttack_State UMETA(DisplayName = "SpecialAttack"),
};

UENUM(BlueprintType)
enum class ECharacterMovementState : uint8
{
	None_State UMETA(DisplayName = "None"),
	Aim_State UMETA(DisplayName = "Aim State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(Display = "SprintRun State"),
	MAX_State UMETA(Display = "MAX")
};

//USTRUCT(BlueprintType)
//struct FCharacterSpeed
//{
//	GENERATED_BODY()
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
//	float AimSpeedWalk = 200.0f;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
//	float AimSpeedNormal = 400.0f;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
//	float WalkSpeedNormal = 300.0f;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
//	float RunSpeedNormal = 600.0f;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
//	float SprintRunSpeedRun = 800.0f;
//};

USTRUCT(BlueprintType)
struct FWeaponDisplacement
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FVector AimWalk = FVector(0.0f, 0.0f, 160.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FVector AimNormal = FVector(0.0f, 0.0f, 160.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FVector WalkNormal = FVector(0.0f, 0.0f, 120.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FVector RunNormal = FVector(0.0f, 0.0f, 120.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FVector SprintSpeed = FVector(0);
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	// Base:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | View")
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | View")
	UParticleSystem* ParticleProjectile = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FVector MeshScale3D = FVector(1.f, 1.f, 1.f);

	// Movement speed:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileInitSpeed = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileStaticMeshOffset = FTransform();

	// State Effects:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<class UStateEffect> StateEffect;

	//61 - 69 need "PhysicCore" in <ProjectName>.Build.cs.
	//Material to decal on Hit:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	//Sound Hit:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	USoundBase* HitSound = nullptr;
	//FX when hit check by surface:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	//Hit FX Actor?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	bool bIsLikeBomb = false;

	// Explode:
	// FX:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | Explode", meta = (EditCondition = "bIsLikeBomb"))
	UParticleSystem* ExplodeFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | Explode", meta = (EditCondition = "bIsLikeBomb"))
	USoundBase* ExplodeSound = nullptr;
	// Radius Damage:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | Explode", meta = (EditCondition = "bIsLikeBomb"))
	float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | Explode", meta = (EditCondition = "bIsLikeBomb"))
	float ProjectileMinRadiusDamage = 200.0f;
	// Damage:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | Explode", meta = (EditCondition = "bIsLikeBomb"))
	float ExplodeMaxDamage = 40.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | Explode", meta = (EditCondition = "bIsLikeBomb"))
	float ExplodeFalloffCoef = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting | Explode", meta = (EditCondition = "bIsLikeBomb"))
	float TimeToExplose = 1.f;
	//Timer add ?
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	//Aim state:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Aim_StateDispersionReduction = 0.3f;

	//AimWalk State:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimWalk_StateDispersionReduction = 0.4f;

	//Walk State:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Walk_StateDispersionReduction = 0.2f;

	//Run State:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Run_StateDispersionReduction = 0.1f;

	//For Sprint no need, because we can't shoot.
};

USTRUCT(BlueprintType)
struct FAnimationWeaponInfo //: public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReloadAim = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponReloadAim = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponTakeWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponRemoveWeapon = nullptr;
};

/*USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		UStaticMesh* DropMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float DropMeshTime = -1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float DropMeshLifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		FVector DropMeshImpulseDir = FVector(0.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float PowerImpulse = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float ImpulseRandomDispersion = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float CustomMass = 0.f;

}; */

USTRUCT(BlueprintType)
struct FDropShellBulletInfo
{
	GENERATED_BODY()

	// Base:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet")
	TSubclassOf<class AShellBulletBase> ShellBulletClass = nullptr; // ...class...
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet")
	UStaticMesh* Mesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet")
	float MeshLifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet")
	FVector MeshScale3D = FVector(1.f, 1.f, 1.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet")
	float DropMeshTime = -1.f;

	// Sound:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Sound")
	USoundBase* SoundOfFalling = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Sound")
	float PitchMultMin = 0.7f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Sound")
	float PitchMultMax = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Sound")
	float VolumeMult = 1.5f;

	// Impulse:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Impulse")
	FVector DropMeshImpulseDir = FVector(0.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Impulse")
	float PowerImpulse = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Impulse")
	float ImpulseRandomDispersion = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropShellBullet | Impulse")
	float CustomMass = 0.f;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Shell Bullet")
	//	FTransform DropMeshOffset = FTransform();
};

USTRUCT(BlueprintType)
struct FDropMagazineInfo
{
	GENERATED_BODY()

	// Base:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine")
	TSubclassOf<class AMagazineBase> MagazineClass = nullptr; // ...class...
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine")
	UStaticMesh* Mesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine")
	FName MagazineBoneName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine")
	float MeshLifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine")
	FVector MeshScale3D = FVector(1.f, 1.f, 1.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine")
	FName SocketSpawn;

	// Sound:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine | Sound")
	USoundBase* SoundOfFalling = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine | Sound")
	float PitchMultMin = 0.7f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine | Sound")
	float PitchMultMax = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMagazine | Sound")
	float VolumeMult = 1.5f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	EWeaponType TypeWeapon = EWeaponType::Rifle_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	FName WeaponSocket;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float ReloadTime = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 MaxRounds = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 WasteOfAmmo = 1; // How mach waste ammo When fired.

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	UParticleSystem* EffectFireWeapon = nullptr;

	//If NULL use trace logic (TSubclassOf<class ProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	FProjectileInfo ProjectileSetting;
	//one decal on all?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Effect")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
	float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
	float DistanceTrace = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShellBullet")
	FDropShellBulletInfo ShellBulletSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magzine")
	FDropMagazineInfo MagazineSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	FAnimationWeaponInfo AnimWeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStats")
	float SwitchTimeToWeapon = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStats")
	UTexture2D* WeaponIcon = nullptr;

	bool IsEmpty()
	{
		if (!DropMesh)
		{
			return true;
		}
		return false;
	}
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	int32 CurrentRound = 0;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FName NameItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo;

	FWeaponSlot() {}
	FWeaponSlot(FName Name, FAdditionalWeaponInfo AdditionalInfo)
	{
		NameItem = Name;
		this->AdditionalInfo = AdditionalInfo;
	}

	bool IsEmpty()
	{
		return NameItem.IsNone();
	}

	void Clear()
	{
		NameItem = FName();
		AdditionalInfo.CurrentRound = 0;
	}
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponType = EWeaponType::NONE_Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 Count = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	int32 MaxCount = 100;

	FAmmoSlot() {}
	FAmmoSlot(EWeaponType NewWeaponType, int32 NewCount, int32 NewMaxCount)
	{
		WeaponType = NewWeaponType;
		Count = NewCount;
		MaxCount = NewMaxCount;
	}

	bool IsEmpty()
	{
		return WeaponType == EWeaponType::NONE_Type ? true : false;
	}

	void Clear()
	{
		WeaponType = EWeaponType::NONE_Type;
		Count = 0;
		MaxCount = 0;
	}
};

USTRUCT(BlueprintType)
struct FAmmoInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AmmoInfo")
	EWeaponType AmmoType = EWeaponType::NONE_Type;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AmmoInfo")
	UTexture2D* AmmoIcon = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "AmmoInfo")
	UStaticMesh* StaticMesh = nullptr;
};

USTRUCT(BlueprintType)
struct FDropItemInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMesh* StaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SkeletalMesh")
	USkeletalMesh* SkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FWeaponSlot WeaponInfo;

	bool IsEmpty()
	{
		if ((!StaticMesh || !SkeletalMesh) && WeaponInfo.IsEmpty())
		{
			return true;
		}

		return false;
	}
};

/** Sublevel information. */
USTRUCT(BlueprintType)
struct FSublevelInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SubLevel Information")
	FName SublevelName = "";

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SubLevel Information")
	ESublevelType SublevelType = ESublevelType::None_Type;
};

/** Level information. */
USTRUCT(BlueprintType)
struct FLevelInfo : public FTableRowBase
{
	GENERATED_BODY()

	/** Level name, for "OpenLevel". */
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Level Information")
	FName LevelName = NAME_None;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Level Information")
	ETDSGameState GameState = ETDSGameState::None_State;

	/** Sub-level names */
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Level Information")
	TArray<FSublevelInfo> SublevelsInfo = {};

	/** Player Start Tags */
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Level Information")
	TArray<FString> PlayerStartTags = {};

	/** Level Display name. */
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Level Information")
	FName LevelDisplayName = NAME_None;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Level Information")
	UTexture2D* LevelImage = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Level Information")
	FText LevelDescription;
};

/** Character information. */
USTRUCT(BlueprintType)
struct FCharacterParameters : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Enemy Parameters")
	float MovementSpeed = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Enemy Parameters")
	float AttackDistance = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Enemy Parameters")
	float AttackFrequency = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Enemy Parameters")
	float DamageAttack = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Enemy Parameters")
	float DamageCoef = 1.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Enemy Parameters")
	float SpecialAttackFrequency = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Enemy Parameters")
	float SpecialAttackCoefDamage = 0.0f;

	FCharacterParameters()
	{
		MovementSpeed = 0.0f;
		AttackDistance = 0.0f;
		DamageAttack = 0.0f;
		DamageCoef = 1.0f;
		SpecialAttackFrequency = 0.0f;
		SpecialAttackCoefDamage = 1.0f;
	}

	bool IsEmpty()
	{
		if (FMath::IsNearlyZero(MovementSpeed, 0.1f)			 //
			&& FMath::IsNearlyZero(AttackDistance, 0.1f)		 //
			&& FMath::IsNearlyZero(DamageAttack, 0.1f)			 //
			&& DamageCoef == 1.0f								 //
			&& FMath::IsNearlyZero(SpecialAttackFrequency, 0.1f) //
			&& SpecialAttackCoefDamage == 1.0f)					 //
		{
			return true;
		}

		return false;
	}
};

/** Switch weapon info */
USTRUCT(BlueprintType)
struct FSwitchWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SwitchWeaponInfo")
	FName IdWeaponName = "None";

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SwitchWeaponInfo")
	FAdditionalWeaponInfo AdditionalWeaponInfo;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SwitchWeaponInfo")
	int32 NewCurrentIndexWeapon = -1;

	FSwitchWeaponInfo() {}
	FSwitchWeaponInfo(FName Name, FAdditionalWeaponInfo NewAdditionalWeaponInfo, int32 NewCurrentIndexWeapon)
	{
		this->IdWeaponName = Name;
		this->AdditionalWeaponInfo = NewAdditionalWeaponInfo;
		this->NewCurrentIndexWeapon = NewCurrentIndexWeapon;
	}

	void Clear()
	{
		IdWeaponName = "None";
		AdditionalWeaponInfo.CurrentRound = 0;
		NewCurrentIndexWeapon = -1;
	}
};

/** This delegate broadcast from LevelWidgetBase. */
DECLARE_MULTICAST_DELEGATE_OneParam(FOnLevelSelectedSignature, FLevelInfo&);

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
