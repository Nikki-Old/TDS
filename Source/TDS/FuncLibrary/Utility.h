#pragma once



class Utils
{
public:
	template <typename T>
	static T* GetTPSComponent(AActor* PlayerPawn)
	{
		if (!PlayerPawn)
			return nullptr;

		// Get Player health component:
		const auto Component = PlayerPawn->GetComponentByClass(T::StaticClass());
		return Cast<T>(Component);
	}
};