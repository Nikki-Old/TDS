// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GameMode/GameModeGame.h"
#include "Game/TPSPlayerController.h "
#include "Character/TDSCharacterBase.h"
#include "PlayerCharacter.h"

AGameModeGame::AGameModeGame()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	DefaultPawnClass = APlayerCharacter::StaticClass();
}