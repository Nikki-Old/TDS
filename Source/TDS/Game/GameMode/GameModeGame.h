// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameMode/TPSGameMode.h"
#include "GameModeGame.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AGameModeGame : public ATPSGameMode
{
	GENERATED_BODY()

public:
	AGameModeGame();
};
