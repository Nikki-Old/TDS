// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPSGameMode.h"
#include "Game/TPSPlayerController.h "
#include "Character/TDSCharacterBase.h"
#include "PlayerCharacter.h"
#include "FuncLibrary/TPSClassLibrary.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "UI/Menu/MainMenu.h"

ATPSGameMode::ATPSGameMode(){};

void ATPSGameMode::BeginPlay()
{
	Super::BeginPlay();
}

void ATPSGameMode::GameStateIsChange_Implementation(ETDSGameState NewGameState)
{
	// In BP:
}

bool ATPSGameMode::GetEnemyParamsByName(FName NameEnemy, FCharacterParameters& OutInfo) const
{
	bool bIsFind = false;

	if (EnemyParametersTable)
	{
		FCharacterParameters* EnemyParam;

		EnemyParam = EnemyParametersTable->FindRow<FCharacterParameters>(NameEnemy, "", false);
		if (EnemyParam)
		{
			if (!EnemyParam->IsEmpty())
			{
				OutInfo = *EnemyParam;
				bIsFind = true;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, (TEXT("ATPSGameMode::GetEnemyParamsByName - EnemyParametersTable == NULL")));
	}

	return bIsFind;
}

void ATPSGameMode::PlayerControllerIsReady_Implementation()
{
	// In BP:
}

void ATPSGameMode::StartGame_Implementation()
{
	// Get Player Character:
	PlayerCharacter = UTPSClassLibrary::GetTPSCharacter(GetWorld());

	// Debug:
	check(PlayerCharacter);

	// Bind on Character is Dead:
	if (PlayerCharacter)
		PlayerCharacter->OnCharacterDead.AddDynamic(this, &ATPSGameMode::PlayerCharacterIsDead);

	OnStartGame.Broadcast();
}

void ATPSGameMode::CreateMenu_Implementation()
{
	if (IsValid(MenuClass) && !MainMenuWidget)
	{
		MainMenuWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), MenuClass, "MainMenu");

		if (MainMenuWidget)
		{
			MainMenuWidget->AddToViewport(++CurrentZOrder);
		}
	}
}

void ATPSGameMode::RemoveMenu()
{
	if (MainMenuWidget)
	{
		MainMenuWidget->RemoveFromParent();
		--CurrentZOrder;
	}
}

void ATPSGameMode::StartLoadLevel(FLevelInfo LevelInfo)
{
	if (IsValid(LoadScreenClass))
	{
		LoadScreenWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), LoadScreenClass);
		if (LoadScreenWidget)
		{
			LoadScreenWidget->AddToViewport(++CurrentZOrder);
		}
	}
}

void ATPSGameMode::EndLoadLevel()
{
	if (!bIsBlockLoadscreen)
	{
		if (LoadScreenWidget)
		{
			LoadScreenWidget->RemoveFromParent();
			--CurrentZOrder;
		}
	}
}

void ATPSGameMode::StartLoadSublevel()
{
	if (IsValid(LoadIconClass))
	{
		LoadIconWidget = CreateWidget(UGameplayStatics::GetPlayerController(GetWorld(), 0), LoadIconClass, "LoadIcon");

		if (LoadIconWidget)
		{
			LoadIconWidget->AddToViewport(++CurrentZOrder);
		}
	}
}

void ATPSGameMode::EndLoadSublevel()
{
	if (LoadIconWidget)
	{
		LoadIconWidget->RemoveFromParent();
		--CurrentZOrder;
	}
}

void ATPSGameMode::PlayerCharacterIsDead(ATDSCharacterBase* Char)
{
	if (Char != PlayerCharacter)
		return;

	bPlayerCharacterIsDead = true;
}
