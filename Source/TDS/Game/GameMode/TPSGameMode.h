// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FuncLibrary/Types.h"
#include "TPSGameMode.generated.h"

class ATDSCharacterBase;
class APlayerCharacter;
class UMainMenu;

/** Delegate for broadcast "Game is start" */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartGame);

UCLASS(minimalapi)
class ATPSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPSGameMode();

	virtual void BeginPlay() override;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameState")
	void GameStateIsChange(ETDSGameState NewGameState);
	void GameStateIsChange_Implementation(ETDSGameState NewGameState);

#pragma region EnemyInfo

public:
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	bool GetEnemyParamsByName(FName NameEnemy, FCharacterParameters& OutInfo) const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy")
	UDataTable* EnemyParametersTable = nullptr;

#pragma endregion

#pragma region Game

public:
	UPROPERTY(BlueprintAssignable, Category = "Game")
	FOnStartGame OnStartGame;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game")
	void PlayerControllerIsReady();
	void PlayerControllerIsReady_Implementation();

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game")
	void StartGame();
	void StartGame_Implementation();

#pragma endregion

#pragma region UI

protected:
	// Menu:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "UI | Menu")
	void CreateMenu();
	void CreateMenu_Implementation();

	UFUNCTION(BlueprintCallable, Category = "UI | Menu")
	void RemoveMenu();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "UI | Loadscreen")
	UUserWidget* MainMenuWidget = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "UI | Menu")
	TSubclassOf<UMainMenu> MenuClass = nullptr;

	// LoadScreen:
	UFUNCTION(BlueprintCallable, Category = "UI | Loadscreen")
	void StartLoadLevel(FLevelInfo LevelInfo);

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "UI | Loadscreen")
	bool bIsBlockLoadscreen = false;

	UPROPERTY(EditDefaultsOnly, Category = "UI | Loadscreen")
	TSubclassOf<UUserWidget> LoadScreenClass = nullptr;

	UFUNCTION(BlueprintCallable, Category = "UI | Loadscreen")
	void EndLoadLevel();

	// LoadIcon:
	UFUNCTION(BlueprintCallable, Category = "UI | Loadscreen")
	void StartLoadSublevel();

	UPROPERTY(EditDefaultsOnly, Category = "UI | Loadscreen")
	TSubclassOf<UUserWidget> LoadIconClass = nullptr;

	UFUNCTION(BlueprintCallable, Category = "UI | Loadscreen")
	void EndLoadSublevel();

	/* https://gitlab.com/Nikki-Old/TDS/-/work_items/2 */
	uint8 CurrentZOrder = 0;

private:
	UPROPERTY()
	UUserWidget* LoadScreenWidget = nullptr;

	UPROPERTY()
	UUserWidget* LoadIconWidget = nullptr;

#pragma endregion

#pragma region Player
private:
	/** Base Player Character: */
	APlayerCharacter* PlayerCharacter = nullptr;

	UPROPERTY()
	bool bPlayerCharacterIsDead = false;

	UFUNCTION(Category = "Character")
	void PlayerCharacterIsDead(ATDSCharacterBase* Char);

#pragma endregion
};
