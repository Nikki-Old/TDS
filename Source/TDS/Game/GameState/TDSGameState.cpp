// Fill out your copyright notice in the Description page of Project Settings.


#include "GameState/TDSGameState.h"

void ATDSGameState::SetNewGameState_Implementation(ETDSGameState NewGameState)
{
	if (CurrentGameState != NewGameState)
	{
		CurrentGameState = NewGameState;
		OnChangeGameState.Broadcast(CurrentGameState);
	}
}
