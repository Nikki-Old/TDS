// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "FuncLibrary/Types.h"
#include "TDSGameState.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeGameState, ETDSGameState, NewGameState);

UCLASS()
class TDS_API ATDSGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	/** Get Current Game State */
	UFUNCTION(BlueprintPure, Category = "GameState")
	FORCEINLINE ETDSGameState GetCurrentGameState() const { return CurrentGameState; }

	UPROPERTY(BlueprintAssignable, Category = "GameState")
	FOnChangeGameState OnChangeGameState;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameState")
	void SetNewGameState(ETDSGameState NewGameState);
	void SetNewGameState_Implementation(ETDSGameState NewGameState);

private:
	/** Current Game State */
	UPROPERTY()
	ETDSGameState CurrentGameState = ETDSGameState::None_State;
};
