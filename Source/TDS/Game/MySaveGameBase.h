// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MySaveGameBase.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UMySaveGameBase : public USaveGame
{
	GENERATED_BODY()

public:

	UMySaveGameBase();

	UPROPERTY(EditAnywhere)
		FVector PlayerPosition = FVector(0);

};
