// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/PlayerState/TPSPlayerState.h"
#include "FuncLibrary/TPSClassLibrary.h"
#include "Game/TPSGameInstance.h"

// Weapon slots:
void ATPSPlayerState::SwitchWeaponSlot(int32 IndexSwitchWeaponSlot, FWeaponSlot NewWeaponSlot)
{

	if (ClearWeaponSlot(IndexSwitchWeaponSlot))
	{
		SetWeaponSlot(IndexSwitchWeaponSlot, NewWeaponSlot);
	}

}

bool ATPSPlayerState::SetWeaponSlot(const int32 IndexSwitchWeaponSlot, FWeaponSlot& NewWeaponSlot)
{

	if (WeaponSlots.IsValidIndex(IndexSwitchWeaponSlot))
	{
		WeaponSlots[IndexSwitchWeaponSlot] = NewWeaponSlot;
		return true;
	}

	return false;
}

bool ATPSPlayerState::ClearWeaponSlot(const int IndexWeaponSlot)
{
	if (WeaponSlots.IsValidIndex(IndexWeaponSlot))
	{
		WeaponSlots[IndexWeaponSlot].Clear();

		return true;
	}

	return false;
}

TArray<FWeaponSlot> ATPSPlayerState::GetWeaponSlots() const
{
	return WeaponSlots;
}

// Ammo slots:
void ATPSPlayerState::SwitchAmmoSlot(int32 IndexSwitchAmmoSlot, FAmmoSlot NewAmmoSlot)
{

	if (ClearAmmoSlot(IndexSwitchAmmoSlot))
	{
		SetAmmoSlot(IndexSwitchAmmoSlot, NewAmmoSlot);
	}
}

bool ATPSPlayerState::SetAmmoSlot(const int32 IndexAmmoSlot, FAmmoSlot& NewAmmoSlot)
{

	if (AmmoSlots.IsValidIndex(IndexAmmoSlot))
	{
		AmmoSlots[IndexAmmoSlot] = NewAmmoSlot;
		return true;
	}

	return false;
}

bool ATPSPlayerState::ClearAmmoSlot(const int IndexAmmoSlot)
{
	if (AmmoSlots.IsValidIndex(IndexAmmoSlot))
	{
		AmmoSlots[IndexAmmoSlot].Clear();

		return true;
	}

	return false;
}

bool ATPSPlayerState::AddAmmoSlot(FAmmoSlot& NewAmmoSlots)
{
	AmmoSlots.Add(NewAmmoSlots);

	return true;
}

TArray<FAmmoSlot> ATPSPlayerState::GetAmmoSlots() const
{
	return AmmoSlots;
}

void ATPSPlayerState::SavePlayerInformation(bool bIsEndGame)
{
	if (GI)
	{
		GI->SetPlayerWeaponSlots(WeaponSlots, bIsEndGame);
		GI->SetPlayerAmmoSlots(AmmoSlots, bIsEndGame);
	}
}

void ATPSPlayerState::PostInitializeComponents()
{
	InitialInventoryInfo();

	Super::PostInitializeComponents();
}

void ATPSPlayerState::Destroyed()
{
	SavePlayerInformation();
	Super::Destroyed();
}

void ATPSPlayerState::InitialInventoryInfo()
{
	WeaponSlots.SetNum(MaxWeaponSlots);

	if (GetWorld())
	{
		if (!GI)
		{
			GI = UTPSClassLibrary::GetTPSGameInstance(GetWorld());
		}

		if (GI)
		{
			WeaponSlots = GI->GetPlayerWeaponSlots();
			AmmoSlots = GI->GetPlayerAmmoSlots();
		}
	}
}
