// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FuncLibrary/Types.h"
#include "FuncLibrary/Constant/TPSConstant.h"
#include "GameFramework/PlayerState.h"
#include "TPSPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ATPSPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	// Inventory:

	// Weapon slots:

	/** Switch Weapon slot by index */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	void SwitchWeaponSlot(int32 IndexSwitchWeaponSlot, FWeaponSlot NewWeaponSlot);

	/** Set Weapon slot by index */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	bool SetWeaponSlot(const int32 IndexWeaponSlot, FWeaponSlot& NewWeaponSlot);

	/** Clear Weapon slot by index */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	bool ClearWeaponSlot(const int IndexWeaponSlot);

	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	TArray<FWeaponSlot> GetWeaponSlots() const;

	// Ammo slots:

	/** Switch Ammo slot by index */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	void SwitchAmmoSlot(int32 IndexSwitchAmmoSlot, FAmmoSlot NewAmmoSlot);

	/** Set Ammo slot by index */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	bool SetAmmoSlot(const int32 IndexAmmoSlot, FAmmoSlot& NewAmmoSlot);

	/** Clear Ammos slot by index */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	bool ClearAmmoSlot(const int IndexAmmoSlot);

	/** Add new Ammo Slot */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	bool AddAmmoSlot(FAmmoSlot& NewAmmoSlots);

	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState | Inventory")
	TArray<FAmmoSlot> GetAmmoSlots() const;

	/** Save All Player information. If bISave */
	UFUNCTION(BlueprintCallable, Category = "TPSPlayerState")
	void SavePlayerInformation(bool bIsEndGame = false);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "TPSPlayerState | Inventory")
	int MaxWeaponSlots = TPSConstant::MAX_WEAPON_SLOTS;

	/** Current Player Weapon slots */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "TPSPlayerState | Inventory")
	TArray<FWeaponSlot> WeaponSlots;

	/** Current Player Ammo slots */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "TPSPlayerState | Inventory")
	TArray<FAmmoSlot> AmmoSlots;

	virtual void PostInitializeComponents() override;
	virtual void Destroyed() override;

	/** Initialiaze Inventory infromation: */
	UFUNCTION()
	void InitialInventoryInfo();

	class UTPSGameInstance* GI = nullptr;
};