// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/StateEffect/StateEffect.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

#include "Interface/StateEffectInterface.h"
#include "FuncLibrary/Types.h"
#include "ActorComponent/Health/TPSHealthComponentBase.h"
#include "ActorComponent/Health/TPSUpdatedHealthComponent.h"
#include "FuncLibrary/Utility.h"
#include "PrintString.h"

// Base State Effect:
bool UStateEffect::InitObject(AActor* OwnerActor, AActor* Instigator)
{
	if(bIsInitState)
	{
		return true;
	}

	// Set Owner Actor:
	if (OwnerActor)
	{
		this->OwnerActorState = OwnerActor;
		this->InstigatorActorState = Instigator;
		return true;
	}
	else
	{
		return false;
	}
}

bool UStateEffect::ExecuteEffect()
{
	return true;
}

void UStateEffect::DestroyState()
{
	//this->MarkPendingKill();
	this->ConditionalBeginDestroy();
}

bool UStateEffect::IsPossibleSpawn(EPhysicalSurface SurfaceType)
{
	for (auto& Surface : PossibleIteractSurface)
	{
		if (Surface == SurfaceType)
		{
			return true;
		}
	}

	return false;
}
// Base State Effect - END.

// Once State Effect:
bool UOnceStateEffect::InitObject(AActor* OwnerActor, AActor* Instigator)
{

	if (Super::InitObject(OwnerActor, Instigator))
	{
		ExecuteEffect();
		return true;
	}
	else
	{
		return false;
	}
}

bool UOnceStateEffect::ExecuteEffect()
{
	if (ParticleFX)
	{
		// Spawn ParticleSystem:
		UGameplayStatics::SpawnEmitterAttached(ParticleFX, //
			OwnerActorState->GetRootComponent(),		   //
			FName(), FVector(0), FRotator(0),			   //
			EAttachLocation::KeepRelativeOffset);		   //
	}

	DestroyState();
	return true;
}

void UOnceStateEffect::DestroyState()
{
	Super::DestroyState();
}
//  Once State Effect - END.

// Health State Effect:
bool UHealthStateEffect::InitObject(AActor* OwnerActor, AActor* Instigator)
{
	if (OwnerActor)
	{
		OwnerActorState = OwnerActor;

		// Find Health Class:
		OwnerHealthBase = Utils::GetTPSComponent<UTPSHealthComponentBase>(OwnerActor);
		if (OwnerHealthBase)
		{
			if (!bIsCanIgnoreShield)
			{
				// Is Health component is Updated?
				auto OwnerUpdatedHealth = Cast<UTPSUpdatedHealthComponent>(OwnerHealthBase);

				if (OwnerUpdatedHealth->GetShieldPoints() <= 0.0f)
				{
					ExecuteEffect();
					return true;
				}
			}
			else
			{
				ExecuteEffect();
				return true;
			}
		}
	}

	DestroyState();
	return false;
}

bool UHealthStateEffect::ExecuteEffect()
{
	if (OwnerHealthBase && !OwnerHealthBase->IsDead())
	{
		OwnerHealthBase->ChangeHealth(ChangeHealth, InstigatorActorState);
	}

	return Super::ExecuteEffect();
}
// Once Heatlh State Effect - END.

// TemporaryStateEffect:
bool UTemporaryStateEffect::InitObject(AActor* OwnerActor, AActor* Instigator)
{
	if (Super::InitObject(OwnerActor, Instigator))
	{
		ExecuteEffect();
		return true;
	}
	else
	{
		return false;
	}
}

bool UTemporaryStateEffect::ExecuteEffect()
{
	if (OwnerActorState && GetWorld())
	{
		if (ParticleFX)
		{
			ParticleFXComponent = UGameplayStatics::SpawnEmitterAttached(ParticleFX, OwnerActorState->GetRootComponent());
		}

		LeftCount = Count;
		GetWorld()->GetTimerManager().SetTimer(ActionTimer, this, &UTemporaryStateEffect::ActionStateEffect, HowOftenAction, true, FirstDelay);
		return true;
	}
	else
	{
		DestroyState();
		return false;
	}
}

void UTemporaryStateEffect::ActionStateEffect()
{
}

void UTemporaryStateEffect::DestroyState()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(ActionTimer);
	}

	if (ParticleFXComponent)
	{
		ParticleFXComponent->DestroyComponent();
	}

	Super::DestroyState();
}
// Temporary State Effect - END.

// Health Temporary State Effect:
bool UHealthTemporaryStateEffect::ExecuteEffect()
{
	if (OwnerActorState)
	{
		OwnerHealthComponent = Utils::GetTPSComponent<UTPSHealthComponentBase>(OwnerActorState);
		if (OwnerHealthComponent && OwnerHealthComponent->IsCanSetStateEffect())
		{
			if (OwnerHealthComponent->AddStateEffect(this))
			{
				if (!bIsIgnoreShield)
				{
					OwnerUpdatedHealthComponent = Cast<UTPSUpdatedHealthComponent>(OwnerHealthComponent);
				}

				return Super::ExecuteEffect();
			}
		}
	}

	DestroyState();
	return false;
}

void UHealthTemporaryStateEffect::ActionStateEffect()
{
	if (0 >= LeftCount--)
	{
		DestroyState();
		return;
	}

	if (!bIsIgnoreShield && OwnerUpdatedHealthComponent && OwnerUpdatedHealthComponent->GetShieldPoints() > 0.0f)
	{
		return;
	}

	if (!OwnerHealthComponent)
	{
		return;
	}

	OwnerHealthComponent->ChangeHealth(ChangeHealth, InstigatorActorState);
}

void UHealthTemporaryStateEffect::DestroyState()
{
	OwnerHealthComponent->RemoveStateEffect(this);
	Super::DestroyState();
}
// Health Temporary State Effect - END.
