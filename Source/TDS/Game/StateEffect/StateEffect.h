// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Types.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Containers/EnumAsByte.h"

#include "StateEffect.generated.h"

/**
 *	 Base State Effect.
 *	This class base for all other StateEffect's.
 */

UCLASS(Blueprintable, BlueprintType)
class TDS_API UStateEffect : public UObject
{
	GENERATED_BODY()

public:
	/** Initial function: */
	virtual bool InitObject(AActor* OwnerActor, AActor* Instigator);
	/** After InitObject, can use Execute: */
	virtual bool ExecuteEffect();
	/** Destroy state. */
	virtual void DestroyState();

	/** Is can Spawn? */
	bool IsPossibleSpawn(EPhysicalSurface SurfaceType);

	/** Array Surface types to which there is a reaction: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleIteractSurface;

	/** Can stackable */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	bool bIsStackable = true;

protected:
	/** Owner Actor */
	AActor* OwnerActorState = nullptr;
	AActor* InstigatorActorState = nullptr;

	bool bIsInitState = false;
};
// Base State Effect - END.

/**
 *	 Once State Effect.
 *	This class for state effect's which are used only once.
 */

UCLASS(Blueprintable, BlueprintType)
class TDS_API UOnceStateEffect : public UStateEffect
{
	GENERATED_BODY()

public:
	/** Initial function: */
	virtual bool InitObject(AActor* OwnerActor, AActor* Instigator) override;
	virtual void DestroyState() override;

protected:
	virtual bool ExecuteEffect() override;

	/** Particle system */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	class UParticleSystem* ParticleFX = nullptr;
};
//  Once State Effect - END.

/**
 *	Once Health State Effect.
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_API UHealthStateEffect : public UOnceStateEffect
{
	GENERATED_BODY()

public:
	/** Initial function: */
	virtual bool InitObject(AActor* OwnerActor, AActor* Instigator) override;

protected:
	virtual bool ExecuteEffect() override;

	/** How much does Owner Actor's health change: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect", meta = (ClampMin = "-100", UMin = "-100", ClampMax = "100", UMax = "100"))
	float ChangeHealth = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	bool bIsCanIgnoreShield = true;

private:
	/** Owner Actor Health Component: */
	class UTPSHealthComponentBase* OwnerHealthBase = nullptr;
};
// Once Heatlh State Effect - END.

/*
*	Temporary State Effect.
*/

UCLASS(Blueprintable, BlueprintType)
class TDS_API UTemporaryStateEffect : public UStateEffect
{
	GENERATED_BODY()

public:
	/** Initial function: */
	virtual bool InitObject(AActor* OwnerActor, AActor* Instigator);

	virtual void DestroyState();

protected:
	virtual bool ExecuteEffect();

	/** Action to be performed: */
	UFUNCTION(Category = "StateEffect")
	virtual void ActionStateEffect();

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	//ETemporaryStateEffectType TemporaryStateEffectType;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect", meta = (EditCondition = "TemporaryStateEffectType == ETemporaryStateEffectType::Time_Type"))
	//float TimeOfAction = 5.0f;

	/** Count Action before destroy.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect" /*, meta = (EditCondition = "TemporaryStateEffectType == ETemporaryStateEffectType::Count_Type")*/)
	int Count = 1;

	/** Delay between Action's */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect", meta = (ClampMin = "0.1", UMin = "0.1", ClampMax = "30.0", UMax = "30.0"))
	float HowOftenAction = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect", meta = (ClampMin = "-1", UMin = "-1", ClampMax = "30.0", UMax = "30.0"))
	float FirstDelay = -1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	class UParticleSystem* ParticleFX = nullptr;

protected:
	int LeftCount = 0;
	FTimerHandle ActionTimer;
	class UParticleSystemComponent* ParticleFXComponent = nullptr;
};
// Temporary State Effect - END.

/*
*	Health Temporary State Effect:
*/

UCLASS(Blueprintable, BlueprintType)
class TDS_API UHealthTemporaryStateEffect : public UTemporaryStateEffect
{
	GENERATED_BODY()

public:
	virtual void DestroyState();

protected:
	virtual bool ExecuteEffect();

	/** Action to be performed: */
	virtual void ActionStateEffect();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	float ChangeHealth = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "StateEffect")
	bool bIsIgnoreShield = false;

private:
	UTPSHealthComponentBase* OwnerHealthComponent = nullptr;
	class UTPSUpdatedHealthComponent* OwnerUpdatedHealthComponent = nullptr;
};
// Health Temporary State Effect - END.