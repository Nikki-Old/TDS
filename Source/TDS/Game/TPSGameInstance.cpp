// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/TPSGameInstance.h"

DEFINE_LOG_CATEGORY_STATIC(LogGameInstance, All, All)

void UTPSGameInstance::Init()
{
	Super::Init();
	CheckWeaponSlotsNum();
}

// Search Weapon info in WeaponInfoTable:
bool UTPSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo) const
{
	if (NameWeapon.IsNone())
		return false;

	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		// Get information from table:
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);

		if (WeaponInfoRow)
		{
			if (WeaponInfoRow->WeaponClass)
			{
				bIsFind = true;
				OutInfo = *WeaponInfoRow;
			}
			else
			{
				UE_LOG(LogTemp, Warning, (TEXT("UTPSGameInstance :: GetWeaponInfoByName - WeaponTable - NULL")));
			}
		}
	}
	return bIsFind;
}

// Search Item info in ItemInfoTable:
bool UTPSGameInstance::GetItemInfoByName(FName NameItem, FDropItemInfo& OutInfo) const
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		FDropItemInfo* ItemInfoRow;
		TArray<FName> RowNames = DropItemInfoTable->GetRowNames();

		// Get information from table:
		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			ItemInfoRow = DropItemInfoTable->FindRow<FDropItemInfo>(RowNames[i], "");
			if (ItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = *(ItemInfoRow);
				bIsFind = true;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, (TEXT("UTPSGameInstance::GetItemInfoByName - ItemInfoTable - NULL")));
	}
	return bIsFind;
}

bool UTPSGameInstance::GetLevelInfoByName(FName LevelName, FLevelInfo& OutInfo) const
{
	if (LevelName.IsNone())
	{
		UE_LOG(LogGameInstance, Warning, TEXT("GetlevelInfoByName - LevelName is None!"));
		return false;
	}

	bool bIsFind = false;
	FLevelInfo* LevelInfoRow;

	if (LevelsInfoTable)
	{
		// Get information from table:
		LevelInfoRow = LevelsInfoTable->FindRow<FLevelInfo>(LevelName, "", false);

		if (LevelInfoRow)
		{
			if (!LevelInfoRow->LevelName.IsNone())
			{
				bIsFind = true;
				OutInfo = *LevelInfoRow;
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, (TEXT("GetLevelInfoByName - LevelInfoTable - NULL")));
		}
	}
	return bIsFind;
}

void UTPSGameInstance::GetLevelsInfo(TArray<FLevelInfo>& LevelsInfo) const
{
	if (LevelsInfoTable)
	{
		for (auto LevelName : LevelsName)
		{
			if (!LevelName.IsNone())
			{
				auto CurrentInfo = LevelsInfoTable->FindRow<FLevelInfo>(LevelName, "", false);
				if (!CurrentInfo->LevelName.IsNone())
				{
					LevelsInfo.Add(*CurrentInfo);
				}
			}
		}
	}
	else
	{
		checkf(LevelsInfoTable, TEXT("LevelInfoTable is nullptr"));
	}
}

void UTPSGameInstance::SetStartupLevel(FLevelInfo& LevelInfo)
{
	if (!LevelInfo.LevelName.IsNone())
	{
		StartupLevelName = LevelInfo.LevelName;
	}
}

FName UTPSGameInstance::GetStartupLevelName() const
{
	return StartupLevelName;
}

FName UTPSGameInstance::GetDefaultLevelName() const
{
	return LevelsName[0];
}

FName UTPSGameInstance::GetLastLevelName() const
{
	return LastLevelName;
}

void UTPSGameInstance::SetLastLevelName(const FName NewLastLevelName)
{
	this->LastLevelName = NewLastLevelName;
}

TArray<FWeaponSlot> UTPSGameInstance::GetPlayerWeaponSlots() const
{
	return PlayerWeaponSlots;
}

TArray<FAmmoSlot> UTPSGameInstance::GetPlayerAmmoSlots() const
{
	return PlayerAmmoSlots;
}

void UTPSGameInstance::SetPlayerWeaponSlots(const TArray<FWeaponSlot> NewPlayerWeaponSlots, bool bIsSave)
{
	PlayerWeaponSlots = NewPlayerWeaponSlots;
}

void UTPSGameInstance::SetPlayerAmmoSlots(const TArray<FAmmoSlot> NewPlayerAmmoSlots, bool bIsSave)
{
	PlayerAmmoSlots = NewPlayerAmmoSlots;
}

void UTPSGameInstance::LoadNewLevel_Implementation(FName NewLevelName)
{
	if (!GetWorld())
	{
		return;
	}

	const auto CurrentLevelName = UGameplayStatics::GetCurrentLevelName(GetWorld());

	if (GetLevelInfoByName(NewLevelName, CurrentLevelInfo))
	{
		OnStartLoadLevel.Broadcast(CurrentLevelInfo);
		bIsLoadLevel = true;

		if (CurrentLevelName != NewLevelName.ToString())
		{
			UGameplayStatics::OpenLevel(GetWorld(), NewLevelName);
		}

		SetCurrentGameState(CurrentLevelInfo.GameState);
		StartLoadSublevels();

		this->SetLastLevelName(NewLevelName);
	}
}

void UTPSGameInstance::CallOnEndLoadLevel()
{
	bIsLoadLevel = false;
	OnEndLoadLevel.Broadcast();
}

void UTPSGameInstance::CallOnStartLoadSubLevel()
{
	OnStartLoadSublevel.Broadcast();
}

void UTPSGameInstance::CallOnEndLoadSubLevel()
{
	OnEndLoadSublevel.Broadcast();
}

void UTPSGameInstance::StartLoadSublevels_Implementation()
{
}

void UTPSGameInstance::CheckWeaponSlotsNum()
{
	while (PlayerWeaponSlots.Num() > TPSConstant::MAX_WEAPON_SLOTS)
	{
		auto LastIndexWeaponSlot = PlayerWeaponSlots.Num() - 1;
		PlayerWeaponSlots.RemoveAt(LastIndexWeaponSlot);
	}
}

void UTPSGameInstance::SetCurrentGameState_Implementation(ETDSGameState NewGameState)
{
	CurrentGameState = NewGameState;
}
