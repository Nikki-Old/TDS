// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FuncLibrary/Types.h"
#include "FuncLibrary/Constant/TPSConstant.h"
#include "Engine/DataTable.h"
#include "Game/Weapon/WeaponDefault.h"
#include "Containers/EnumAsByte.h"
#include "TPSGameInstance.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStartLoadLevel, FLevelInfo, LevelInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndLoadLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartLoadSublevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndLoadSublevel);

UCLASS()
class TDS_API UTPSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;

#pragma region Table
public:
	// Search Weapon info in WeaponInfoTable:
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo) const;

	UFUNCTION(BlueprintCallable)
	bool GetItemInfoByName(FName NameItem, FDropItemInfo& OutInfo) const;

	UFUNCTION(BlueprintCallable, Category = "Level")
	bool GetLevelInfoByName(FName LevelName, FLevelInfo& OutInfo) const;

protected:
	// DT Weapon:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UDataTable* WeaponInfoTable = nullptr;

	// DT Drop Item:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	UDataTable* DropItemInfoTable = nullptr;

	// DT Levels:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
	UDataTable* LevelsInfoTable = nullptr;

#pragma endregion

public:
	UFUNCTION(BlueprintCallable, Category = "Level")
	void GetLevelsInfo(TArray<FLevelInfo>& LevelsInfo) const;

	UFUNCTION(BlueprintCallable, Category = "Level")
	void SetStartupLevel(FLevelInfo& LevelInfo);

	UFUNCTION(BlueprintCallable, Category = "Level")
	FName GetStartupLevelName() const;

	UFUNCTION(BlueprintCallable, Category = "Level")
	FName GetDefaultLevelName() const;

	UFUNCTION(BlueprintCallable, Category = "Level")
	FName GetLastLevelName() const;

	UFUNCTION(BlueprintCallable, Category = "Level")
	void SetLastLevelName(const FName LastLevelName);

	// Get Saved Player Weapon Slots:
	UFUNCTION(BlueprintCallable, Category = "PlayerInfo | Inventory")
	TArray<FWeaponSlot> GetPlayerWeaponSlots() const;

	// Get Saved Player Weapon Slots:
	UFUNCTION(BlueprintCallable, Category = "PlayerInfo | Inventory")
	TArray<FAmmoSlot> GetPlayerAmmoSlots() const;

	// Set Player Weapon slots for save:
	UFUNCTION(BlueprintCallable, Category = "PlayerInfo | Inventory")
	void SetPlayerWeaponSlots(const TArray<FWeaponSlot> NewPlayerWeaponSlots, bool bIsSave = false);

	// Set Player Ammo slots for save:
	UFUNCTION(BlueprintCallable, Category = "PlayerInfo | Inventory")
	void SetPlayerAmmoSlots(const TArray<FAmmoSlot> NewPlayerAmmoSlots, bool bIsSave = false);

#pragma region Level
public:
	/** Call if start load level in "LoadNewLevel" function */
	UPROPERTY(BlueprintAssignable, Category = "Level")
	FOnStartLoadLevel OnStartLoadLevel;

	/** Call if end load sublevels in BP_GameInstance class */
	UPROPERTY(BlueprintAssignable, Category = "Level")
	FOnEndLoadLevel OnEndLoadLevel;

	/** Call if start load Sublevels in "LoadSublevels" function */
	UPROPERTY(BlueprintAssignable, Category = "Level")
	FOnStartLoadSublevel OnStartLoadSublevel;

	/** Call if end load Sublevels in BP_GameInstance class */
	UPROPERTY(BlueprintAssignable, Category = "Level")
	FOnEndLoadSublevel OnEndLoadSublevel;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Level")
	void LoadNewLevel(FName NewLevelName);
	void LoadNewLevel_Implementation(FName NewLevelName);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Level")
	bool IsLoadLevel() const { return bIsLoadLevel; }

protected:
	UFUNCTION(BlueprintCallable, Category = "Level")
	void CallOnEndLoadLevel();

	UFUNCTION(BlueprintCallable, Category = "Level")
	void CallOnStartLoadSubLevel();

	UFUNCTION(BlueprintCallable, Category = "Level")
	void CallOnEndLoadSubLevel();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Level")
	bool bIsLoadLevel = false;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Level")
	void StartLoadSublevels();
	void StartLoadSublevels_Implementation();

#pragma endregion

	void CheckWeaponSlotsNum();

#pragma region GameState

public:
	/** For take current information in TDSGameState: */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GameState")
	ETDSGameState GetCurrentGameState() const { return CurrentGameState; }

protected:
	/** For Save current game state */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameState")
	void SetCurrentGameState(ETDSGameState NewGameState);
	void SetCurrentGameState_Implementation(ETDSGameState NewGameState);

	/** Current Game State */
	UPROPERTY()
	ETDSGameState CurrentGameState = ETDSGameState::InMain_State;

#pragma endregion
#pragma region Level

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Level")
	FLevelInfo GetLevelInfo() const { return CurrentLevelInfo; }

protected:
	// All level names:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Level")
	TArray<FName> LevelsName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Level")
	FName StartupLevelName = NAME_None;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Level")
	FName LastLevelName = NAME_None;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Level")
	FLevelInfo CurrentLevelInfo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Level")
	FName MainMenuLevelName = NAME_None;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Level")
	FName GetMainMenuLevelName() const { return MainMenuLevelName; }
#pragma endregion

protected:
	// Player Weapon Slots:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerInfo | Inventory")
	TArray<FWeaponSlot> PlayerWeaponSlots;

	// Player Weapon Slots:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerInfo | Inventory")
	TArray<FAmmoSlot> PlayerAmmoSlots;
};
