// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPSPlayerController.h"

#include "Game/PlayerState/TPSPlayerState.h"
#include "PlayerCharacter.h"
#include "Engine/World.h"

ATPSPlayerController::ATPSPlayerController()
{
	bShowMouseCursor = false;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATPSPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void ATPSPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (InputComponent)
	{
		InputComponent->BindAction("ToggleMenu", IE_Pressed, this, &ATPSPlayerController::ToggleMenu);
		{
			//// Create the delegate to connect with our input bind:
			//// Pressed:
			//FInputActionHandlerSignature ActionMenuOn;
			//ActionMenuOn.BindUFunction(this, TEXT("PlayerNeedMenu"), true);

			//FInputActionBinding ActionMenuOnBinding("ToggleMenu", IE_Pressed);
			//ActionMenuOnBinding.ActionDelegate = ActionMenuOn;

			//// Realeased:
			//FInputActionHandlerSignature ActionMenuOff;
			//ActionMenuOff.BindUFunction(this, TEXT("PlayerNeedMenu"), false);

			//FInputActionBinding ActionMenuOffBinding("ToggleMenu", IE_Released);
			//ActionMenuOffBinding.ActionDelegate = ActionMenuOff;

			//InputComponent->AddActionBinding(ActionMenuOnBinding);
			//InputComponent->AddActionBinding(ActionMenuOffBinding);
		}
	}
}

void ATPSPlayerController::ToggleMenu_Implementation()
{
	OnPlayerNeedMenu.Broadcast(!bIsShowMenu);
}

void ATPSPlayerController::BeginPlayingState()
{
	Super::BeginPlayingState();

	PlayerState = GetPlayerState<ATPSPlayerState>();
}

void ATPSPlayerController::PlayerIsDead_Implementation()
{
	if (PlayerState)
	{
		PlayerState->SavePlayerInformation();
	}

	UnPossess();
}

void ATPSPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}
