// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TPSPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerNeedMenu, bool, IsPause);

UCLASS()
class ATPSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATPSPlayerController();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "TPSPlayerController")
	void PlayerIsDead();
	void PlayerIsDead_Implementation();

	UPROPERTY(BlueprintAssignable, Category = "PlayerController")
	FOnPlayerNeedMenu OnPlayerNeedMenu;

protected:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PlayerController")
	void ToggleMenu();
	void ToggleMenu_Implementation();

	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void BeginPlayingState();
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Override base Function PlayerController: */
	virtual void OnUnPossess() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PlayerController")
	bool bIsShowMenu = false;

private:
	class ATPSPlayerState* PlayerState = nullptr;
};
