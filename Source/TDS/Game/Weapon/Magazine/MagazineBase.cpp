// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/Weapon/Magazine/MagazineBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMagazineBase::AMagazineBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Mesh settings:
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh component"));
	Mesh->bReturnMaterialOnMove = true; 												// hit event return physMaterial
	Mesh->SetCanEverAffectNavigation(false); 											// collision not affect navigation (P keybord on editor)
	Mesh->BodyInstance.SetCollisionProfileName(TEXT("ShellBullet")); 					// Set the sphere's collision profile name to "ShellBullet":
	RootComponent = Mesh;
	Mesh->OnComponentHit.AddDynamic(this, &AMagazineBase::MeshOnHit);
	
	// Sets base values:
	bDoOnceSpawnSound = false;
}

// Called when the game starts or when spawned
void AMagazineBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMagazineBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// After spawning, sets parametrs from weapon table:
void AMagazineBase::InitMagazine(FDropMagazineInfo SettingsMagazine)
{
	this->SetLifeSpan(SettingsMagazine.MeshLifeTime);

	if (SettingsMagazine.Mesh)
	{
		Mesh->SetStaticMesh(SettingsMagazine.Mesh); // Set mesh.
	}

	// Save trancferred settings:
	Info = SettingsMagazine;
}

void AMagazineBase::MeshOnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// If has Hit...
	if (OtherActor)
	{
		/* Set sound Weapon. */
		if (Info.SoundOfFalling)
		{
			if (!bDoOnceSpawnSound) // ... do once...
			{
				float PitchMin = Info.PitchMultMin;
				float PitchMax = Info.PitchMultMax;
				float VolumeMult = Info.VolumeMult;
				
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Info.SoundOfFalling, Hit.Location, this->GetActorRotation(), FMath::FRandRange(PitchMin, PitchMax) * VolumeMult, FMath::FRandRange(PitchMin, PitchMax)); // Add USoundAttenuation * AttenuationSettings.
				bDoOnceSpawnSound = true;
			}
		}
	}
}

