// Fill out your copyright notice in the Description page of Project Settings.
#include "Game/Weapon/OtherWeapons/RocketWeaponBase.h"
#include "ActorComponent/CustomMovementComponent/TPSCharacterMovementComponent.h"

void ARocketWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	OwnerMovementComp = Cast<UTPSCharacterMovementComponent>(GetOwner()->GetComponentByClass(UTPSCharacterMovementComponent::StaticClass()));
}
void ARocketWeaponBase::Fire()
{
	if (OwnerMovementComp && OwnerMovementComp->AimEnabled)
	{
		Super::Fire();
	}
}