// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponDefault.h"
#include "RocketWeaponBase.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ARocketWeaponBase : public AWeaponDefault
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	virtual void Fire() override;

	class UTPSCharacterMovementComponent* OwnerMovementComp = nullptr;
};
