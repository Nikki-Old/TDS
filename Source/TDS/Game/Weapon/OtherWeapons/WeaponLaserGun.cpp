// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/Weapon/OtherWeapons/WeaponLaserGun.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AWeaponLaserGun::AWeaponLaserGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setting ChargingSphere:
	ChargingSphere = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ChargingSphere"));
	ChargingSphere->SetupAttachment(ShootLocation);
	ChargingSphere->SetCollisionProfileName(TEXT("NoCollision"));
	ChargingSphere->SetGenerateOverlapEvents(false);

	// Settings Laser:
	Laser = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Laser"));
	Laser->SetupAttachment(ShootLocation);
	if (Laser)
		Laser->Deactivate();

	// Set base values:
	ChargingSphereReady = false;
	NeedScaleForFire = FVector(2.f, 2.f, 2.f);
	SizeIncreaseDelta = FVector(0.1f, 0.1f, 0.1f);
}
/*void AWeaponLaserGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}*/
void AWeaponLaserGun::FireTick(float DeltaTime)
{
	if (WeaponFiring) //...cooldown over...
	{
		if (GetWeaponRound() > 0) // If have ammo...
		{
			if (ChargingSphereReady) //...and can fire...
			{
				//Fire();
				// Activate Laser:
				if (Laser)
					if (!Laser->IsActive())
						Laser->Activate();
			}
			else
			{
				if (ChargingSphere)
				{
					if (!UKismetMathLibrary::EqualEqual_VectorVector(ChargingSphere->GetComponentScale(), NeedScaleForFire, 0.5f))
					{
					
						ChargingSphere->SetRelativeScale3D(ChargingSphere->GetComponentScale() + SizeIncreaseDelta);
					}
					else
					{
						ChargingSphereReady = true;
					}
				}
				else
				{
					ChargingSphere->SetStaticMesh(ChargingSphereMesh);
				}
			}
		}
		else //...�lse start reload;
		{
			if (!WeaponReloading)
				InitReload();

			if (Laser->IsActive())
				Laser->Deactivate();
		}
	}
}

void AWeaponLaserGun::Fire()
{
	// Activate Laser:
	if (Laser)
		if (!Laser->IsActive())
			Laser->Activate();
	// Set character shooting animation:
	UAnimMontage* AnimToPlayCharacter = nullptr;
	UAnimMontage* AnimToPlayWeapon = nullptr;

	if (WeaponAiming)
		AnimToPlayCharacter = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
	else
		AnimToPlayCharacter = WeaponSetting.AnimWeaponInfo.AnimCharFire;

	AnimToPlayWeapon = WeaponSetting.AnimWeaponInfo.AnimWeaponFire;
	AnimFireStart(AnimToPlayWeapon);

	FireTimer = WeaponSetting.RateOfFire; // Set timer how can often fire.

	// Decreasing current ammo, chaecking !< 0 and !> Max rounds:
	AdditionalWeaponInfo.CurrentRound = FMath::Clamp((AdditionalWeaponInfo.CurrentRound - GetNumberProjectileByShot()), 0, WeaponSetting.MaxRounds);

	ChangeDispersionByShot();

	OnWeaponFireStart.Broadcast(AnimToPlayCharacter);

	// Set sound shot Weapon:
	if (WeaponSetting.SoundFireWeapon)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	// Set particle with the shot from Weapon:
	if (WeaponSetting.EffectFireWeapon)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentLocation());

	// How much projectile one shot


}
