// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/Weapon/WeaponDefault.h"
#include "WeaponLaserGun.generated.h"

/**
 * 
 */

UCLASS()
class TDS_API AWeaponLaserGun : public AWeaponDefault
{
	GENERATED_BODY()

public:
	AWeaponLaserGun();
	// Object for charging shot:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		UStaticMeshComponent* ChargingSphere = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "ChargingSphere")
		UStaticMesh* ChargingSphereMesh = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		UParticleSystemComponent* Laser = nullptr;

	// Flags:
	bool ChargingSphereReady;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "ChargingSphere")
	FVector NeedScaleForFire = FVector(0);
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "ChargingSphere")
	FVector SizeIncreaseDelta = FVector(0);

	/*// Called every frame
	virtual void Tick(float DeltaTime) override;*/

	// Overloading firing function:
	
	void FireTick(float DeltaTime) override;

	// Overloading firing function:
	
	void Fire() override;
	
};
