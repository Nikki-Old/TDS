// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/Weapon/Projectiles/ProjectileDefault.h"

#include "FuncLibrary/PrintString.h"
#include "Interface/StateEffectInterface.h"
#include "Utility.h"
#include "FuncLibrary/TPSSimpleThingsLibrary.h"

#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollisionProjectile = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere")); // what touches with other actor's

	SphereCollisionProjectile->SetSphereRadius(15.0f);
	SphereCollisionProjectile->bReturnMaterialOnMove = true;	  //hit event return physMaterial
	SphereCollisionProjectile->SetCanEverAffectNavigation(false); //collision not affect navigation (P keybord on editor)
	// Set the sphere's collision profile name to "Projectile"
	SphereCollisionProjectile->BodyInstance.SetCollisionProfileName(TEXT("Projectile"));

	// Event's:
	SphereCollisionProjectile->OnComponentHit.AddDynamic(this, &AProjectileDefault::ProjectileCollisionSphereHit);					 // Hit
	SphereCollisionProjectile->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::ProjectileCollisionSphereBeginOverlap); // Begin Overlap
	SphereCollisionProjectile->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::ProjectileCollisionSphereEndOverlap);	 // EndOverlap

	RootComponent = SphereCollisionProjectile;

	StaticMeshProjectile = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh component"));
	StaticMeshProjectile->SetupAttachment(RootComponent);
	StaticMeshProjectile->SetCanEverAffectNavigation(false);
	StaticMeshProjectile->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	ParticleSystemProjectile = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle System"));
	ParticleSystemProjectile->SetupAttachment(RootComponent);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile movement component"));
	// What object we update?
	ProjectileMovement->UpdatedComponent = RootComponent; //SetUpdatedComponent(RootComponent);

	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	ProjectileMovement->ProjectileGravityScale = 0.0f;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	// Set speed:
	ProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	ProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	/*printf_k(2, "Projectile IitialSpeed: %f", ProjectileMovement->InitialSpeed);
	printf_k(2, "Projectile MaxSpeed: %f", ProjectileMovement->MaxSpeed);*/

	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	//Logic delete mesh or particle:
	//if we no have component, destroy this...
	if (InitParam.ProjectileStaticMesh)
	{
		StaticMeshProjectile->SetStaticMesh(InitParam.ProjectileStaticMesh);
	}

	if (InitParam.ParticleProjectile)
	{
		ParticleSystemProjectile->SetTemplate(InitParam.ParticleProjectile);
	}

	if (!ParticleSystemProjectile && StaticMeshProjectile->GetStaticMesh())
	{
		ParticleSystemProjectile->DestroyComponent(); //...Static mesh.
	}

	ProjectileSettings = InitParam;
}

// Hit Event:
void AProjectileDefault::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// If has Hit and this object have PhysMaterial...
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{

		EPhysicalSurface HitSurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		// Decal:
		if (ProjectileSettings.HitDecals.Contains(HitSurfaceType))
		{
			UMaterialInterface* myMaterial = ProjectileSettings.HitDecals[HitSurfaceType];

			if (myMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(10.f), OtherComp, NAME_None, Hit.Location, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}

		// FX:
		if (ProjectileSettings.HitFXs.Contains(HitSurfaceType))
		{
			UParticleSystem* myParticle = ProjectileSettings.HitFXs[HitSurfaceType];
			if (myParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.Location, FVector(1.0f)));
			}
		}

		// Sound:
		if (ProjectileSettings.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.HitSound, Hit.ImpactNormal);
		}

		// State Effect:
		// Other Actor have Interface "State Effect Interface"?
		if (OtherActor->GetClass()->ImplementsInterface(UStateEffectInterface::StaticClass()))
		{
			// Is Can Set new State Effect?
			if (IStateEffectInterface::Execute_IsCanSetStateEffect(OtherActor))
			{
				UStateEffect* StateEffect = nullptr;

				if (UTPSSimpleThingsLibrary::AddStateEffectBySurface(OtherActor, GetInstigator(), ProjectileSettings.StateEffect, HitSurfaceType, StateEffect))
				{
					if (StateEffect)
					{
						//// StateEffect is added:
						//StateEffect->InitObject(OtherActor);
					}
				}
			}
		}
	}

	// Apply Damage:
	UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSettings.ProjectileDamage, Hit.Location, Hit, GetInstigatorController(), GetOwner()->GetOwner(), NULL);

	ImpactProjectile();
}

// BeginOverlap Event:
void AProjectileDefault::ProjectileCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

// EndOverlap Event:
void AProjectileDefault::ProjectileCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}
