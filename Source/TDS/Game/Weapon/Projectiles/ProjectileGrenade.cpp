// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/Weapon/Projectiles/ProjectileGrenade.h"
#include "Kismet/GameplayStatics.h"
#include "Interface/StateEffectInterface.h"
#include "FuncLibrary/TPSSimpleThingsLibrary.h"

#include "Kismet/KismetSystemLibrary.h"

// Create log for this class:
DEFINE_LOG_CATEGORY_STATIC(LogProjectileGrenade, All, All)

void AProjectileGrenade::BeginPlay()
{
	Super::BeginPlay();

	ImpactProjectile();
}

void AProjectileGrenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileGrenade::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::ProjectileCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileGrenade::Explose()
{
	if (!GetWorld())
		return;

	if (ProjectileSettings.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplodeFX, GetActorLocation(), GetActorRotation());
	}

	if (ProjectileSettings.ExplodeSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ProjectileSettings.ExplodeSound, GetActorLocation());
	}

	if (bIsShowDebug)
	{
		if (GetWorld())
		{
			// Draw min radius:
			UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ProjectileMinRadiusDamage, 12, FLinearColor::Red, 10.0f, 10.0f);

			// Draw max radius:
			UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ProjectileMaxRadiusDamage, 12, FLinearColor::Green, 10.0f, 10.0f);
		}
	}

	// Set Radial Damage:
	TArray<AActor*> IgnoreActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSettings.ExplodeMaxDamage,
		ProjectileSettings.ExplodeMaxDamage * 0.2f,
		GetActorLocation(), ProjectileSettings.ProjectileMinRadiusDamage, ProjectileSettings.ProjectileMaxRadiusDamage, 5, NULL, IgnoreActor, GetInstigator(), nullptr);

	// State Effect:
	// Get All Actor in Damage Radius:
	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	//TraceObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery7);

	TArray<AActor*> OutActors;

	auto DamageActors = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), //
		GetActorLocation(),													  //
		ProjectileSettings.ProjectileMaxRadiusDamage, TraceObjectTypes, AActor::StaticClass(), IgnoreActor, OutActors);

	for (auto TargetActor : OutActors)
	{
		if (TargetActor->GetClass()->ImplementsInterface(UStateEffectInterface::StaticClass()))
		{
			// Is Can Set new State Effect?
			if (IStateEffectInterface::Execute_IsCanSetStateEffect(TargetActor))
			{
				UStateEffect* StateEffect = nullptr;

				if (UTPSSimpleThingsLibrary::AddStateEffectBySurface(TargetActor, GetInstigator(), ProjectileSettings.StateEffect, EPhysicalSurface::SurfaceType_Default, StateEffect))
				{
					if (bIsShowDebug)
					{
						UE_LOG(LogProjectileGrenade, Display, TEXT("Added State effect \"%s\" to \"%s\"."), *StateEffect->GetName(), *TargetActor->GetName());
					}
				}
				else
				{
					// StateEffect is added:
					if (StateEffect)
					{
						//StateEffect->InitObject(TargetActor);

					}
				}
			}
		}
	}

	this->Destroy();
}

void AProjectileGrenade::ImpactProjectile()
{
	if (!GetWorld())
	{
		return;
	}

	if (ProjectileSettings.TimeToExplose <= 0.0f)
	{
		Explose();
	}
	else
	{
		GetWorld()->GetTimerManager().SetTimer(Explose_Timer, this, &AProjectileGrenade::Explose, ProjectileSettings.TimeToExplose, false);
	}
}
