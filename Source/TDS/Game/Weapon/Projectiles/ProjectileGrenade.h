// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/Weapon/Projectiles/ProjectileDefault.h"
#include "ProjectileGrenade.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AProjectileGrenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Debug")
	bool bIsShowDebug = false;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ProjectileCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	UFUNCTION()
	void Explose();

	virtual void ImpactProjectile();

private:
	FTimerHandle Explose_Timer;
};
