// Fill out your copyright notice in the Description page of Project Settings.

#include "RocketProjectile.h"
#include "Kismet/GameplayStatics.h"

ARocketProjectile::ARocketProjectile() {}
void ARocketProjectile::BeginPlay()
{
	Super::BeginPlay();
	StartFlight();
}

void ARocketProjectile::ImpactProjectile()
{
	Explose();
	ClearSpeedIncreaseTimer();
	this->Destroy();
}

void ARocketProjectile::StartFlight()
{
	if (!GetWorld() || SpeedIncreaseTimer.IsValid())
		return;

	/** Start Speed Increase Timer: */
	GetWorld()->GetTimerManager().SetTimer(SpeedIncreaseTimer, this, &ARocketProjectile::IncreaseSpeed, SpeedIncreaseFrequency, true, StartDelayToIncreaseSpeed);
}

void ARocketProjectile::IncreaseSpeed()
{
	this->ProjectileMovement->Velocity *= CoefSpeedIncrease;
}

void ARocketProjectile::Explose()
{
	if (ProjectileSettings.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplodeFX, GetActorLocation(), GetActorRotation());
	}
	if (ProjectileSettings.ExplodeSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ProjectileSettings.ExplodeSound, GetActorLocation());
	}

	TArray<AActor*> IgnoreActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSettings.ExplodeMaxDamage,
		ProjectileSettings.ExplodeMaxDamage * 0.2f,
		GetActorLocation(), ProjectileSettings.ProjectileMinRadiusDamage, ProjectileSettings.ProjectileMaxRadiusDamage, 5, NULL, IgnoreActor, this, nullptr);
}

void ARocketProjectile::ClearSpeedIncreaseTimer()
{
	SpeedIncreaseTimer.Invalidate();
}
