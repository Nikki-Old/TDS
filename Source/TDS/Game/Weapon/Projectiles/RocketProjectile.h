// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "RocketProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ARocketProjectile : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	ARocketProjectile();

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Rocket", meta = (ClamMin = "0", UMin = "0"))
	//float FitstSpeed = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Rocket", meta = (ClamMin = "0", UMin = "0"))
	float StartDelayToIncreaseSpeed = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Rocket", meta = (ClamMin = "0", UMin = "0"))
	float SpeedIncreaseFrequency = 0.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile | Rocket", meta = (ClamMin = "0", UMin = "0"))
	float CoefSpeedIncrease = 0.0f;

	virtual void ImpactProjectile() override;

private:

	/** Start launch Rocket: */
	void StartFlight();

	/** Increase speed: */
	void IncreaseSpeed();

	/** Explose: */
	void Explose();

	/** Timer: */
	FTimerHandle SpeedIncreaseTimer;

	void ClearSpeedIncreaseTimer();
};
