// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/Types.h"
#include "ShellBulletBase.generated.h"

UCLASS()
class TDS_API AShellBulletBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShellBulletBase();

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* Mesh = nullptr;

	// Settings Shell Bullet:
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "SettingsShellBullet")
		FDropShellBulletInfo Info;

	// Flags:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Flags")
		bool bDoOnceSpawnSound;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// After spawning, sets parametrs from weapon table:
	UFUNCTION()
		void InitShellBullet(FDropShellBulletInfo SettingsShellBullet);

	UFUNCTION()
		void MeshOnHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

};
