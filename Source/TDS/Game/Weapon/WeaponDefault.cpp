// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/Weapon/WeaponDefault.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"

#include "Game/Weapon/ShellBullet/ShellBulletBase.h"
#include "Game/Weapon/Magazine/MagazineBase.h"
#include "ActorComponent/TPSInventoryComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeapon, All, All)

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeleta Mesh"));
	// Setting collision skeletal mesh:
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	// Attach to root
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	// Setting collision static mesh:
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Arrow"));
	// Setting location of the shooting:
	ShootLocation->SetupAttachment(RootComponent);

	DropShellBulletLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropShellBullet Arrow"));
	// Setting location of the spawn Shell bullet:
	DropShellBulletLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	// ClipDropTick(DeltaTime);
	ShellDropTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring && !WeaponReloading) // Can fire...if have ammo...
	{
		if (GetWeaponRound() > 0)
		{

			if (FireTimer < 0.0f) // and cooldown over...
			{
				Fire(); //...shoot.
			}
			else
			{
				FireTimer -= DeltaTime; //...or wait and cooldown.
			}
		}
		else
		{
			InitReload();
		}
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading) // if need reloading...
	{
		if (ReloadTime <= 0.0f) //...and cooldown over...
		{
			FinishedReload(); //...reloading.
		}
		else
		{
			ReloadTime -= DeltaTime; //...or wait cooldown.
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading) // If weapon is not reloading...
	{
		if (!WeaponFiring) // ...and not shooting...
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion -= CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion += CurrentDispersionReduction;
			}
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}

	if (ShowDebug)
	{
		UE_LOG(LogWeapon, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f."), CurrentDispersionMax, CurrentDispersionMin,
			CurrentDispersion);
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag) // If true...
	{
		if (DropShellTimer < 0.f) //...and time has come...
		{
			DropShellFlag = false;
			// Call Func with parapmetrs WeaponSettings.ShellBullets:
			InitDropShell(WeaponSetting.ShellBulletSetting);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

// Loads information from a table:
void AWeaponDefault::WeaponInit()
{
	// if we no have component, destroy this...
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true); //...Skeletal mesh.
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent(); //...Static mesh.
	}

	if (SkeletalMeshWeapon->SkeletalMesh)
	{
		ShootLocation->SetWorldLocation(SkeletalMeshWeapon->GetSocketLocation(TEXT("Spawn_Bullet")));
		ShootLocation->SetWorldRotation(SkeletalMeshWeapon->GetSocketRotation(TEXT("Spawn_Bullet")));

		DropShellBulletLocation->SetWorldLocation(SkeletalMeshWeapon->GetSocketLocation(TEXT("Spawn_ShellBullet")));
		DropShellBulletLocation->SetWorldRotation(SkeletalMeshWeapon->GetSocketRotation(TEXT("Spawn_ShellBullet")));
	}

	if (StaticMeshWeapon->GetStaticMesh())
	{
		ShootLocation->SetWorldLocation(StaticMeshWeapon->GetSocketLocation(TEXT("Spawn_Bullet")));
		ShootLocation->SetWorldRotation(StaticMeshWeapon->GetSocketRotation(TEXT("Spawn_Bullet")));

		DropShellBulletLocation->SetWorldLocation(StaticMeshWeapon->GetSocketLocation(TEXT("Spawn_ShellBullet")));
		DropShellBulletLocation->SetWorldRotation(StaticMeshWeapon->GetSocketRotation(TEXT("Spawn_ShellBullet")));
	}

	// Set start status Weapon.
	UpdateStateWeapon(ECharacterMovementState::Run_State);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (IsCanFire())
	{
		WeaponFiring = bIsFire; //...Take from Character input AttacEvent.
	}
	else
	{
		WeaponFiring = false;
		FireTimer = 0.01f;
	}
}

bool AWeaponDefault::IsCanFire() // if retun true, can fire.
{
	return !BlockFire;
}

// Set dispersion dependence on MovementState:
void AWeaponDefault::UpdateStateWeapon(ECharacterMovementState NewMovementState)
{
	// To do Dispersion:
	BlockFire = false;

	switch (NewMovementState)
	{
		case ECharacterMovementState::Aim_State:
			WeaponAiming = true;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
			break;

		case ECharacterMovementState::AimWalk_State:
			WeaponAiming = true;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionReduction;
			break;

		case ECharacterMovementState::Walk_State:
			WeaponAiming = false;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
			break;

		case ECharacterMovementState::Run_State:
			WeaponAiming = false;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
			break;

		case ECharacterMovementState::SprintRun_State:
			WeaponAiming = false;
			BlockFire = true;
			SetWeaponStateFire(false); // Set fire trigger to false.
			break;

		default:
			break;
	}
}

void AWeaponDefault::Fire()
{
	// If not have Owner - return;
	if (!GetOwner())
		return;

	FireTimer = WeaponSetting.RateOfFire; // Set timer how can often fire.

	/* How much projectile one shot? */
	int32 NumberProjectile = FMath::Clamp(GetNumberProjectileByShot(), 0, AdditionalWeaponInfo.CurrentRound);

	// Decreasing current ammo, checking !< 0 and !> Max rounds.
	AdditionalWeaponInfo.CurrentRound =
		FMath::Clamp((AdditionalWeaponInfo.CurrentRound - GetNumberProjectileByShot()), 0, WeaponSetting.MaxRounds);
	ChangeDispersionByShot();

	// Animation start:
	// Set character shooting animation :
	UAnimMontage* AnimToPlayCharacter = nullptr;
	UAnimMontage* AnimToPlayWeapon = nullptr;

	if (WeaponAiming)
		AnimToPlayCharacter = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
	else
		AnimToPlayCharacter = WeaponSetting.AnimWeaponInfo.AnimCharFire;

	AnimToPlayWeapon = WeaponSetting.AnimWeaponInfo.AnimWeaponFire;
	AnimFireStart(AnimToPlayWeapon);

	OnWeaponFireStart.Broadcast(AnimToPlayCharacter);
	// Animation end

	// FX start:
	/*Set sound Weapon.*/
	if (WeaponSetting.SoundFireWeapon)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());

	/*Set particle with the shot from Weapon.*/
	if (WeaponSetting.EffectFireWeapon)
	{
		UParticleSystemComponent* SpawnEmitter = nullptr;

		// !!! Need add logic for intialize MESH!
		SpawnEmitter = UGameplayStatics::SpawnEmitterAttached(WeaponSetting.EffectFireWeapon, SkeletalMeshWeapon, (TEXT("Spawn_Bullet")));
	}
	// FX end:

	if (ShootLocation)
	{
		FTransform SpawnTransform;
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileSetting;
		ProjectileSetting = GetProjectileSettings();

		FVector EndLocation;

		// Checking have shell mesh:
		if (WeaponSetting.ShellBulletSetting.Mesh)
		{
			if (WeaponSetting.ShellBulletSetting.DropMeshTime < 0.0f)
			{
				InitDropShell(WeaponSetting.ShellBulletSetting);
			}
			else
			{
				DropShellFlag = true;
				DropShellTimer = WeaponSetting.ShellBulletSetting.DropMeshTime;
			}
		}

		for (int8 i = 0; i < NumberProjectile; i++)
		{
			/*Take last contact location*/
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize(); //... direction.

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = GetInstigator();

			SpawnTransform.SetLocation(SpawnLocation);
			SpawnTransform.SetRotation(SpawnRotation.Quaternion());
			SpawnTransform.SetScale3D(WeaponSetting.ProjectileSetting.MeshScale3D);

			if (ProjectileSetting.Projectile)
			{
				AProjectileDefault* myProjectile = GetWorld()->SpawnActorDeferred<AProjectileDefault>(ProjectileSetting.Projectile, SpawnTransform, this);
					//GetWorld()->SpawnActor<AProjectileDefault>(ProjectileSetting.Projectile, SpawnTransform, SpawnParams);
				if (myProjectile)
				{
					myProjectile->InitProjectile(ProjectileSetting);
					myProjectile->FinishSpawning(SpawnTransform);
					myProjectile->SetInstigator(GetInstigator());

				}
				else
				{
					FHitResult Hit;
					TArray<AActor*> Actors;

					EDrawDebugTrace::Type DebugTrace;
					if (ShowDebug)
					{
						DrawDebugLine(GetWorld(), SpawnLocation,
							SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace, FColor::Black, false, 5.f,
							(uint8)'\000', 0.5f);
						DebugTrace = EDrawDebugTrace::ForDuration;
					}
					else
						DebugTrace = EDrawDebugTrace::None;

					UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
						ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green,
						5.0f);

					if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
					{
						EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

						if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
						{
							UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];

							if (myMaterial && Hit.GetComponent())
							{
								UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None,
									Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
							}
						}
						if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
						{
							UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
							if (myParticle)
							{
								UGameplayStatics::SpawnEmitterAtLocation(
									GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
							}
						}

						if (WeaponSetting.ProjectileSetting.HitSound)
						{
							UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
						}

						UGameplayStatics::ApplyDamage(
							Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
					}
				}
			}
		}
	}

	/*if (GetWeaponRound() <= 0 && !WeaponReloading)
    {
          //Init Reload
          if (CheckCanWeaponReload())
                InitReload();
    }*/
}

void AWeaponDefault::AnimFireStart(UAnimMontage* Anim)
{
	AnimFireStart_BP(Anim);
}

void AWeaponDefault::AnimFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// In BP.
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform OffSet, FVector DropImpulseDirection, float LifeTimeMesh,
	float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;
		FVector LocalDir = this->GetActorForwardVector() * OffSet.GetLocation().X + this->GetActorRightVector() * OffSet.GetLocation().Y + this->GetActorUpVector() * OffSet.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(OffSet.GetScale3D());
		Transform.SetRotation((this->GetActorRotation() + OffSet.Rotator()).Quaternion());

		AStaticMeshActor* NewActor = nullptr;

		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = LifeTimeMesh;

			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMass > 0.f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if (!DropImpulseDirection.IsNearlyZero())
			{
				FVector FinalDir;
				LocalDir = LocalDir + (DropImpulseDirection * 1000.f);
				if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
				{
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
				}
				FinalDir.GetSafeNormal(0.0001f);

				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
			}
		}
	}
}

void AWeaponDefault::InitDropShell(FDropShellBulletInfo ShellBulletInfo)
{
	if (ShellBulletInfo.Mesh)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = GetOwner();
		SpawnParams.Instigator = GetInstigator();

		FTransform SpawnTransform;

		// SpawnTransform.SetLocation(SkeletalMeshWeapon->GetSocketLocation(TEXT("Spawn_ShellBullet")));
		// SpawnTransform.SetRotation(SkeletalMeshWeapon->GetSocketRotation(TEXT("Spawn_ShellBullet")).Quaternion());
		SpawnTransform.SetScale3D(ShellBulletInfo.MeshScale3D);
		SpawnTransform.SetLocation(DropShellBulletLocation->GetComponentLocation());
		SpawnTransform.SetRotation(DropShellBulletLocation->GetComponentRotation().Quaternion());

		AShellBulletBase* ShellBullet;
		ShellBullet = GetWorld()->SpawnActor<AShellBulletBase>(ShellBulletInfo.ShellBulletClass, SpawnTransform, SpawnParams);

		ShellBullet->InitShellBullet(ShellBulletInfo);
	}
}

void AWeaponDefault::InitReload()
{
	// If not have ammo in inventory - return:
	if (GetAvailableAmmoForReload() == 0)
		return;

	WeaponReloading = true;

	// Start animation reloading:
	if (WeaponSetting.AnimWeaponInfo.AnimCharReload)
	{
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimWeaponInfo.AnimCharReload);
	}
}

void AWeaponDefault::FinishedReload()
{
	WeaponReloading = false;
	int8 AvailableAmmoFromInventory = GetAvailableAmmoForReload();

	int8 AmmoNeedTakeFromInventory;
	int8 NeedToReload = WeaponSetting.MaxRounds - AdditionalWeaponInfo.CurrentRound;

	if (NeedToReload > AvailableAmmoFromInventory)
	{
		AdditionalWeaponInfo.CurrentRound = AvailableAmmoFromInventory;
		AmmoNeedTakeFromInventory = AvailableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.CurrentRound += NeedToReload;
		AmmoNeedTakeFromInventory = NeedToReload;
	}

	// End animation reloading:
	ReloadTime = WeaponSetting.ReloadTime;
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInventory);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	// DropClipGlag = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;

	if (ShowDebug)
		UE_LOG(LogWeapon, Error, TEXT("AWeaponDefault::CheckCanWeaponReload() - OwnerName - %s"), *GetOwner()->GetName());

	if (GetOwner())
	{
		UTPSInventoryComponent* myInventory =
			Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (myInventory)
		{
			int8 AvailableAmmoForWeapon;
			if (!myInventory->CheckAmmoForWeapon(WeaponSetting.TypeWeapon, AvailableAmmoForWeapon))
			{
				result = false;
			}
		}
	}
	return result;
}

FProjectileInfo AWeaponDefault::GetProjectileSettings() const
{
	return WeaponSetting.ProjectileSetting;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.CurrentRound;
}

void AWeaponDefault::StartInteract_Implementation(AActor* Caller)
{
}

void AWeaponDefault::StartUsage_Implementation(AActor* User)
{
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

// Releases a trace:
FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bSootDirection = false;
	FVector EndLocation = FVector(0.0f);

	FVector tmV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation()) - ShootEndLocation).GetSafeNormal() * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation),
				WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald,
				false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetForwardVector())) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), (ShootLocation->GetForwardVector()),
				WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald,
				false, .1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		// Direction weapon look:
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(),
			ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 0.1f, (uint8)'\000',
			1.0f);
		// Direction projectile must fly:
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 0.1f, (uint8)'\000', 0.5f);
		// Direction Projectile fly now:
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 0.1f, (uint8)'\000', 0.5f);

		// DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() *
		// SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

int32 AWeaponDefault::GetNumberProjectileByShot()
{
	return WeaponSetting.WasteOfAmmo;
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
	//int8 AvailableAmmoForWeapon = WeaponSetting.MaxRounds;
	int8 AvailableAmmoForWeapon = 0;

	if (GetOwner())
	{
		if (!OwnerInventory)
		{
			OwnerInventory =
				Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));

			checkf(OwnerInventory, TEXT("OwnerInventory cast to UTPSInventoryComponent is FAIL!"));
		}

		if (OwnerInventory)
		{
			if (OwnerInventory->CheckAmmoForWeapon(WeaponSetting.TypeWeapon, AvailableAmmoForWeapon))
			{
				AvailableAmmoForWeapon = AvailableAmmoForWeapon;
			}
		}
	}

	return AvailableAmmoForWeapon;
}
