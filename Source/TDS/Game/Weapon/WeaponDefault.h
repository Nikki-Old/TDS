// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "GameFramework/Actor.h"
#include "Item/ItemBase.h"
#include "Components/ArrowComponent.h"
#include "DrawDebugHelpers.h"

#include "FuncLibrary/Types.h"
#include "Game/Weapon/Projectiles/ProjectileDefault.h"
#include "Kismet/GameplayStatics.h"

#include "WeaponDefault.generated.h"

// Create delegate to call in Character for Play Animation:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);
// Create delegate to call in Character for Play Animation:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);
// Create delegate to call in Character for ...:
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoTake);

UCLASS()
class TDS_API AWeaponDefault : public AItemBase
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponDefault();

	/**Delegate: */
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
	FOnWeaponFireStart OnWeaponFireStart;	  //...informs all listeners about the start fire.
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
	FOnWeaponReloadStart OnWeaponReloadStart; //... informs all listeners about the start of reloading.
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
	FOnWeaponReloadEnd OnWeaponReloadEnd;	  //... informs all listeners about the end of Weapon reloading.

	// Base componennts:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr; //... root componennt.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr; //... bullet exit point.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* DropShellBulletLocation = nullptr; //...shell bullet exit point.

	/** Weapon information: */
	// Values for this variables is set from the Table:
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite, Category = "Weapon Setting's")
	FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Setting's")
	FAdditionalWeaponInfo AdditionalWeaponInfo;

	/** Flags: */
	// Can weapon fire?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
	bool WeaponFiring = false;
	// Weapon reloading/recharge?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
	bool WeaponReloading = false;
	//Weapon aim state?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
	bool WeaponAiming = false;
	// Stoped logic fire:
	bool BlockFire = false;

	//Timer how often Fire.
	float FireTimer = 0.0f;

	//Timer how often Reloading.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reload Logic")
	float ReloadTime = 0.0f;

	//Dispersion:
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	//For Displacement:
	UPROPERTY()
	FVector ShootEndLocation = FVector(0);

	//For Func GetFireEndLocation, this min length from cursor to character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GetFireEndLocation debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;

	/*// For Func ClipDropTick, timer Drop Magazine on reload:
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Drop Mesh info")
		bool DropClipFlag = false; // ... flag.*/
	//UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Drop Mesh info")
	//	float DropClipTimer = -1.0; // ... Timer.

	// For Func ShellDropTick, timer Drop Shell on fire:
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Drop Mesh info")
	bool DropShellFlag = false; // ... flag.
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Drop Mesh info")
	float DropShellTimer = 0.f; // ... Timer.

	/** Debug: */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;

private:
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Loads information from a table:
	UFUNCTION()
	void WeaponInit();

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire); //If get input set true.

	UFUNCTION()
	bool IsCanFire();

	// Set dispersion dependence on MovementState:
	UFUNCTION()
	void UpdateStateWeapon(ECharacterMovementState NewMovementState); //To do Dispersion.

	UFUNCTION()
	virtual void Fire();

	void AnimFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void AnimFireStart_BP(UAnimMontage* Anim);
	void AnimFireStart_BP_Implementation(UAnimMontage* Anim);

	UFUNCTION()
	void InitDropMesh(UStaticMesh* DropMesh,
		FTransform OffSet,
		FVector DropImpulseDirection,
		float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	UFUNCTION()
	void InitDropShell(FDropShellBulletInfo ShellBulletInfo);

	// Reload logic:
	UFUNCTION(BlueprintCallable)
	void InitReload();
	UFUNCTION()
	void FinishedReload();
	UFUNCTION()
	void CancelReload();
	UFUNCTION()
	bool CheckCanWeaponReload();

	//Take projectile settings
	UFUNCTION()
	FProjectileInfo GetProjectileSettings() const;

	UFUNCTION()
	float GetCurrentDispersion() const;
	UFUNCTION()
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	// Releases a trace:
	UFUNCTION()
	FVector GetFireEndLocation() const;
	UFUNCTION()
	void ChangeDispersionByShot();
	UFUNCTION()
	int32 GetNumberProjectileByShot();
	UFUNCTION()
	int8 GetAvailableAmmoForReload();
	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	// Interact
	void StartInteract_Implementation(AActor* Caller) override;

	// Usage
	void StartUsage_Implementation(AActor* User) override;

private:
	// Called every frame:
	UFUNCTION()
	virtual void FireTick(float DeltaTime);
	UFUNCTION()
	void ReloadTick(float DeltaTime);
	UFUNCTION()
	void DispersionTick(float DeltaTime);
	UFUNCTION()
	void ShellDropTick(float DeltaTime);

	UPROPERTY()
	class UTPSInventoryComponent* OwnerInventory = nullptr;
};
