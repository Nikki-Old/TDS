// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/WorldActor/Damage/TPSDamageActorBase.h"
#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "UI/ValueChange/TPSChangeValueWidgetBase.h"

// Sets default values
ATPSDamageActorBase::ATPSDamageActorBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Create WidgetComponent:
	DamageWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("DamageWidget"));
	SetRootComponent(DamageWidgetComponent);

	// Set Widget Class:
	if (IsValid(DamageWidgetClass))
		DamageWidgetComponent->SetWidgetClass(DamageWidgetClass);

	DamageWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
}

/*void ATPSDamageActorBase::SetLifeTimer()
{
	if (!GetWorld())
		return;

	GetWorld()->GetTimerManager().SetTimer(LifeTimerHandle, this, &ATPSDamageActorBase::DestroyEvent, LifeTime, false);
}*/

// Called when the game starts or when spawned
void ATPSDamageActorBase::BeginPlay()
{
	Super::BeginPlay();

	//SetLifeTimer();
}

// Called every frame
void ATPSDamageActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATPSDamageActorBase::UpdateDamageWidget(float Value, float Change)
{
	LeftValue = Value;
	auto DamageWidget = Cast<UTPSChangeValueWidgetBase>(DamageWidgetComponent->GetUserWidgetObject());
	if (DamageWidget)
	{
		DamageWidget->InitEvent(this, Change);
	}
}

void ATPSDamageActorBase::DestroyEvent()
{
	this->Destroy();
}
