// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPSDamageActorBase.generated.h"

UCLASS()
class TDS_API ATPSDamageActorBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATPSDamageActorBase();

	/** Widget component: */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Damage")
	class UWidgetComponent* DamageWidgetComponent = nullptr;

	/** Widget class: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
	TSubclassOf<class UTPSChangeValueWidgetBase> DamageWidgetClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Damage")
	float LeftValue;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Get Value(Health or shield point and etc.) and Change this Value: */
	UFUNCTION(BLueprintCallable, Category = "Damage")
	void UpdateDamageWidget(float Value, float Change);

	UFUNCTION(BLueprintCallable, Category = "Damage")
	void DestroyEvent();

	UFUNCTION(BlueprintCallable, Category = "Damage")
	float GetLeftValue() const { return LeftValue; }
};
