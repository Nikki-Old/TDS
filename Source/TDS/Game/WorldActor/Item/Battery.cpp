// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/WorldActor/Item/Battery.h"

using namespace TDS;

constexpr float ChargeAmount = 0.1f;

Battery::Battery(float PercentIn)
{
	SetPercent(PercentIn);
}

void TDS::Battery::Charge()
{
	SetPercent(Percent + ChargeAmount);
}

void Battery::UnCharge()
{
	SetPercent(Percent - ChargeAmount);
}

float Battery::GetPercent() const
{
	return Percent;
}

FColor Battery::GetColor() const
{
	if (Percent > 0.8)
	{
		return FColor::Green;
	}

	if (Percent > 0.3)
	{
		return FColor::Yellow;
	}

	return FColor::Red;
}

FString Battery::ToString() const
{
	return FString::Printf(TEXT("%i%%"), FMath::RoundToInt(GetPercent() * 100));
}

void Battery::SetPercent(float PercentIn)
{
	Percent = FMath::Clamp(PercentIn, 0.0f, 1.0f);
}
