// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

namespace TDS
{
	class TDS_API Battery
	{
	public:
		Battery() = default;
		Battery(float PercentIn);

		void Charge();
		void UnCharge();

		float GetPercent() const;
		FColor GetColor() const;
		FString ToString() const;

	private:
		float Percent{ 1.0f };
		void SetPercent(float PercentIn);
	};

} // namespace TDS
