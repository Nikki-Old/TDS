// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/WorldActor/Item/ItemBase.h"

// Sets default values
AItemBase::AItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AItemBase::BeginPlay()
{
	Super::BeginPlay();
	
	InitialItem();
}

void AItemBase::PickUpSuccess_Implementation()
{
	this->Destroy();
}

// Called every frame
void AItemBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AItemBase::InitialItem_Implementation()
{
}

void AItemBase::StartInteract_Implementation(AActor* Caller)
{
}

void AItemBase::SetWeaponDropInfo_Implementation(FWeaponSlot WeaponSlotInfo)
{
}

void AItemBase::EndInteract()
{
}

void AItemBase::SetCanInteract(bool NewCan)
{
	bIsCanInteract = NewCan;
}

void AItemBase::StartUsage(AActor* User)
{
}

void AItemBase::EndUsage()
{
}

