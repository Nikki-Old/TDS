// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/InteractInterface.h"
#include "Interface/UsageInterface.h"
#include "ItemBase.generated.h"

UCLASS()
class TDS_API AItemBase : public AActor, public IInteractInterface, public IUsageInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AItemBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** This function need override: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	void PickUpSuccess();
	virtual void PickUpSuccess_Implementation();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** This function called in ItemBase(BeginPlay): */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	void InitialItem();
	virtual void InitialItem_Implementation();

	/** Interact Inteface: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	void StartInteract(AActor* Caller);
	void StartInteract_Implementation(AActor* Caller);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	void SetWeaponDropInfo(FWeaponSlot WeaponSlotInfo);
	void SetWeaponDropInfo_Implementation(FWeaponSlot WeaponSlotInfo);

	// void StartInteract_Implem  entation(AActor* Caller); BAD?

	void EndInteract();
	
	/** Get bIsCanInteract: */
	UFUNCTION(BlueprintPure, Category = "Interact")
	bool IsCanInteract() const { return bIsCanInteract; }

	/** Set bIsCanInteract: */
	UFUNCTION(BlueprintCallable, Category = "Interact")
	void SetCanInteract(bool NewCan);

	/** Usage Inteface: */
	void StartUsage(AActor* User);

	void EndUsage();

private:
	bool bIsCanInteract = false;
};
