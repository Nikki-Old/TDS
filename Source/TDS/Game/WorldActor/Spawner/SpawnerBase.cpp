// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/WorldActor/Spawner/SpawnerBase.h"
#include "SpawnerBase.h"

// Sets default values
ASpawnerBase::ASpawnerBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Create SceneComponent:
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	this->SetRootComponent(SceneComponent);

	// Set Base value:
	bIsCanSpawn = true;
	DelaySpawn = 1.0f;
	NumberOfActors = 3;
}

// Called when the game starts or when spawned
void ASpawnerBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpawnerBase::SpawnActor_Implementation()
{
	if (!GetWorld() || !bIsCanSpawn)
		return;

	// Set Spawn Parameters:
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = this;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	// Spawn:
	const auto NewActor = GetWorld()->SpawnActor<AActor>(ActorSpawnClass, GetActorTransform(), SpawnParameters);
	if (NewActor)
	{
		SpawnedActors.Add(NewActor);
		if (bIsInfinitySpawn)
		{
			CurrentNumberOfActors = -1;
		}
		else
		{
			CurrentNumberOfActors++;
		}
	}

	if (CurrentNumberOfActors < NumberOfActors)
	{
		// Start Delay:
		GetWorld()->GetTimerManager().SetTimer(Spawn_Timer, this, &ASpawnerBase::SpawnActor, DelaySpawn, false);
	}
}

int32 ASpawnerBase::GetSpawnedActors(TArray<AActor*>& CurrentSpawnedActors) const
{
	CurrentSpawnedActors = SpawnedActors;
	return SpawnedActors.Num();
}

void ASpawnerBase::StartSpawnActors()
{
	CurrentNumberOfActors = 0;

	if (NumberOfActors > 0 || bIsInfinitySpawn)
	{
		SpawnActor();
	}
}

void ASpawnerBase::EndSpawnActors()
{
	bIsCanSpawn = false;
}
