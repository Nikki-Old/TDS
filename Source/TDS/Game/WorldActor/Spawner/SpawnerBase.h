// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnerBase.generated.h"

class USceneComponent;

UCLASS()
class TDS_API ASpawnerBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawnerBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Spawner")
	USceneComponent* SceneComponent = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	/** How many actors need to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner | Settings")
	bool bIsInfinitySpawn = false;

	/** How many actors need to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner | Settings", meta = (EditCondition = "!bIsInfinitySpawn"))
	int32 NumberOfActors = 0;

	/** How often to spawn actors */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner | Settings")
	float DelaySpawn = 0.0f;

	/** Actor Class */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner | Settings")
	TSubclassOf<AActor> ActorSpawnClass;

	/** Get Spawned Actors */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Spawner")
	int32 GetSpawnedActors(TArray<AActor*>& CurrentSpawnedActors) const;

	UFUNCTION(BlueprintCallable, Category = "Spawner")
	void StartSpawnActors();
	UFUNCTION(BlueprintCallable, Category = "Spawner")
	void EndSpawnActors();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Spawner"/*, meta = (= "true")*/)
	void SpawnActor();
	virtual void SpawnActor_Implementation();

protected:
	/** Current number of spawned Actors: */
	int32 CurrentNumberOfActors = 0;

	/** Spawned Actors */
	TArray<AActor*> SpawnedActors;

	/** Spawn Timer */
	FTimerHandle Spawn_Timer;

	bool bIsCanSpawn = false;


};
