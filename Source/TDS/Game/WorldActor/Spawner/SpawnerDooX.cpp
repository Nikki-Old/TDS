// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/WorldActor/Spawner/SpawnerDooX.h"
#include "AI/Doox/Character_DooX.h"
#include "ActorComponent/Enemy/EnemyControllerComponent.h"
#include "Interface/CharacterInterface.h"
#include "Enemy/ControlledEnemyComponent.h"
#include "GameFramework/GameModeBase.h"

void ASpawnerDooX::BeginPlay()
{
	Super::BeginPlay();

	// Get EnemyController:
	if (GetWorld())
	{
		const auto GameMode = GetWorld()->GetAuthGameMode();
		if (GameMode)
		{
			auto Component = GameMode->GetComponentByClass(UEnemyControllerComponent::StaticClass());
			if (Component)
			{
				EnemyController = Cast<UEnemyControllerComponent>(Component);

				if (EnemyController)
				{
					EnemyController->OnStartSpawnEnemy.AddDynamic(this, &ASpawnerBase::StartSpawnActors);
					EnemyController->OnEndSpawnEnemy.AddDynamic(this, &ASpawnerBase::EndSpawnActors);
				}
			}
		}
	}
}
void ASpawnerDooX::SpawnActor_Implementation()
{
	if (!GetWorld() || !bIsCanSpawn)
		return;

	//// Set Spawn Parameters:
	//FActorSpawnParameters SpawnParameters;
	//SpawnParameters.Owner = this;
	//SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	auto SpawnClass = ChoiceSpawnClass();

	// Spawn:
	auto NewActor = GetWorld()->SpawnActorDeferred<AActor>(SpawnClass, GetActorTransform(), this, nullptr, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);
	if (NewActor)
	{
		if (EnemyController)
		{
			/** CEComp = Controlled Enemy Component, sorry! */
			UControlledEnemyComponent* CEComp = Cast<UControlledEnemyComponent>(NewActor->GetComponentByClass(UControlledEnemyComponent::StaticClass()));
			if (CEComp)
			{
				CEComp->InitComponent(EnemyController);
			}
		}

		ICharacterInterface::Execute_SetDynamicSpawn(NewActor, true);

		NewActor->FinishSpawning(GetActorTransform());

		SpawnedActors.Add(NewActor);

		if (bIsInfinitySpawn)
		{
			CurrentNumberOfActors = -1;
		}
		else
		{
			CurrentNumberOfActors++;
		}
	}

	if (CurrentNumberOfActors < NumberOfActors)
	{
		// Start Delay:
		GetWorld()->GetTimerManager().SetTimer(Spawn_Timer, this, &ASpawnerBase::SpawnActor, DelaySpawn, false);
	}
}

UClass* ASpawnerDooX::ChoiceSpawnClass()
{
	if (bIsRandomChoice)
	{
		const int RandomIndex = FMath::RandRange(0, SpawnCharacterClasses.Num() - 1);

		if (SpawnCharacterClasses.IsValidIndex(RandomIndex))
		{
			return SpawnCharacterClasses[RandomIndex];
		}
	}
	else
	{
		if (SpawnCharacterClasses.IsValidIndex(CurrentIndexSpawnCharacterClass))
		{
			const auto ChooseSpawnClass = SpawnCharacterClasses[CurrentIndexSpawnCharacterClass];

			CurrentIndexSpawnCharacterClass = (CurrentIndexSpawnCharacterClass + 1) % SpawnCharacterClasses.Num();

			return ChooseSpawnClass;
		}
	}

	return nullptr;
}
