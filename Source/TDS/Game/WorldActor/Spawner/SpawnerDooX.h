// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/WorldActor/Spawner/SpawnerBase.h"
#include "FuncLibrary/Types.h"
#include "SpawnerDooX.generated.h"

/**
 * 
 */
class UEnemyControllerComponent;
class ACharacter_DooX;

UCLASS()
class TDS_API ASpawnerDooX : public ASpawnerBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	//UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "SpawnerDooX | Settings ")
	//FCharacterParameters SpawnedCharacterParameters;

	/** Randomly choose which spawn class: */
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "SpawnerDooX | Settings ")
	bool bIsRandomChoice = true;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "SpawnerDooX | Settings ")
	TArray<TSubclassOf<ACharacter_DooX>> SpawnCharacterClasses;

	virtual void SpawnActor_Implementation() override;

private:
	UEnemyControllerComponent* EnemyController = nullptr;

	UClass* ChoiceSpawnClass();

	int32 CurrentIndexSpawnCharacterClass = 0;
};
