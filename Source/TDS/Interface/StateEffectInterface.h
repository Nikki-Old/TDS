// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Game/StateEffect/StateEffect.h"
#include "StateEffectInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UStateEffectInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API IStateEffectInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	/** Check: Return can set state effect for this Actor: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	bool IsCanSetStateEffect();

	/** Get Current State Effects: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	TArray<UStateEffect*> GetStateEffects() const;

	/** Add State Effect: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	bool AddStateEffect(UStateEffect* NewStateEffect);

	/** Remove State Effect: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "StateEffect")
	bool RemoveStateEffect(UStateEffect* StateEffect);

protected:
	bool bIsCanSetState = true;

	TArray<UStateEffect*> StateEffects = {};
};
