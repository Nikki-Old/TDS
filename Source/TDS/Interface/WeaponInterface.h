#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Game/Weapon/WeaponDefault.h"

#include "WeaponInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UWeaponInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API IWeaponInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	/** Try Get Weapon pointer */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	bool GetWeapon(AWeaponDefault*& Weapon);
};