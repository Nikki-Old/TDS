// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS : ModuleRules
{
    public TDS(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "SlateCore", "Paper2D" });

        PublicIncludePaths.AddRange(new string[] { "TDS/ActorComponent", "TDS/Interface", "TDS/Game/WorldActor", "TDS/Game/Weapon", "TDS/FuncLibrary", "TDS/Character", "TDS/Character/PlayerCharacter", "TDS/Game", "TDS/Game/GameMode" });
    }
};
