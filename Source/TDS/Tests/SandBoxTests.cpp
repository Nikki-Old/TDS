// Fill out your copyright notice in the Description page of Project Settings.

#if (WITH_DEV_AUTOMATION_TESTS || WITH_PERF_AUTOMATION_TESTS) // Check this Unreal macros. If True - compile this file.

#include "Tests/SandBoxTests.h"
#include "CoreMinimal.h"
#include "Misc/AutomationTest.h" // Tests library.

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FMathMaxInt, "TPSGame.Math.MaxInt", EAutomationTestFlags::ApplicationContextMask /*When it starts a Test. Current set all.*/ | EAutomationTestFlags::ProductFilter /* Can filter Tests.*/ | EAutomationTestFlags::HighPriority);

bool FMathMaxInt::RunTest(const FString& Parameters)
{
	AddInfo("Max [int] function testing"); // Adding information.

	TestTrue("2 different positive numbers", FMath::Max(12, 25) == 25);

	TestEqual("2 equal positive numbers", FMath::Max(123, 12), 111);

	TestTrueExpr(FMath::Max(0, 12343) == 12343);

	TestTrue("2 ", FMath::Max(0, 0) == 0);

	return true;
}

#endif