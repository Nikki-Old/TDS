// Fill out your copyright notice in the Description page of Project Settings.

#if (WITH_DEV_AUTOMATION_TESTS || WITH_PERF_AUTOMATION_TESTS) // Check this Unreal macros. If True - compile this file.

	#include "Tests/ScienceFuncLibTest.h"
	#include "CoreMinimal.h"
	#include "Misc/AutomationTest.h" // Tests library.
	#include "FuncLibrary/ScienceFuncLib.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FFibonacciSimple, "TPSGame.Science.Fibonacci.Simple",
	EAutomationTestFlags::ApplicationContextMask /*When it starts a Test. Current set all.*/ | EAutomationTestFlags::ProductFilter /* Can filter Tests.*/ | EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FFibonacciStress, "TPSGame.Science.Fibonacci.Stress",
	EAutomationTestFlags::ApplicationContextMask /*When it starts a Test. Current set all.*/ | EAutomationTestFlags::StressFilter /* Can filter Tests.*/ | EAutomationTestFlags::LowPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FFibonacciLogHasErrors, "TPSGame.Science.Fibonacci.LogHasErrors",
	EAutomationTestFlags::ApplicationContextMask /*When it starts a Test. Current set all.*/ | EAutomationTestFlags::ProductFilter /* Can filter Tests.*/ | EAutomationTestFlags::HighPriority);

bool FFibonacciSimple::RunTest(const FString& Parameters)
{
	AddInfo("Fibonacci simple testing: "); // Adding information.

	// 0 1 1 2 3 5 8 13 ...

	TestTrueExpr(UScienceFuncLib::Fibonacci(0) == 0);
	TestTrueExpr(UScienceFuncLib::Fibonacci(1) == 1);
	TestTrueExpr(UScienceFuncLib::Fibonacci(2) == 1);
	TestTrueExpr(UScienceFuncLib::Fibonacci(3) == 2);

	// TInterval == FVector2D
	struct TestPayload
	{
		int32 TestValue;
		int32 ExpectedValue;
	};

	const TArray<TestPayload> TestData{ { 0, 0 }, { 1, 1 }, { 2, 1 }, { 3, 2 }, { 4, 12 } };

	for (const auto Data : TestData)
	{
		const FString InfoString = FString::Printf(TEXT("Test value: %i, expected value: %i "), Data.TestValue, Data.ExpectedValue);
		TestEqual(InfoString, UScienceFuncLib::Fibonacci(Data.TestValue), Data.ExpectedValue);
	}

	return true;
}

bool FFibonacciStress::RunTest(const FString& Parameters)
{
	AddInfo("Fibonacci stress testing: "); // Adding information.

	/* for (int32 i = 2; i < 40; ++i)
	{
		TestTrueExpr(UScienceFuncLib::Fibonacci(i) == //
			UScienceFuncLib::Fibonacci(i - 1) + UScienceFuncLib::Fibonacci(i - 2));
	}*/

	int32 PrevPrevValue = 0;
	int32 PrevValue = 1;

	for (int32 i = 2; i < 40; ++i)
	{
		const int32 CurrentValue = UScienceFuncLib::Fibonacci(i);
		TestTrueExpr(CurrentValue == PrevPrevValue + PrevValue);

		PrevPrevValue = PrevValue;
		PrevValue = CurrentValue;
	}

	return true;
}

bool FFibonacciLogHasErrors::RunTest(const FString& Parameters)
{
	AddInfo("Fibonacci log has Errors testing: "); // Adding information.

	AddExpectedError("Invalid input for Fibonacci", EAutomationExpectedErrorFlags::Contains);
	UScienceFuncLib::Fibonacci(-10);

	return true;
}


#endif