// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/UMG/Public/UMG.h"
#include "CoreMinimal.h"
#include "Components/Button.h"
#include "TPSButtonBase.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTPSButtonBase : public UButton
{
	GENERATED_BODY()

public:
	UTPSButtonBase();


#if WITH_EDITOR
	virtual const FText GetPaletteCategory() override;
#endif
};
