// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Button/TPSCloseButton.h"

#define LOCTEXT_NAMESPACE "UMG"
UTPSCloseButton::UTPSCloseButton()
{
	// SlateWidgetStyleAsset'/Game/UI/SlateWidgetStyle/SW_Button_Base.SW_Button_Base'
	static ConstructorHelpers::FObjectFinder<USlateWidgetStyleAsset> ButtonCustomStyle(TEXT("/Game/UI/Button/SlateWidget/SW_Button_Base"));

	SButton::FArguments ButtonDefaults;

	ButtonDefaults.ButtonStyle(ButtonCustomStyle.Object);
	WidgetStyle = *ButtonDefaults._ButtonStyle;
}

#if WITH_EDITOR
const FText UTPSCloseButton::GetPaletteCategory()
{
	return LOCTEXT("", "TPS Custom Widget");
}
#endif

#undef LOCTEXT_NAMESPACE