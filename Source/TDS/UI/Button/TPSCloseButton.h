// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "TPSCloseButton.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTPSCloseButton : public UButton
{
	GENERATED_BODY()

public:
	UTPSCloseButton();

#if WITH_EDITOR
	virtual const FText GetPaletteCategory() override;
#endif
};
