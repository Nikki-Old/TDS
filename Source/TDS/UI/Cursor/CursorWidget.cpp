// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Cursor/CursorWidget.h"
#include "Components/Image.h"
#include "Character/TDSCharacterBase.h"
#include "CursorWidget.h"

void UCursorWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (OwnerCharacter)
	{
		OwnerCharacter->OnStartAttack.AddDynamic(this, &UCursorWidget::CharacterAttackStatus);
		OwnerCharacter->OnCharacterIsHit.AddDynamic(this, &UCursorWidget::CharacterIsHit);
		OwnerCharacter->OnCharacterIsKill.AddDynamic(this, &UCursorWidget::CharacterIsKill);
	}
}

void UCursorWidget::CharacterAttackStatus_Implementation(bool bIsAttack)
{
}

void UCursorWidget::CharacterIsHit_Implementation()
{
}

void UCursorWidget::CharacterIsKill_Implementation(AActor* WhoIsKilled)
{
}
