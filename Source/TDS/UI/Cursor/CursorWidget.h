// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CursorWidget.generated.h"

/**
 * 
 */

class UImage;
class ATDSCharacterBase;

UCLASS()
class TDS_API UCursorWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (BindWidget))
	UImage* CursorImage = nullptr;

	/* Override base function. */
	virtual void NativeConstruct() override;

	UFUNCTION()
	void SetOwnerCharacter(ATDSCharacterBase* NewOwnerCharacter) { OwnerCharacter = NewOwnerCharacter; }

protected:
	UPROPERTY()
	ATDSCharacterBase* OwnerCharacter = nullptr;

	UFUNCTION(BlueprintNativeEvent, Category = "CursorWidget")
	void CharacterAttackStatus(bool bIsAttack);
	void CharacterAttackStatus_Implementation(bool bIsAttack);

	UFUNCTION(BlueprintNativeEvent, Category = "CursorWidget")
	void CharacterIsHit();
	void CharacterIsHit_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "CursorWidget")
	void CharacterIsKill(AActor* WhoIsKilled);
	void CharacterIsKill_Implementation(AActor* WhoIsKilled);
};
