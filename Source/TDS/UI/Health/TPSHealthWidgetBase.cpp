// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Health/TPSHealthWidgetBase.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetTextLibrary.h"

#include "FuncLibrary/Utility.h"
#include "ActorComponent/Health/TPSHealthComponentBase.h"

void UTPSHealthWidgetBase::InitHealthEvent(AActor* Owner)
{
	checkf(Owner, TEXT("Owner is NULL"));

	// Set Owner Actor:
	OwnerActor = Owner;

	// Get Health Component:
	auto OwnerHealthComponent = Utils::GetTPSComponent<UTPSHealthComponentBase>(Owner);

	// Debug:
	checkf(OwnerHealthComponent, TEXT("Owner Actor is not have UTPSHealthComponentBase"));

	if (OwnerHealthComponent)
	{
		OwnerCurrentHealth = OwnerHealthComponent->GetHealth();
		OwnerTXTHealth = OwnerCurrentHealth;
		OwnerHealthComponent->OnChangeHealth.AddDynamic(this, &UTPSHealthWidgetBase::OwnerChangeHealth);
	}
}

void UTPSHealthWidgetBase::OwnerChangeHealth(AActor* Owner, float CurrentHealth, float Change)
{
	if (Owner != OwnerActor)
		return;

	OwnerCurrentHealth = CurrentHealth;

	if (OwnerCurrentHealth <= 0)
	{
		OwnerCurrentHealth = 0;
	}

	StartUpdateTXTHealth(OwnerTXTHealth);
}

FText UTPSHealthWidgetBase::GetCurrentHealthText() const
{
	TEnumAsByte<ERoundingMode> RoundingMode;
	return UKismetTextLibrary::Conv_FloatToText(OwnerTXTHealth, RoundingMode, false, true, 3, 3, 0, 0);
}

void UTPSHealthWidgetBase::UpdateTXTHealth_Implementation()
{
	if (OwnerCurrentHealth == OwnerTXTHealth)
		return;

	OwnerCurrentHealth > OwnerTXTHealth ? ++OwnerTXTHealth : --OwnerTXTHealth;

	StartUpdateTXTHealth(OwnerTXTHealth);
}

void UTPSHealthWidgetBase::StartUpdateTXTHealth(int OldHealth)
{
	if (!GetWorld())
		return;

	if (OwnerCurrentHealth != OldHealth)
	{

		GetWorld()->GetTimerManager().SetTimer(UpdateHealthTimer, this, &UTPSHealthWidgetBase::UpdateTXTHealth, SpeedUpdate);
	}
}
