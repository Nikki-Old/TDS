// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "TPSHealthWidgetBase.generated.h"

/**
 * 
 */

UCLASS()
class TDS_API UTPSHealthWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TXTHealth = nullptr;

	UFUNCTION(BlueprintCallable, Category = "HealthWidget")
	virtual void InitHealthEvent(class AActor* Owner);

	UFUNCTION()
	void OwnerChangeHealth(AActor* Owner, float CurrentHealth, float Change);

	UFUNCTION(BlueprintCallable, Category = "HealthWidget")
	FText GetCurrentHealthText() const;

	/** How fast update */
	UPROPERTY(EditDefaultsOnly, Category = "HealthWidget")
	float SpeedUpdate = 0.01f;

	/** Update TXTHealh: */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "HealthWidget")
	void UpdateTXTHealth();
	void UpdateTXTHealth_Implementation();

protected:
	/** Owner Actor this Widget. */
	AActor* OwnerActor = nullptr;

private:
	/** Owner current health. */
	int OwnerCurrentHealth = 0;

	/** Owner health. */
	int OwnerTXTHealth = 0;

	/** Start timer to update function: */
	void StartUpdateTXTHealth(int OldHealth);

	/** Timer to update next Health: */
	FTimerHandle UpdateHealthTimer;
};
