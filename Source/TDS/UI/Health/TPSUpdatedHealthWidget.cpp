// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Health/TPSUpdatedHealthWidget.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetTextLibrary.h"

#include "FuncLibrary/Utility.h"
#include "ActorComponent/Health/TPSUpdatedHealthComponent.h"

void UTPSUpdatedHealthWidget::InitHealthEvent(AActor* Owner)
{
	check(Owner);

	checkf(Owner, TEXT("Owner is NULL"));

	// Set Owner Actor:
	OwnerActor = Owner;

	// Get Health Component:
	auto OwnerHealthComponent = Utils::GetTPSComponent<UTPSUpdatedHealthComponent>(Owner);

	// Debug:
	checkf(OwnerHealthComponent, TEXT("Owner Actor is not have UTPSHealthComponentBase"));

	if (OwnerHealthComponent)
	{
		OwnerCurrentShieldPoints = OwnerHealthComponent->GetShieldPoints();
		OwnerTXTShieldPoints = OwnerCurrentShieldPoints;
		OwnerHealthComponent->OnChangeShield.AddDynamic(this, &UTPSUpdatedHealthWidget::OwnerChangeShieldPoints);
	}
	Super::InitHealthEvent(Owner);
}

void UTPSUpdatedHealthWidget::OwnerChangeShieldPoints(AActor* Owner, float CurrentShieldPoints, float Change)
{
	if (Owner != OwnerActor)
		return;

	OwnerCurrentShieldPoints = CurrentShieldPoints;

	if (OwnerCurrentShieldPoints <= 0)
	{
		OwnerCurrentShieldPoints = 0;
	}

	StartUpdateShieldPoints(OwnerTXTShieldPoints);
}

FText UTPSUpdatedHealthWidget::GetCurrentShieldPointsText() const
{
	TEnumAsByte<ERoundingMode> RoundingMode;
	return UKismetTextLibrary::Conv_FloatToText(OwnerTXTShieldPoints, RoundingMode, false, true, 3, 3, 0, 0);
}

void UTPSUpdatedHealthWidget::UpdateShieldPoints_Implementation()
{
	if (OwnerCurrentShieldPoints == OwnerTXTShieldPoints)
		return;

	OwnerCurrentShieldPoints > OwnerTXTShieldPoints ? ++OwnerTXTShieldPoints : --OwnerTXTShieldPoints;

	StartUpdateShieldPoints(OwnerTXTShieldPoints);
}

void UTPSUpdatedHealthWidget::StartUpdateShieldPoints(int OldShieldPoints)
{
	if (!GetWorld())
		return;

	if (OwnerCurrentShieldPoints != OldShieldPoints)
	{

		GetWorld()->GetTimerManager().SetTimer(UpdateShieldPointsTimer, this, &UTPSUpdatedHealthWidget::UpdateShieldPoints, SpeedUpdate);
	}
}