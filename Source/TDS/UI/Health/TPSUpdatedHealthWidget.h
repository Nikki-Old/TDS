// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/Health/TPSHealthWidgetBase.h"
#include "TPSUpdatedHealthWidget.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTPSUpdatedHealthWidget : public UTPSHealthWidgetBase
{
	GENERATED_BODY()
public:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TXTShieldPoints = nullptr;

	virtual void InitHealthEvent(class AActor* Owner) override;

	UFUNCTION()
	void OwnerChangeShieldPoints(AActor* Owner, float CurrentShieldPoints, float Change);

	UFUNCTION(BlueprintCallable, Category = "HealthWidget")
	FText GetCurrentShieldPointsText() const;

	/** Update TXTHealh: */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "HealthWidget")
	void UpdateShieldPoints();
	void UpdateShieldPoints_Implementation();

private:
	/** Start timer to update function: */
	void StartUpdateShieldPoints(int OldShieldPoints);

	/** Owner current health. */
	int OwnerCurrentShieldPoints = 0;

	/** Owner health. */
	int OwnerTXTShieldPoints = 0;

	/** Timer to update next Health: */
	FTimerHandle UpdateShieldPointsTimer;
};
