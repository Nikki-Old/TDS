// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Inventory/DynamicInventoryWidgetBase.h"

void UDynamicInventoryWidgetBase::Hide_Implementation()
{
	this->RemoveFromViewport();
}

void UDynamicInventoryWidgetBase::UnHide_Implementation()
{
	this->AddToViewport(0);
}
