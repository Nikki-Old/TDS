// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DynamicInventoryWidgetBase.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UDynamicInventoryWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InventoryWidget")
	void Hide();
	void Hide_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "InventoryWidget")
	void UnHide();
	void UnHide_Implementation();
};
