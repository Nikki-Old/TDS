// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Menu/Level/LevelSelectionWidget.h"
#include "Components/HorizontalBox.h"
#include "Game/TPSGameInstance.h"
#include "UI/Menu/Level/LevelWidgetBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogLevelSelection, All, All);

void ULevelSelectionWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	InitLevelsInformation();
}

void ULevelSelectionWidget::InitLevelsInformation()
{
	if (!GetWorld())
	{
		return;
	}

	GameInstance = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (!GameInstance)
	{
		UE_LOG(LogLevelSelection, Error, TEXT("InitLevelItems :: GameInstance is nillptr! Cast to UTPSGameInstance is FAILED!"));
		return;
	}

	TArray<FLevelInfo> LevelsInfo;
	GameInstance->GetLevelsInfo(LevelsInfo);

	checkf(LevelsInfo.Num() > 0, TEXT("LevelsInfo is empty!"));

	if (LevelsInfoHorizontalBox)
	{
		LevelsInfoHorizontalBox->ClearChildren();

		for (auto& LevelInfo : LevelsInfo)
		{
			const auto LevelWidget = CreateWidget<ULevelWidgetBase>(GetWorld(), LevelInfoWidgetClass);

			if (!LevelWidget)
				continue;

			LevelWidget->SetLevelInfo(LevelInfo);
			LevelWidget->OnLevelSelected.AddUObject(this, &ULevelSelectionWidget::OnLevelSelected);

			LevelsInfoHorizontalBox->AddChild(LevelWidget);
			LevelInfoWidgets.Add(LevelWidget);

			if (LevelWidget->GetLevelInfo().LevelName == GameInstance->GetLastLevelName())
			{
				LevelWidget->SetSelected(true);
			}
		}
	}
}

void ULevelSelectionWidget::OnLevelSelected(FLevelInfo& LevelInformation)
{
	if (GameInstance)
	{
		GameInstance->SetStartupLevel(LevelInformation);

		for (auto LevelWidget : LevelInfoWidgets)
		{
			if (LevelWidget)
			{
				const auto IsSelected = LevelInformation.LevelName == LevelWidget->GetLevelInfo().LevelName ? true : false;

				LevelWidget->SetSelected(IsSelected);
			}
		}
	}
}
