// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/Menu/MenuWidgetBase.h"
#include "FuncLibrary/Types.h"
#include "LevelSelectionWidget.generated.h"

/**
 * 
 */

UCLASS()
class TDS_API ULevelSelectionWidget : public UMenuWidgetBase
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	class UHorizontalBox* LevelsInfoHorizontalBox = nullptr;

protected:
	virtual void NativeOnInitialized() override;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "LevelSelection")
	class UTPSGameInstance* GameInstance = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "LevelSelection")
	TSubclassOf<class ULevelWidgetBase> LevelInfoWidgetClass;

private:
	UPROPERTY()
	TArray<class ULevelWidgetBase*> LevelInfoWidgets;

	void InitLevelsInformation();
	void OnLevelSelected(FLevelInfo& LevelInformation);
};
