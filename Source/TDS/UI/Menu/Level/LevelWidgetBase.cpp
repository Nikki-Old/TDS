// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Menu/Level/LevelWidgetBase.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"

bool ULevelWidgetBase::Initialize()
{
	bool Result = Super::Initialize();

	return Result;
}

void ULevelWidgetBase::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (LevelButton)
	{
		LevelButton->OnClicked.AddDynamic(this, &ULevelWidgetBase::OnLevelWidgetClicked);
	}
}

void ULevelWidgetBase::OnLevelWidgetClicked()
{
	OnLevelSelected.Broadcast(LevelInfo);
}

void ULevelWidgetBase::SetLevelInfo(FLevelInfo &NewInfo)
{
	LevelInfo = NewInfo;

	// Updated Level Display Name:
	if (LevelName)
	{
		LevelName->SetText(FText::FromName(LevelInfo.LevelDisplayName));
	}

	// Updated Level Display Image:
	if (LevelImage && LevelInfo.LevelImage)
	{
		LevelImage->SetBrushFromTexture(LevelInfo.LevelImage);

	}

	// TO DO: Update Level description.
}

FLevelInfo ULevelWidgetBase::GetLevelInfo() const
{
	return LevelInfo;
}

void ULevelWidgetBase::SetSelected_Implementation(bool IsSelected)
{
	if (LevelFrameImage)
	{
		LevelFrameImage->SetVisibility(IsSelected ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	}
}
