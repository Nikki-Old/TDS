// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FuncLibrary/Types.h"
#include "LevelWidgetBase.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ULevelWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	/** Delegate: */
	FOnLevelSelectedSignature OnLevelSelected;

	UFUNCTION(BlueprintCallable, Category = "LevelWidget")
	void SetLevelInfo(FLevelInfo &NewInfo);

	UFUNCTION(BlueprintCallable, Category = "LevelWidget")
	FLevelInfo GetLevelInfo() const;

	UFUNCTION(BlueprintNativeEvent, Category = "LevelWidget")
	void SetSelected(bool IsSelected);
	void SetSelected_Implementation(bool IsSelected);

protected:
	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Categoty = "LevelWidgetBase")
	virtual bool Initialize() override;

	virtual void NativeOnInitialized() override;

	/** Click and start load this level. */
	UPROPERTY(meta = (BindWidget))
	class UButton* LevelButton = nullptr;

	/** Level Image. */
	UPROPERTY(meta = (BindWidget))
	class UImage* LevelImage = nullptr;

	UPROPERTY(meta = (BindWidget))
	class UImage* LevelFrameImage = nullptr;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* LevelName = nullptr;

private:
	FLevelInfo LevelInfo;

	UFUNCTION()
	void OnLevelWidgetClicked();
};
