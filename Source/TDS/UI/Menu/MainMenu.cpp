// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Menu/MainMenu.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "FuncLibrary/TPSClassLibrary.h"
#include "Game/TPSGameInstance.h"

void UMainMenu::NativeOnInitialized()
{
	Super::NativeOnInitialized();
}

void UMainMenu::RemoveFromParent()
{
	ClearChildWidgets();

	Super::RemoveFromParent();
}

void UMainMenu::PlayGame_Implementation()
{
	if (!GetWorld())
		return;

	// Get GameInstate:
	auto GameInstance = UTPSClassLibrary::GetTPSGameInstance(GetWorld());
	if (GameInstance)
	{
		// Get Start Level Name:
		const auto StartupLevelName = GameInstance->GetStartupLevelName();
		if (!StartupLevelName.IsNone())
		{
			GameInstance->LoadNewLevel(StartupLevelName);
		}
		else
		{
			GameInstance->LoadNewLevel(GameInstance->GetDefaultLevelName());
		}
	}
	else
	{
		checkf(GameInstance, TEXT("GameInstance is nullptr!"));
	}
}

void UMainMenu::ExitGame_Implementation()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, true);
}

void UMainMenu::AddChild(UUserWidget* NewChild)
{
	if (NewChild)
	{
		ChildWidgets.Add(NewChild);
	}
}

void UMainMenu::RemoveChild(UUserWidget* Child)
{
	if (Child)
	{
		auto ChildWidget = ChildWidgets.Find(Child);
		if (ChildWidget > -1)
		{
			ChildWidgets.RemoveAt(ChildWidget);
		}
	}
}

void UMainMenu::ClearChildWidgets()
{
	if (ChildWidgets.Num() <= 0)
	{
		return;
	}

	for (auto &It : ChildWidgets)
	{
		It->RemoveFromParent();
	}
}
