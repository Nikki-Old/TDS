// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* NameGameTextBlock = nullptr;

	virtual void NativeOnInitialized() override;

	virtual void RemoveFromParent() override;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MainMenu")
	void PlayGame();
	void PlayGame_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MainMenu")
	void ExitGame();
	void ExitGame_Implementation();

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void AddChild(UUserWidget* NewChild);

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void RemoveChild(UUserWidget* Child);

protected:
	void ClearChildWidgets();

	UPROPERTY()
	TArray<UUserWidget*> ChildWidgets;
};
