// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/Menu/MenuWidgetBase.h"
#include "UI/Button/TPSCloseButton.h"
#include "Blueprint/WidgetTree.h"

void UMenuWidgetBase::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (CloseButtonWidget)
	{
		if (CloseButtonWidget->WidgetTree)
		{
			auto CloseButton = Cast<UTPSCloseButton>(CloseButtonWidget->WidgetTree->FindWidget("CloseButton"));
			if (CloseButton)
			{
				CloseButton->OnClicked.AddDynamic(this, &UMenuWidgetBase::CloseMenu);
			}
		}
	}
}

void UMenuWidgetBase::AddChild(UUserWidget* NewChild)
{
	if (NewChild)
	{
		ChildWidgets.Add(NewChild);
	}
}

void UMenuWidgetBase::RemoveChild(UUserWidget* Child)
{
	if (Child)
	{
		auto ChildWidget = ChildWidgets.Find(Child);
		if (ChildWidget > -1)
		{
			ChildWidgets.RemoveAt(ChildWidget);
		}
	}
}

void UMenuWidgetBase::SetPreviousWidget(UUserWidget* InPreviousWidget)
{
	if (InPreviousWidget)
	{
		PreviousWidget = InPreviousWidget;
	}
	else
	{
		check(InPreviousWidget);
	}
}

void UMenuWidgetBase::CloseMenu_Implementation()
{
	if (PreviousWidget)
	{
		//PreviousWidget->SetFocus();
	}
	else
	{
		check(PreviousWidget);
	}

	this->RemoveFromParent();
}

void UMenuWidgetBase::RemoveFromParent()
{
	OnCloseMenuWidget.Broadcast(this);

	ClearChildWidgets();

	Super::RemoveFromParent();
}

void UMenuWidgetBase::ClearChildWidgets()
{
	if (ChildWidgets.Num() <= 0)
	{
		return;
	}

	for (auto& It : ChildWidgets)
	{
		It->RemoveFromParent();
	}
}
