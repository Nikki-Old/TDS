// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidgetBase.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCloseMenuWidget, UMenuWidgetBase*, MenuWidget);

UCLASS()
class TDS_API UMenuWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "MenuWidget")
	FOnCloseMenuWidget OnCloseMenuWidget;

protected:
	UPROPERTY(meta = (BindWidget))
	UUserWidget* CloseButtonWidget = nullptr;

	//UPROPERTY(meta = (BindWidget))
	//UButton* SelectLevelButton = nullptr;

	virtual void NativeOnInitialized() override;

	/** Previus Widget */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "MenuWidget")
	UUserWidget* PreviousWidget = nullptr;

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void AddChild(UUserWidget* NewChild);

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void RemoveChild(UUserWidget* Child);

public:
	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void SetPreviousWidget(UUserWidget* InPreviousWidget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MenuWidget")
	void CloseMenu();
	void CloseMenu_Implementation();

	virtual void RemoveFromParent() override;

protected:
	void ClearChildWidgets();

	UPROPERTY()
	TArray<UUserWidget*> ChildWidgets;
};
