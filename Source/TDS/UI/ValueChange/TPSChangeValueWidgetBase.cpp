// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/ValueChange/TPSChangeValueWidgetBase.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetTextLibrary.h"
#include "Kismet/KismetMathLibrary.h"

#include "FuncLibrary/Constant/TPSConstant.h"
#include "Game/WorldActor/Damage/TPSDamageActorBase.h"

bool UTPSChangeValueWidgetBase::Initialize()
{
	return Super::Initialize();
}

void UTPSChangeValueWidgetBase::NativeConstruct()
{
	Super::NativeConstruct();

	if (TXTValueChange)
	{
		TXTValueChange->TextDelegate.BindUFunction(this, TEXT("GetValueChangeTXT"));
		TXTValueChange->ColorAndOpacityDelegate.BindUFunction(this, TEXT("GetColorTXTValueChange"));
	}

	ValueWidgetAnimIsEnd.BindDynamic(this, &UTPSChangeValueWidgetBase::AnimationIsEnd);

	// Bind to AnimationFinished Delegate and AnimationIsEnd:
	if (TXTValueChangeAnim)
	{
		BindToAnimationFinished(TXTValueChangeAnim, ValueWidgetAnimIsEnd);
	}
}

void UTPSChangeValueWidgetBase::InitEvent(AActor* Owner, float Change)
{
	checkf(Owner, TEXT("Owner is NULL"));

	// Set Owneractor:
	OwnerActor = Cast<ATPSDamageActorBase>(Owner);

	check(OwnerActor);

	/* Save current Change: **/
	SetValueChangeInTextBlock(Change);
	/* Set Color for TXT: **/
	TXTValueChange->ColorAndOpacity = GetColorTXTValueChange();

	// Play animation:
	if (TXTValueChangeAnim)
	{
		PlayAnimation(TXTValueChangeAnim);
	}
}

void UTPSChangeValueWidgetBase::SetValueChangeInTextBlock(float Change)
{
	ValueChange = Change;
}

FText UTPSChangeValueWidgetBase::GetValueChangeTXT() const
{
	TEnumAsByte<ERoundingMode> RoundingMode = ERoundingMode::HalfFromZero;
	return UKismetTextLibrary::Conv_FloatToText(ValueChange, RoundingMode, true, true, 1, 3, 0, 0);
}

void UTPSChangeValueWidgetBase::AnimationIsEnd()
{
	if (OwnerActor)
		OwnerActor->DestroyEvent();
}

/* FLinearColor UTPSChangeHealthWidgetBase::GetColorHealthChange() const
{
	return HealthChangeColor;
}*/

FSlateColor UTPSChangeValueWidgetBase::GetColorTXTValueChange()
{
	if (ValueChange == 0.0f || !OwnerActor)
		return FSlateColor(BaseColor);

	float CurrentHeath = OwnerActor->GetLeftValue();

	// Improved, inctorrect color calcuclations. Must be GREEN to RED:
	BaseColor.R = UKismetMathLibrary::MapRangeClamped(CurrentHeath, 0.0f, TPSConstant::MAX_HEALTHS, 1.0f, 0.0f);
	BaseColor.G = UKismetMathLibrary::MapRangeClamped(CurrentHeath, 0.0f, TPSConstant::MAX_HEALTHS, 0.0f, 1.0f);
		
	BaseColor.B = 0.0f;
	return FSlateColor(BaseColor);
}